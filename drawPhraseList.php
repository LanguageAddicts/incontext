<?php
require("db_functions.php");
global $connection; 											//set up db connection using global variable
opendb(); 
$limitViewSize = 30;
$phraselist = getWantedPhrases();  								//get list of wanted words

	while($phrase = mysqli_fetch_array($phraselist))
	{
		$wantedPhrases[] = $phrase;									//store the sql result in working array
	}
$total = count($wantedPhrases);
//get list of themes
$themeList = getAllTags();  								//get list of themes
	while($theme = mysqli_fetch_array($themeList))
	{
		$themes[] = $theme;									//store the sql result in working array
	}          


echo"<br>";
echo"Total phrases: ". sizeof($wantedPhrases);
echo "<div id=\"phraselist\">";
echo "<table class=\"wordlisttable\">";
echo "<tr class=\"tableRowStyle1\"><td>Phrase</td><td>Scene notes</td><td>Theme</td><td>action</td></tr>";
foreach($wantedPhrases as $wantedPhrase)							//add contributor details to list
		{
		if (++$phraseLoop == $limitViewSize){  break;}
    echo "<tr>";
    $deleteID="deletephrase-".$wantedPhrase["PHRASEID"];
    $saveID="savephrase-".$wantedPhrase["PHRASEID"];
    $phraseID = "phraseString-".$wantedPhrase["PHRASEID"];
    $notesID = "notesString-".$wantedPhrase["PHRASEID"];
	$themeID = "themeString-".$wantedPhrase["PHRASEID"];
	$currentTheme = $wantedPhrase["THEME"];
  	$currentPoints = $wantedPhrase["POINTS"];
    $currentPointsID= "pointsString-".$wantedPhrase["PHRASEID"];
    //$notes = " ";
    $notes=stripslashes($wantedPhrase["NOTES"]);
    $printPhrase=stripslashes($wantedPhrase["PHRASE"]);
	echo "<td><textarea id=\"{$phraseID}\" cols=\"20\" rows=\"10\">".$printPhrase."</textarea></td>";
    echo "<td><textarea id=\"{$notesID}\"cols=\"65\" rows=\"10\">{$notes}</textarea></td>"; //delete icon
    echo "<td>"; 
	themeDropDown($themeID,$currentTheme,$themes);
	echo "</td>";
	echo "<td><a href=\"#\" id={$saveID}><img src=\"images/save.png\"><br></a><a href=\"#\" id={$deleteID}><img src=\"images/delete.png\"></a><br>";
  enumPoints($currentPoints,$currentPointsID);
  echo"</td>"; //delete icon
    echo "</tr>";
		}	
      echo "</table>";
echo "</div>";
mysqli_close($connection);										//close the database connection

function themeDropDown($themeID,$currentTheme,$themesIn){

echo<<<EOF
<select id={$themeID}>
<option value=""> </option>
EOF;
foreach($themesIn as $readTheme)
{
$selected = "";
$thisTheme = $readTheme["TAGNAME"];
echo "<option value=\"{$thisTheme}\" "; 
if ($thisTheme == $currentTheme)									//make sure if a value already exists for this row that it is pre-selected in dropdown
{
echo "selected";
}					
echo ">{$thisTheme}</option>";
}
echo "</select>";

}


function enumPoints($currentPoints,$pointsString)
{
  
   
   
   $selectDropdown = "<select id=\"{$pointsString}\" size=\"0\">";
   $enumList = getPointsList();

    foreach($enumList as $value)
    {         
         $selectDropdown .= "<option value=\"$value\"";
         if ($value == $currentPoints)									//make sure if a value already exists for this row that it is pre-selected in dropdown
        {
         $selectDropdown .=" selected ";
        }	
         $selectDropdown .= ">".$value."</option>";
    }
    $selectDropdown .= "</select>\n";

    echo $selectDropdown;
    return; 
}
?>