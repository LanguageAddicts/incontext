<?php
/**
 * @author Timothy Clements
 * @copyright 2010
 * ` */


function preparepostdata($words, $scenario, $videoWords, $connection)
{
    // This function filters and escapes the data posted from the form and sets the required variables and arrays
    
    global $scenario;
    $scenario = strval($_POST[scenario]);
    // Remove line breaks
    $scenario = str_replace("\r", "", $scenario);
    // Remove carriage returns
    $scenario = str_replace("\n", "", $scenario);
    // escape vars
    $scenario = mysqli_real_escape_string($connection, $scenario);

    // escape vars
    global $words;
    $words = strval($_POST[words]);
    // Remove line breaks
    $words = str_replace("\r", "", $words);
    // Remove carriage returns
    $words = str_replace("\n", "", $words);
    // escape vars
    // $words = mysqli_real_escape_string($connection, $words);
   


    // Now strip out blanks from
    // $words = str_replace(" ", '', $words);
    // Explode words into an array
    // $words = preg_replace("/[^a-zA-Z\s]/", "", $words);
    // $words = ereg_replace("[^A-Za-z0-9]", "", $words);
    preg_match_all('/\b[a-z\']+\b/i', $words, $matches);
    // Regular expression
    // [a-zA-Z]  => match any single character between a and z and A and Z
    // [^a=z] => don't match any character between a and z
    // ^ means not when placed in []
    // \b[a-z]\b = whole word search with characters between a and z
    // + in [a-z]+ means allow proceeding character to be found more than once e.g.
    // /i makes regex case insensitive


    global $videoWords;
    $videoWords = $matches[0];
    // $videoWords = explode(" ", $words);
}

function wordHasBeenAddedAlready($addedWords, $insertWord)
{
    $count = count($addedWords);

    for ($i = 0; $i <= $count - 1; $i++) {

        if ($addedWords[$i][word] == $insertWord) {
            return true;
        }

    }
    return false;

}


function getvideofilename($wordCount, $videoWords)
{

    // Build the file name by concatenating the words with an underscore. Don't add an underscore during the last loop!
    // Remember array starts at zero so minus one from word count when looping
    // $wordcount = no of words in the video

    for ($i = 0; $i <= $wordCount - 1; $i++) {
        if ($i + 1 == $wordCount) {
            $videoName .= $videoWords[$i];
        } else {
            $videoName .= $videoWords[$i] . "_";
        }
    }
    return $videoName;
}

function addvideofile($videoName, $wordCount, $scenario, $attribution, $attributionurl, $submitted, $contributorID, $connection)
{
    // Escape the query
	
	$videoName = mysqli_real_escape_string($connection, $videoName);

    $add_videofile = "insert into videos (VIDEONAME, WORDCOUNT, SCENARIO, ATTRIBUTION, ATTRIBUTIONURL, SUBMITTED, CONTRIBID, ACTIVE) values ('$videoName','$wordCount','$scenario', '$attribution', '$attributionurl','$submitted', '$contributorID', 'Y')";
    
  // $add_videofile = "insert into videos (VIDEONAME, WORDCOUNT, SCENARIO, ATTRIBUTION, ATTRIBUTIONURL, ACTIVE) values ('$videoName','$wordCount','$scenario', '$attribution', '$attributionurl', 'Y')";


    $result = mysqli_query($connection, $add_videofile) or die($add_videofile);

    if (!$result) {
        die("ERROR!<br />Query is: $query<br />Error is: " . mysqli_error($connection));
    }

}

function getvideoid($videoName, $connection)
{

    // Escape the query

    $videoName = mysqli_real_escape_string($connection, $videoName);
    // Add word to video mapping

    // First get videoid for newly created video
    $query = "select videoid from videos where videoname = '$videoName' order by videoid desc";
    $result = mysqli_query($connection, $query);

    // or die("ERROR!<br />Query is: $query<br />Error is: " . mysql_error($connection));


    if (!$result) {
        die("ERROR!<br />Query is: $query<br />Error is: " . mysqli_error($connection));
    }
    $resultarray = mysqli_fetch_assoc($result);
    $videoId = $resultarray["videoid"];
    return $videoId;
}

function mapnewwords($videoId, $newWords, $connection)
{
    $wordCount = count($newWords);
    if ($newWords > 0) {
        foreach ($newWords as $resultWord) {
            // Get wordid of new word
            $query = "select wordid from words where word = '$resultWord'";
            $result = mysqli_query($connection, $query) or die($query);
            $resultarray = mysqli_fetch_assoc($result);
            $wordId = $resultarray["wordid"];

            // Add new mapping record of video to words for new words and new video
            $addWordMapping = "insert into wordvideomap (WORDID, VIDEOID) values ('$wordId','$videoId')";
            $result = mysqli_query($connection, $addWordMapping) or die($addWordMapping);

        }
    }
}

function mapexistingwords($videoId, $duplicateWords, $connection)
{
    // Next add existing words ($duplicateWords) with a mapping to new video ($videoName)

    $wordCount = count($duplicateWords);
    if ($duplicateWords > 0) {
        foreach ($duplicateWords as $resultWord) {

            // Get wordid of existing words ($duplicateWords)
            $query = "select wordid from words where word = '$resultWord'";
            $result = mysqli_query($connection, $query) or die($query);
            $resultarray = mysqli_fetch_assoc($result);
            $wordId = $resultarray["wordid"];

            // Add new mapping record of video to words for existing words and new video
            $addWordMapping = "insert into wordvideomap (WORDID, VIDEOID) values (WORDID, VIDEOID) ('','$wordId','$videoId')";
            $result = mysqli_query($connection, $addWordMapping) or die("ERROR!<br />Query is: $addWordMapping<br />Error is: " .
                mysql_error());

        }
    }
}

function prepareValidateData($videoWords, $videoToEdit, $connection)
{

    // Loop round the $videoWords array and add words into $newWords array. If the word is a new word,
    // i.e. is not in the database already or hasn't aleady been added in this batch of words, then add
    // it with the newword element set to "checked"


    // Set a duplicate and new word counter

    global $WordCount;
    $WordCount = 0;
    global $uniqueWordsCount;
    global $newWords;
    $uniqueWordsCount = 0;
    $Sequence = 0;
    $wordsAddedThisTime = array();


    foreach ($videoWords as $resultWord) {

        //	$resultWord = mysqli_real_escape_string($connection, $resultWordEsc);
        $resultWordEsc = mysqli_real_escape_string($connection, $resultWord);
        $doesWordExistAlready = "select wordid, notes from words where word = '$resultWordEsc'";

        $result = mysqli_query($connection, $doesWordExistAlready) or die($doesWordExistAlready);

        if (!$result) {
            die("ERROR!<br />Query is: $doesWordExistAlready<br />Error is: " . mysqli_error
                ());
        }

        // Set number to how many records the query retrieved
        $number = mysqli_num_rows($result);
        // Just get first row from array
        $resultarray = mysqli_fetch_assoc($result);
        $description = $resultarray["notes"];
        $wordId = $resultarray["wordid"];

        // Now get time from videowords file
        $query_wordtimes = "select time from videowords where wordid ='$wordId' and videos_videoid = '$videoToEdit'";
        $result_wordtimes = mysqli_query($connection, $query_wordtimes) or die($query_wordtimes);

        if (!$result_wordtimes) {
            die("ERROR!<br />Query is: $query_wordtimes<br />Error is: " . mysqli_error());
        }
        $resultarray_wordtimes = mysqli_fetch_assoc($result_wordtimes);
        $wordTime = $resultarray_wordtimes["time"];

        // Only add word if it has't been added already
        if ($number == 0) {
            // Word not in database . Now check if it is in $wordsAddedThisTime, i.e. added as part of this batch
            // of words already
            if (!in_array($resultWord, $wordsAddedThisTime)) {
                // Add this word to the words added in this batch array
                $wordsAddedThisTime[] = $resultWord;
                // Increase the unique words counter
                ++$uniqueWordsCount;
                // word isn't in newsordsarray already so add it with newword set to checked
                $newWords[$WordCount++] = array("word" => $resultWord, "newword" => "checked",
                    "sequence" => ++$Sequence, "notes" => $description, "time" => $wordTime);

            } else {
                // word is not in database but is in array so will be added twice if we added it here.
                // Add word without the checked value and do not increase unique word counter
                $newWords[$WordCount++] = array("word" => $resultWord, "newword" => " ",
                    "sequence" => ++$Sequence, "notes" => $description, "time" => $wordTime);

            }


        } else {            {
                $wordsAddedThisTime[] = $resultWord;
                if (!in_array($resultWord, $wordsAddedThisTime)) {
                    ++$uniqueWordsCount;
                }

                // word is in database already so don't set checked for word
                $newWords[$WordCount++] = array("word" => $resultWord, "newword" => " ",
                    "sequence" => ++$Sequence, "notes" => $description, "time" => $wordTime);
            }


        }

    }

}

function updatevideoid($deleteEditVideo, $videoId, $connection)
{
    $query = "update videos set videoid = $deleteEditVideo where videoid = $videoId";
    $result = mysqli_query($connection, $query);

    if (!$result) {
        die("ERROR!<br />Query is: $query<br />Error is: " . mysqli_error());
        return false;
    }

}

function updatevideowordids($deleteEditVideo, $videoId, $connection)
{
    $query = "update videowords set videos_videoid =$deleteEditVideo where videos_videoid = $videoId";
    $result = mysqli_query($connection, $query);

    if (!$result) {
        die("ERROR!<br />Query is: $query<br />Error is: " . mysqli_error());
        return false;
    }

}


function deletevideo($videoID, $connection)
{
    // Delete video file and associated words


    // Create msqli script to delete video record

    $videoToDelete = "delete from videos where videoid = '$videoID'";


    $result = mysqli_query($connection, $videoToDelete);

    if (!$result) {
        die("ERROR!<br />Query is: $deleteVideo<br />Error is: " . mysqli_error());
        return false;
    } else {
        // Setup an array of WordIds that need to be deleted
        $wordIDsToDelete = array();
        // Setup query to get all wordids from videowords for selected videoid
        $query = "select wordid from videowords where videos_videoid = $videoID";
        $result = mysqli_query($connection, $query);
        if (!$result) {
            die("ERROR!<br />Query is: $query<br />Error is: " . mysql_error());
            return false;
        } else {
            // Loop round result and populate wordIdsToDelete with wordids
            while ($row = mysqli_fetch_assoc($result)) {
                $wordIDsToDelete[] = $row['wordid'];

            }
            // Now delete all entries in videowords for the selected videoid
            $query = "Delete from videowords where videos_videoid = '$videoID'";
            $result = mysqli_query($connection, $query);
            if (!$result) {
                die("ERROR!<br />Query is: $query<br />Error is: " . mysql_error());
                return false;
            } else {

                // Now delete all words with the same wordid as the wordIDsToDelete array as long as
                // there are no other videos linked to that word id
                foreach ($wordIDsToDelete as $wordId) {
                    $query = "select * from videowords where wordid = '$wordId'";
                    $result = mysqli_query($connection, $query);
                    if ($result) {
                        // Only interested in executing the delete if the query was successful
                        // Delete word from words if the result of the previous query is zero
                        $number = mysqli_num_rows($result);
                        if ($number < 1) {
                            $query = "delete from words where wordid = '$wordId'";
                            $result = mysqli_query($connection, $query);
                            if (!$result) {
                                die("ERROR!<br />Query is: $query<br />Error is: " . mysql_error());
                                return false;
                            }

                        }


                    }

                }
                return true;


            }

        }
    }
}
?>