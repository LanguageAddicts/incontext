<?php
session_start();
require("page_elements.php");
require("db_functions.php");


					

//competition submission page
openPage();
drawHead("Language Addicts English - Submit Video");
drawBody();
closePage();

function drawBody()
{
//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
//getWantedPhrases()
drawContent();
//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
//drawTiming();
//list of section functions here
submissionSection();
echo "</div><!--end of content-->";
}

function drawTiming()
{
echo<<<EOF
<div class="shortwideblock"  id="timing">
<p>The contest is now closed for submissions.</p> 
<p>If you have a video which meets our <a href="comp.php" >guidelines</a> that you think we'll like, please <a href="contact.htm">contact us</a>. If we like it we'll pay you $2 for it.</p>
</div>
EOF;
}

function submissionSection()
{
$name=$_SESSION['name'];
$file=$_SESSION['file'];
$email=$_SESSION['email'];
$url=$_SESSION['url'];
$sceneid=$_SESSION['sceneid'];
$prodnotes=$_SESSION['prodnotes'];
echo<<<EOF
<div  id="comp_body">       
<div class="shortwideblock" id="submission">
<p>Before you submit, have you read the <a href="comp.php" >guidelines?</a></p>
<form action="comp_process.php" method="post" enctype="multipart/form-data">

<label for="file">File*:</label>
 <br>
<input type="file" name="file" id="file" size="120" value="{$file}">
 <br>
 <br>
 <label for="name">Name*:</label>
 <br>
<input type="text" name="name" size="50" value="{$name}">
<br>
<br>
 <label for="email">Email*:</label>
 <br>
<input type="email" name="email" size="50" value="{$email}">
<br>
<br>
EOF;
//build drop down of current scenarios
echo<<<EOF
<label for="sceneid">Scene you are uploading*:</label>
 <br>
<select name="sceneid" width="100">
EOF;
drawPhraseDropDown();

echo<<<EOF
<br>
<label for="prodnotes">(optional) Any additional notes you would like to add to your submission</label>
 <br>
<input type="text" name="prodnotes" size="100" value="{$prodnotes}">
<br>
<br>
<label for="url">(optional) Linkback URL - we will place this link next to your work in our gallery. Please enter a full URL i.e. http://www.youtube.com/mychannel</label>
 <br>
<input type="url" name="url" size="100" value="{$url}">
<br>
<br>
 <br>
<p>by submitting this file I agree to the <a href="comp_rules.php">terms</a> of the competition</p>
<input type="submit" value="Submit">
</form>
EOF;

if (isset($_SESSION['formerror']))
 {
 $error =$_SESSION['formerror'];
echo "<p class=\"submission_error\">$error</p><br>";
}
echo <<<EOF
<p>Having trouble submitting your file? <a href="contact.htm">contact us</a></p>
</div>
</div><!--end of comp body-->
EOF;
}
function drawPhraseDropDown()
{
//open db
global $connection; //set up
opendb(); //open the database (db_functions.php)
$phraselist = getWantedPhrases();  								//get list of wanted words
mysqli_close($connection);
	while($phrase = mysqli_fetch_array($phraselist))
	{
		$wantedPhrases[] = $phrase;									//store the sql result in working array
	}
$total = count($wantedPhrases);
	
foreach($wantedPhrases as $wantedPhrase)							//add contributor details to list
		{
    
    $phraseID = $wantedPhrase["PHRASEID"];
    $printPhrase=stripslashes($wantedPhrase["PHRASE"]);
		echo "<option value=\"{$phraseID}\">{$printPhrase}</option>";

		}	
	echo "<option value=\"-1\">**Phrase not showing**</option>";
echo"</select>";
echo"<br>";
}
return
?>