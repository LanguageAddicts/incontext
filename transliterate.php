<?php
/**
 * @author Timothy Clements
 * @copyright 2012
 */
ini_set('default_charset', 'UTF-8');
/* Display current internal character encoding */
mb_internal_encoding("UTF-8");
$check = mb_internal_encoding();

function mail_utf8($to, $subject = '(No subject)', $message = '', $header = '') {
  $header_ = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n";
  mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=', $message, $header_ . $header);
}
 
 function transliterate($replaced)
 {
$string1 = "αυ";
$string2 = "ρ";
$original = $replaced;	
$length = mb_strlen($replaced);
$replaced = mb_strtolower($replaced, "UTF-8");
$replaced = mb_ereg_replace("τζ", "dz", $replaced);
$replaced = mb_ereg_replace("τσ", "ts", $replaced);
$replaced = mb_ereg_replace("γγ", "ng", $replaced);
// Νeed to make all double consonants that don't appear at beginging
// of the word to single consonants as their sound is the same
$pos = mb_strpos($replaced, "ββ");
if ($pos > 0)
{
    $replaced = mb_ereg_replace("ββ", "β", $replaced);
}
$pos = mb_strpos($replaced, "λλ");
if ($pos > 0)
{
    $replaced = mb_ereg_replace("λλ", "λ", $replaced);
}
$pos = mb_strpos($replaced, "μμ");
if ($pos > 0)
{
    $replaced = mb_ereg_replace("μμ", "μ", $replaced);
}
$pos = mb_strpos($replaced, "σσ");
if ($pos > 0)
{
    $replaced = mb_ereg_replace("σσ", "σ", $replaced);
}
// The diphthong "ευ" has two sounds depending on which letter follows it
// hence need to find out what the next letter is
$exists = mb_ereg ("ευ", $replaced);
if ($exists = true)
{
 	$pos = mb_strpos($replaced, "ευ");
    $NextLetter = mb_substr($replaced, $pos + 2, 1);
    // Check these letters first θ, κ, ξ, π, σ, τ, φ, χ or ψ,
    if ($NextLetter == "θ" || $NextLetter == "κ" || $NextLetter == "ξ" || $NextLetter == "π" || 	 	 $NextLetter == "σ" || $NextLetter == "ς" || $NextLetter == "τ" || $NextLetter == "φ" || 	  	  $NextLetter == "χ" || $NextLetter == "ψ")
		{
	  	  	$replaced = mb_ereg_replace("ευ", "ef", $replaced);
		} 
	else
		{
			// Wasn't any of above, so.....
			$replaced = mb_ereg_replace("ευ", "ev", $replaced);
		}
}

// Repeat above code for accented "εύ"
$exists = mb_ereg ("εύ", $replaced);
if ($exists = true)
{
	$pos = mb_strpos($replaced, "εύ");
    $NextLetter = mb_substr($replaced, $pos + 2, 1);
    // Check these letters first θ, κ, ξ, π, σ, τ, φ, χ or ψ,
    if ($NextLetter == "θ" || $NextLetter == "κ" || $NextLetter == "ξ" || $NextLetter == "π" ||	 		 $NextLetter == "σ" || $NextLetter == "ς" || $NextLetter == "τ" || $NextLetter == "φ" ||
		$NextLetter == "χ" || $NextLetter == "ψ")
		{
			// Wasn't any of above, so.....
    		$replaced = mb_ereg_replace("εύ", "EF", $replaced);
		} 
	else
		{
			$replaced = mb_ereg_replace("εύ", "EV", $replaced);
		}
}

// The diphthong "αυ" has two sounds depending on which letter follows it
// hence need to find out what the next letter is
$exists = mb_ereg ("αυ", $replaced);
if ($exists = true)
{
	$pos = mb_strpos($replaced, "αυ");
	$NextLetter = mb_substr($replaced, $pos + 2, 1);	
	// Check these letters first θ, κ, ξ, π, σ, τ, φ, χ or ψ, 
	if ($NextLetter == "θ" || $NextLetter == "κ" || $NextLetter == "ξ" || $NextLetter == "π" || 	     $NextLetter == "σ" || $NextLetter == "ς" || $NextLetter == "τ" || $NextLetter == "φ" ||          $NextLetter == "χ" || $NextLetter == "ψ")
		{
   			$replaced = mb_ereg_replace("αυ", "af", $replaced);
		}
	else
		{
	    	// Wasn't any of above, so.....
			$replaced = mb_ereg_replace("αυ", "av", $replaced);
		}
}
// Repeat above code for accented "αύ"
$exists = mb_ereg ("αύ", $replaced);
if ($exists = true)
{
	$pos = mb_strpos($replaced, "αύ");
	$NextLetter = mb_substr($replaced, $pos + 2, 1);	
	// Check these letters first θ, κ, ξ, π, σ, τ, φ, χ or ψ, 
	if ($NextLetter == "θ" || $NextLetter == "κ" || $NextLetter == "ξ" || $NextLetter == "π" ||          $NextLetter == "σ" || $NextLetter == "ς" || $NextLetter == "τ" || $NextLetter == "φ" || 	      $NextLetter == "χ" || $NextLetter == "ψ")
    	{
        	$replaced = mb_ereg_replace("αύ", "AF", $replaced);
        }
    else
    	{
    		// Wasn't any of above, so.....
            $replaced = mb_ereg_replace("αύ", "AV", $replaced);
        }
}

// The pronunciation of "γχ" changes depending on whether it 
// is preceeded by a light vowel or not
// In IPA terms "γχ" is pronounced /ŋç/ before light vowels
// and /ŋx/ before any over sound
// /ŋ/ is like the ng in sing. /ç/ is like the "h" in hew or human.
// /x/ is like the ch in loch
$exists = mb_ereg ("γχ", $replaced);
if ($exists = true)
// Get next letter so can check it is a dark or light vowel
{
	$pos = mb_strpos($replaced, "γχ");
	$NextLetter = mb_substr($replaced, $pos + 2, 1);
    // First check light, single letter vowels, i.e. "ε", "ι" and "υ" (non-accented)
    if ($NextLetter == "ε" || $NextLetter == "ι" || $NextLetter == "υ")
        {
        	$replaced = mb_ereg_replace("γχ", "nh", $replaced);
        }
    else if ($NextLetter == "έ" || $NextLetter == "ί" || $NextLetter == "ύ")
    // need to check accented letters too
        {
            $replaced = mb_ereg_replace("γχ", "nh", $replaced);
        }
    // Now get next two letters to check vowel combinations
    $NextLetters = mb_substr($replaced, $pos + 2, 2);
	if ($NextLetters == "αί" || NextLetter == "οί")
        { 
        	$replaced = GeneratedText.Replace("γχ", "nh");
        }

    // Now check non accented vowel combinations
    else if (NextLetter == "αι" || NextLetter == "οι")
        {
           	$replaced = mb_ereg_replace("γχ", "nh", $replaced);
        }
    else
    // If followed by anything other than a light vowel sounds like "nch"
        {
        	$replaced = mb_ereg_replace("γχ", "nch", $replaced);
    	}
}
// The pronunciation of "γκ" changes depending on whether it is
// at the start of the word or not so need to find where it is
$exists = mb_ereg ("γκ", $replaced);
if ($exists = true)
{   // is "γχ" at the beginning?
	$pos = mb_strpos($replaced, "γκ");
	if ($pos = 0)
    	{
        	$replaced = mb_ereg_replace("γκ", "g", $replaced);
        }
    else
        // "γχ" IS NOT at start of string
    	{
           	$replaced = mb_ereg_replace("γκ", "ng", $replaced);
        }
}

// The pronunciation of "ντ" changes depending on whether it is
// at the start of the word or not so need to find where it is
$exists = mb_ereg ("ντ", $replaced);
if ($exists = true)
{	// Is "ντ" and at beginning?
	$pos = mb_strpos($replaced, "ντ");
	if ($pos = 0)
    	{
        	$replaced = mb_ereg_replace("ντ", "d", $replaced);
        }
    else
        // "ντ" IS NOT at start of string
    	{
           	$replaced = mb_ereg_replace("ντ", "nd", $replaced);
        }
}
// The pronunciation of "μπ" changes depending on whether it is
// at the start of the word or not so need to find where it is
$exists = mb_ereg ("μπ", $replaced);
if ($exists = true)
{	// Is "μπ" and at beginning?
	$pos = mb_strpos($replaced, "μπ");
	if ($pos = 0)
    	{
        	$replaced = mb_ereg_replace("μπ", "b", $replaced);
        }
    else
        // "μπ" IS NOT at start of string
    	{
           	$replaced = mb_ereg_replace("μπ", "mb", $replaced);
        }
}
// χ has two sounds depending on which letter/s follow/s it
// hence need to find out what the next letter/s is/are
$exists = mb_ereg ("χ", $replaced);
if ($exists = true)
// Get next letter so can check it is a dark or light vowel
{
	$pos = mb_strpos($replaced, "χ");
	$NextLetter = mb_substr($replaced, $pos + 1, 1);
// First check single light vowels, i.e. "ε", "ι", "η" or "υ" (accented and non-accented)
    if ($NextLetter == "ε" || $NextLetter == "ι" || $NextLetter == "υ" || $NextLetter == "η")
        {
        	$replaced = mb_ereg_replace("χ", "h", $replaced);
        }
// need to check accented letters too
    else if ($NextLetter == "έ" || $NextLetter == "ί" || $NextLetter == "ύ" || $NextLetter == "ή")
        {
           	$replaced = mb_ereg_replace("χ", "h", $replaced);
        }
// Now need to retrieve next two letter and check vowel combinations that follow
// Need to check that next 2 characters don't extend beyond end of word
    $NextLetters = mb_substr($replaced, $pos + 1, 2);
    if ($NextLetters == "αί" || $NextLetter == "οί")
        {
           	$replaced = mb_ereg_replace("χ", "h", $replaced);
        }
// Now check non accented vowel combinations
    else if ($NextLetters == "αι" || NextLetter == "οι")
        {
        	$replaced = mb_ereg_replace("χ", "h", $replaced);
        }
	else
// Wasn't a light vowel so must be a dark vowel, i.e. "α", "ο" or "ου"
        {
        	$replaced = mb_ereg_replace("χ", "ch", $replaced);        
        }
}

// γ has two sounds depending on which letter/s follow/s it
// hence need to find out what the next letter/s is/are
$exists = mb_ereg ("γ", $replaced);
if ($exists = true)
// Get next letter so can check it is a dark or light vowel
{
	$pos = mb_strpos($replaced, "γ");
	$NextLetter = mb_substr($replaced, $pos + 1, 1);
// First check light vowels, i.e. "ε", "ι", "οι", "αι" or "υ" (accented and non-accented)
    if ($NextLetter == "ε" || $NextLetter == "ι" || $NextLetter == "υ")
    	{
    		$replaced = mb_ereg_replace("γ", "y", $replaced);
        }
// need to check accented letters too
	else if ($NextLetter == "έ" || $NextLetter == "ί" || $NextLetter == "ύ")
        {
        	$replaced = mb_ereg_replace("γ", "y", $replaced);
        }
// Now need to retrieve next two letters and check vowel combinations that follow
    $NextLetters = mb_substr($replaced, $pos + 1, 2);

    if ($NextLetters == "αί" || $NextLetters == "οί")
        {
        	$replaced = mb_ereg_replace("γ", "y", $replaced);
        }
// Now check non accented vowel combinations
    else if ($NextLetters == "αι" || $NextLetters == "οι")
    	{
    		$replaced = mb_ereg_replace("γ", "y", $replaced);
        }
// Wasn't a light vowel so must be a dark vowel, i.e. "α", "ο" or "ου"
    else
    	{
    		$replaced = mb_ereg_replace("γ", "gh", $replaced);
        }
}
$replaced = mb_ereg_replace("ψ", "ps", $replaced);
$replaced = mb_ereg_replace("φ", "f", $replaced);
$replaced = mb_ereg_replace("τ", "t", $replaced);
//TODO:Need to distinguish betwen ss and s 
$replaced = mb_ereg_replace("σ", "ss", $replaced);
$replaced = mb_ereg_replace("ς", "ss", $replaced);
$replaced = mb_ereg_replace("ρ", "r", $replaced);
$replaced = mb_ereg_replace("π", "p", $replaced);
$replaced = mb_ereg_replace("ξ", "ks", $replaced);
$replaced = mb_ereg_replace("ν", "n", $replaced);
$replaced = mb_ereg_replace("μ", "m", $replaced);
$replaced = mb_ereg_replace("λ", "l", $replaced);
$replaced = mb_ereg_replace("κ", "k", $replaced);
$replaced = mb_ereg_replace("θ", "th", $replaced);
$replaced = mb_ereg_replace("ζ", "z", $replaced);
$replaced = mb_ereg_replace("δ", "th", $replaced);
$replaced = mb_ereg_replace("β", "v", $replaced);
$replaced = mb_ereg_replace("αι", "eh", $replaced);
$replaced = mb_ereg_replace("αί", "EH", $replaced);
$replaced = mb_ereg_replace("ει", "ee", $replaced);
$replaced = mb_ereg_replace("εί", "EE", $replaced);
$replaced = mb_ereg_replace("οι", "ee", $replaced);
$replaced = mb_ereg_replace("οί", "EE", $replaced);
$replaced = mb_ereg_replace("υι", "ee", $replaced);
$replaced = mb_ereg_replace("υί", "EE", $replaced);
$replaced = mb_ereg_replace("ου", "oo", $replaced);
$replaced = mb_ereg_replace("ού", "OO", $replaced);
$replaced = mb_ereg_replace("ο", "o", $replaced);
$replaced = mb_ereg_replace("ό", "O", $replaced);
$replaced = mb_ereg_replace("ω", "o", $replaced);
$replaced = mb_ereg_replace("ώ", "O", $replaced);
$replaced = mb_ereg_replace("ι", "ee", $replaced);
$replaced = mb_ereg_replace("ί", "EE", $replaced);
$replaced = mb_ereg_replace("η", "ee", $replaced);
$replaced = mb_ereg_replace("ή", "EE", $replaced);
$replaced = mb_ereg_replace("υ", "ee", $replaced);
$replaced = mb_ereg_replace("ύ", "EE", $replaced);
$replaced = mb_ereg_replace("ε", "eh", $replaced);
$replaced = mb_ereg_replace("έ", "EH", $replaced);
$replaced = mb_ereg_replace("α", "a", $replaced);
$replaced = mb_ereg_replace("ά", "A", $replaced);
$replaced = mb_ereg_replace("μπτ", "mt", $replaced);
return $replaced;
}
?>