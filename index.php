<?php
session_start();

require("db_functions.php");
require("drawMainPage.php");
require("common/trace_functions.php");

traceStart();											//start the trace file
error_reporting(E_ALL);
global $connection; //set up
opendb(); //open the database (db_functions.php)
selectVideo(); //decide what we will show based on the user request
//mysqli_close($connection);//close the database

function selectVideo()
{
	switch (true)
	{
		case (isset($_GET['vid'])):
		{// a video has been selected
		$videoID= filter_input(INPUT_GET, "vid", FILTER_SANITIZE_STRING);
		trace("index.php: video set:", $videoID);  //run trace
		if (is_numeric($videoID)){
		getVidDetails($videoID);
		}
		else
		{}
		break;
		}	
		case( isset($_GET["word"])):
		{//a word has been selected
		$wordid = filter_input(INPUT_GET, "word", FILTER_SANITIZE_STRING);
		$_SESSION['action'] = "select";
		$select0 = $_POST["Select"];
		$_SESSION['wordid'] = $select0; 
		$videoID = chooseAndSave($wordid);;
		$posterID = $videoID;
		$author = "Unknown(index.php)";
		trace("index.php: word set:", $wordid);  //run trace
		drawMainPage($videoID, $posterID, $author);
		break;
		}
		case( isset($_GET["author"])):
		{//a word has been selected
		$authorID = filter_input(INPUT_GET, "author", FILTER_SANITIZE_STRING);
		$videoID = getAuthorLatest($authorID);
		$posterID = $videoID;
		$authorName = getAuthorName($authorID);
		unset($_SESSION['authorPageNo']);
		unset($_SESSION['authorEntries']);
		$_SESSION['authorid']=$authorID;
		trace("index.php: Author set:", $authorID);  //run trace
		drawMainPage($videoID, $posterID, $authorName);
		break;
		}
		default:
		{//no videos found and load visual cue vid
		trace("index.php: no default set");  //run trace
		getLatest();
		break;
		}
	}
}
function getLatest()
{
//$videoID = 153;
$result = getLatestVideos();
$videorow = mysqli_fetch_array($result);
$videoID = 0;
if (mysqli_num_rows($result)>0 )
{
$videoID = $videorow["VIDEOID"];
}
$posterID = $videoID;
$author = getAuthorName($videorow["CONTRIBID"]);
$_SESSION['authorid']=$videorow["CONTRIBID"];
trace("Get latest vid selected videoID:", $videoID);  //run trace
drawMainPage($videoID, $posterID, $author);
}

function getAuthorLatest($authorID)
{
$videoID = -1;
$result = getAuthorLatestVideos($authorID);
$videorow = mysqli_fetch_array($result);
if (mysqli_num_rows($result)>0 )
{
$videoID = $videorow["VIDEOID"];
}
return $videoID;
}

function getVidDetails($videoID)
{
	$posterID = $videoID;
	$videorow = getVideoFiledetails($videoID);
	if ($videorow != null)
	{
	$author = getAuthorName($videorow["CONTRIBID"]);
	$_SESSION['authorid']=$videorow["CONTRIBID"];
	unset($_SESSION['authorEntries']);
	trace("index.php getVidDetails videoID:", $videoID);  //run trace
	drawMainPage($videoID, $posterID, $author);
	}
}



?>