<?php
/**
 * @author Timothy Clements
 * @copyright 2010
 */
 
 
session_start();

// Draw the textarea for scenario and the header columns for the table
echo
"<body>
<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
<h1> Video details:$scenario (confirm.inc)</h1>
 
<table width=\"1043\" border=\"0\">
  <tr>
    <td width=\"70\" align=\"center\">Sequence</td>
    <td width=\"54\" align=\"center\">Word</td>
    <td width=\"172\" align=\"center\">Time in video</td>
    <td width=\"92\" align=\"center\">New word?</td>
    <td width=\"390\" align=\"center\">Notes</td>
  </tr>";
  
// Now loop round the new words adding rows to the table. Add an edit box for time in video and check box for new word in each row.

for ($i = 0; $i <= $newWordCount - 1; $i++) {

    // As a loop always starts at zero, need to add 1 to $i for sequence
	$Sequence = $i + 1;

    echo 
	"<tr>
	      	<td align=\"center\">" .$newWords[$i]['sequence']. "</td>
    		<td align=\"center\">" .$newWords[$i]['word']. "</td>
    		<td align=\"center\">" .$newWords[$i]['wordtime']. "</td>
    		<td align=\"center\">" .$newWords[$i]['newword']. "</td>
    		<td align=\"center\">" .$newWords[$i]['notes']. "</td>
    </tr>\n";
}

// Finish the table and add the add button

echo "
</table>
	
</form>
<a href=\"http://www.english.languageaddicts.com/ShowAllVids.php\">Show all vids - online</a>
<br>
<a href=\"http://localhost/incontext/ShowAllVids.php\">Show all vids - local</a>
<br>
<a href=\"http://localhost/incontext/submissions.php\">Submissions - local</a>
</body>
</html>" 
?>