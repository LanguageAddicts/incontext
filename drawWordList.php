<?php
require("db_functions.php");
global $connection; 											//set up db connection using global variable
opendb(); 
$sortorder=substr($_POST["sort"],5); 												//get phrase from POST variable set in javascript

$wordlist = getWantedWords(true,$sortorder);  								//get list of wanted words

	while($word = mysqli_fetch_array($wordlist))
	{
		$wantedWords[] = $word;									//store the sql result in working array
	}
$total = count($wantedWords);
echo"<br>";
echo<<<EOD
<form id="sort_order">
<input type="radio" id="radioWordsAtoZ" name="sorttype" value="AtoZ"  >A to Z
<input type="radio" id="radioWordsZtoA" name="sorttype" value="ZtoA"  >Z to A
<input type="radio" id="radioWordsOldestFirst" name="sorttype" value="oldestfirst" >oldest first
<input type="radio" id="radioWordsNewestFirst" name="sorttype" value="oldestfirst">newest first
<input type="radio" id="radioWordsMostLiveFirst" name="sorttype" value="MostLivefirst">Most live first
<input type="radio" id="radioWordsLeastLiveFirst" name="sorttype" value="LeastLivefirst">Least live first
</form>
<br>
EOD;
echo "<table class=\"wordlisttable\">";
echo "<tr class=\"tableRowStyle1\"><td>Word</td><td>delete?</td><td>in live vids</td><td>in phrases</td><td>notes</td></tr>";
$group = null;
$style=null;
$groupNumber = 0;
$threshhold = 300;  												// this is the threshhold number of existing vids to exclude new entries
foreach($wantedWords as $wantedWord)							//add contributor details to list
		{
		if ($wantedWord["videoCount"] <= $threshhold)
			{ /*
				if ($group != $wantedWord["GROUP"])
				{	
					++$groupNumber;
					$style=setBackgroundStyle($groupNumber);
					echo "<tr class=\"$style\">";
					echo "<td class=\"WantedWordGroup\">{$wantedWord["GROUP"]} </td>";
					$group = $wantedWord["GROUP"];
				}
				else
				{
					echo "<tr class=\"$style\">";
					echo "<td/>";
				}
        */
        echo "<tr>";
				$cleanedUpWord = stripslashes($wantedWord["WORD"]);
				echo "<td class=\"WantedWordPhrase\">{$cleanedUpWord}</td>"; 
				$deleteID="deleteword-".$wantedWord["WORDID"];
				
				echo "<td><a href=\"#\" id={$deleteID}><img src=\"images/delete.png\"></a></td>"; //delete icon
				echo "<td>{$wantedWord["videoCount"]}</td>";
        $phraseCount = getPhrasesByWord($wantedWord["WORD"]);
				echo "<td> {$phraseCount} </td>"; //empty buffer column
				echo "<td class=\"WantedWordComments\" style=\"font-style:italic;font-size:small\">{$wantedWord["COMMENTS"]}</td>";
				echo "</tr>";
				}
			}	
echo "</table>";
mysqli_close($connection);										//close the database connection

function setBackgroundStyle($groupNumber)
{
if ($groupNumber % 2 == 0)
//even number
{$style="tableRowStyle1";}
else
//odd number
{$style="tableRowStyle2";}
return $style;
}
?>