<?php
/**
 * @author Timothy Clements
 * @copyright 2011
 */
session_start();
// Draw header colu	mns for showall videos table
echo "<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">
<link href=\"http://www.languageaddicts.com/css/styles.css\" rel=\"stylesheet\" media=\"all\">
<title>Show all videos</title>
</head>
<body>

<h1> Available videos</h1>
<table width=\"1043\" border=\"1\">
  <tr>
    <td lign=\"left\">Video id</td>
    <td align=\"left\">Video name</td>
    <td align=\"left\">Thumb</td>
    <td align=\"left\">Words</td>
    <td align=\"left\">Scenario</td>
    <td align=\"left\">Delete?</td>
    <td align=\"left\">Edit?</td>
    <td align=\"left\">Upload?</td>
    <td align=\"left\">Attribute?</td>
    <td align=\"left\">Thumb?</td>
    <td align=\"center\">
    <form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
    	<input type=\"submit\" name=\"button\" value=\"Add new vid?\"/>
    	</form>
    	
    </td>
  </tr>";

echo NoVideos;
// Now loop round the new words adding rows to the table. Add an edit box for time in video and check box for new word in each row.

for ($i = 0; $i <= $NoVideos -1; $i++)
 {   
 	++$Item;
    echo "<tr>
	      	<td align=\"left\">" . $Videos[$i][0] . "</td>
    		<td align=\"left\">" . $Videos[$i][1] . "</td>
    		<td align=\"left\"><a href=\"#\" ><img src=\"" .$Videos[$i][10] . "\" width=\"40\" alt=\"" . $Videos[$i][10] . "\" /></a>" ."</td>
    		<td bgcolor=\"" . $Videos[$i][9] . "\" align=\"left\">" . $Videos[$i][2] . "</td>
    		<td align=\"left\">" . $Videos[$i][3] . "</td>
      		<td align=\"left\">
    			<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
    			<input type=\"hidden\" name=\"VideoToDelete\" value=\"" .$Videos[$i][0] . "\"/>
    			<input type=\"submit\" name=\"button\" id=\"delete\" value=\"delete\"/>
    			</form>
      		</td>
    		<td align=\"left\">
    			<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
    			<input type=\"hidden\" name=\"VideoToEdit\" value=\"" .$Videos[$i][0] . "\"/>
    			<input type=\"submit\" name=\"button\" id=\"edit\" value=\"edit\"/>
    			</form>
      		</td>
      		<td align=\"left\">
    			<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
    			<input type=\"hidden\" name=\"VideoToUpload\" value=\"" .$Videos[$i][0] . "\"/>
    			<input type=\"submit\" name=\"button\" id=\"upload\" value=\"upload\"/>
    			</form>
      		</td>
      		<td align=\"left\">
    			<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
    			<input type=\"hidden\" name=\"VideoToAttribute\" value=\"" .$Videos[$i][0] . "\"/>
    			<input type=\"submit\" name=\"button\" id=\"attribute\" value=\"attribute\"/>
    			</form>
      		</td>
      		<td align=\"left\">
    			<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
    			<input type=\"hidden\" name=\"ThumbToUpload\" value=\"" .$Videos[$i][0] . "\"/>
    			<input type=\"submit\" name=\"button\" id=\"thumb\" value=\"thumb\"/>
    			</form>
      		</td>
    </tr>\n";
}
// Finish the table and add the add button
echo "Total videos: $Item";
echo "	
</table>
</body>
</html>"
?>