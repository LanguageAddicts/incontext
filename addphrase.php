<?php
require("db_functions.php");
$phrase=$_POST["word"]; 
$phraseID=$_POST["phraseid"]; 												//get phrase from POST variable set in javascript
$notes=$_POST["notes"]; 
$bPhrase = $_POST["phrase"];
$theme = $_POST["theme"];
$points = $_POST["points"];
$phrase = stripslashes($phrase);
$trimmedPhrase = trim( preg_replace( "/[^0-9a-z']+/i", " ", $phrase ) );
$words= explode(" ",$trimmedPhrase);											//put all individual words into an array
global $connection; 													//set up db connection using global variable
opendb(); 
foreach ($words as $individualWord) 
{

	if (getWordListEntryByName($individualWord))						//is word already in list
	{	
		echo "Word already in list: ".$individualWord."<br>";
	}
	else
	{											
		echo insertWord($individualWord)."<br>";						//new word, add it
	}
}
//create entry in phrase file
if ($bPhrase == "Y")
{
if ($phraseID == null)
{
$PhraseID = insertPhrase($phrase,$notes);  
echo "Phrase inserted: ".$phrase."<br>";
}
else
{
$result = updatePhrase($phrase,$notes,$phraseID,$theme,$points);
echo "Phrase update: ".$result."<br>";
}
}          
mysqli_close($connection);	
?>