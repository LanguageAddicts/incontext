<?php
session_start();
// Framework loader

require_once('framework/loader.php');

$loader = new loader();



// Registry, store objects

$registry = core_registry::getInstance();

$registry->store('loader', $loader);



if(isset($_GET['view'])) {

  $view  = $_GET['view'];  
  echo $view;
} else if(!isset($view)) {

  $view = 'default';

}



$logicFile = "{$loader->get_webroot_path()}/framework/logic/{$view}.php";



if(!file_exists("{$logicFile}")) {



  $page = new page_template($view);

  $content = new logic_default($view);

  $page->add_content($content->get_html());

  $page->create_page();

  $formattedPage = $page->get_page();

  //special format for transliterator page

   $page = substr($_SERVER['PHP_SELF'],strrpos($_SERVER['PHP_SELF'],"/"));
   
    switch ($page)
    {
    case "/greek_pronunciation.htm":
   
      $replacestring1="@@@@@@@@@@"; 

      $replacestring2="##########"; 

      $question=null;

      $answer=null;

      $tableclass="resulttablehide";

          if (isset($_GET['output']) && $_GET['output'] !="") 

          {$answer=$_GET['output'];}

          if (isset($_GET['input']) && $_GET['input'] !="") 

          {$question=$_GET['input'];}

      $formattedcontent = str_replace($replacestring2, $question, $formattedPage );

      $formattedcontent = str_replace($replacestring1, $answer, $formattedcontent);

      //show or hide the result table

      if ($question != null)

      {$tableclass = "resulttableshow";}

      $formattedcontent = str_replace("ShowHide", $tableclass, $formattedcontent);

      $formattedPage = $formattedcontent;

      break;
      
      case "/comp_completion.htm":
      //retrieve session data
      $name=$_SESSION['name'];
      $file=$_SESSION['file'];
      $email=$_SESSION['email']; 
      $namestr="@@@@";
      $filestr="XXXX";
      $emailstr="YYYY";
      //swap out the stuff
      $formattedPage = str_replace($namestr, $name, $formattedPage);
       $formattedPage = str_replace($filestr, $file, $formattedPage);
        $formattedPage = str_replace($emailstr, $email, $formattedPage);
      break;
      case "/comp_submit.htm":
      //see if there is an error message to inject
      $formError = $_SESSION['formerror'];
      if ($formError != null)
      {
      $errString = "@@@@";
      $formattedPage = str_replace($errString, $formError, $formattedPage);
      //make the error display visible
      $formattedPage = str_replace("HideError", "ShowError", $formattedPage);
      }
      default:
      //nothing

} 

  echo $formattedPage;

  

} else {



  switch($view) {

  

    case 'default':

      $page = new page_template($view);

      $content = new logic_default($view);

      $page->add_content($content->get_html());

      $page->create_page();

      echo $page->get_page();

    break;

    

    case 'contact':

      $page = new page_template($view);

      $content = new logic_contact($view);

      $page->add_content($content->get_html());

      $page->create_page();

      echo $page->get_page();

    break;

    

    case 'search':

      $page = new page_template($view);

      $content = new logic_search($view);

      $page->add_content($content->get_html());

      $page->create_page();

      echo $page->get_page();

    break;

    

    default:

      $loader->redirect('index.php?view=default');

    break;

    

  }

  

}

?>