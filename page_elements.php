<?php
function openPage()
{
echo <<<EOF
<!DOCTYPE html>
<html lang="en">
EOF;
}

function closePage()
{
echo "</html>";
}
function drawHead($title = "Language Addicts English", $script="js/scripts.js")
{
echo <<<EOF
<head>
	<meta charset="utf-8">	
	<title>{$title}</title>
	<meta name="description" content="teaching the world English since yesterday">	
	<!-- Define a viewport to mobile devices to use - telling the browser to assume that the page is as wide as the device (width=device-width) and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<!-- Add normalize.css which enables browsers to render all elements more consistently and in line with modern standards as it only targets particular styles that need normalizing -->
	<link href="css/normalize.css" rel="stylesheet" media="all">
	<link href="css/normalize-legacy.css" rel="stylesheet" media="all">
	<link href="css/styles.css" rel="stylesheet" media="all">
	<!-- Include the HTML5 shiv print polyfill for Internet Explorer browsers 8 and below -->
	<!--[if lt IE 9]><script src="js/html5shiv-printshiv.js" media="all"></script><![endif]-->
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.2.min.js"></script>
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
	<script src="{$script}"></script>
EOF;
drawSocial();
echo "</head>";
}

function drawSocial()
{
echo"\n<meta property=\"og:description\" content=\"Language Addicts Video Competition Entries\" />\n";
echo"<meta property=\"og:site_name\" content=\"languageaddicts.com\" />\n";
echo"<meta property=\"og:title\" content=\"Learn English through video\" />\n";
$url = "http://www.languageaddicts.com/comp.php";
echo"<meta property=\"og:url\" content=\"{$url}\"/>\n";
$poster = "http://www.languageaddicts.com";
echo"<meta property=\"og:image\" content=\"{$poster}\" />\n";
echo"<meta property=\"og:type\" content=\"article\" />\n";
}

function drawHeader()
{
echo <<<EOF
	<header class="mainHeader">
	  <div id="headerpanel">	
	  <div id="headerlogo">
	  <a href="http://www.languageaddicts.com"><img src="images/logo_tagline.png"></a>
	  </div>
EOF;
drawSocialSharing();
$root = getRoot();
echo "</div>";
echo <<<EOF
		<nav>
			<ul>
				<li><a href={$root}>Home</a></li>
				<li><a href="comp.php">Competition</a></li>
				<li><a href="wordlist.php">Word List</a></li>
				<li><a href="comp_submit.php">Submit a video</a></li>
				<li><a href="legacy_index.php">Legacy site</a></li>
				<li><a href="greek_pronunciation.php">Greek Translitterator</a></li>
			</ul>
		</nav>

	</header>
EOF;
}


function drawSocialSharing()
{
echo <<<EOF
<div class="addthis">
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
						<a class="addthis_button_facebook"></a>
						<a class="addthis_button_twitter"></a>
						<a class="addthis_button_pinterest_share"></a>
						<a class="addthis_button_google_plusone_share"></a>
						<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
						</div>
						<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51fad6571860eb6e"></script>
						<!-- AddThis Button END -->
					</div>
EOF;
}

function drawFooter()
{
$date=date('Y');
echo <<<EOF
	<footer class="mainFooter">
		<p>Copyright LanguageAddicts &copy; <time datetime="$date">$date</time>   <a style="text_decoration:none;color:white;" href="comp_rules.php">contest terms</a></p>
	</footer>
EOF;
}

function drawAnalytics()
{
echo <<<EOF
		<!-- Google analytics code tick-->
	<script src="http://www.google-analytics.com/urchin.js" type="text/javascript"></script>
	<script type="text/javascript">
	_uacct = "UA-1960131-2";
	urchinTracker();
	</script>
EOF;
}

?>