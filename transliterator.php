<?php

require ("transmisc.inc");

require ("transliterate.php");

ini_set('default_charset', 'UTF-8');

/* Display current internal character encoding */

mb_internal_encoding("UTF-8");

$input = $_POST['word'];



$connection = mysqli_connect($host, $user, $passwd, $dbname);



if (!$connection) {

    die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());

} else {

    mysqli_set_charset($connection, "utf8");

    $query = "SELECT generated, syllabilized, modified FROM transliterated WHERE requested = '$input'";



    $result = mysqli_query($connection, $query) or die($query);



    if (mysqli_num_rows($result) == 0) {

        $converted = transliterate($input);

        $add_transliteration =

            "insert into transliterated(requested, generated, modified, lastEdit) values ('$input','$converted', 'NO', NOW())";

        $result = mysqli_query($connection, $add_transliteration) or die($add_transliteration);

        $message = "input: " . $input . "\nOutput: " . $converted;


        //switch off email 04/02/2013
        //mail_utf8("admin@languageaddicts.com", "Greek Transliterator usage", $message);

        //header("location:greek_pronunciation.htm?input=$input&output=$converted");
		$output=$converted;


    } else {

        $row = mysqli_fetch_row($result);

        $generated = $row[0];

        $syllablized = $row[1];

        $modified = $row[2];

        if ($modified == "No") {

            //header("location:greek_pronunciation.htm?input=$input&output=$generated");
			$output = $generated;




        } else {

            //header("location:greek_pronunciation.htm?input=$input&output=$syllablized");
			$output = $syllablized;



        }

    }
echo <<<EOF
<table class=ShowHide>

<tr class=highlightTableRow>

<td><b>Greek</b></td>

<td>{$input}</td>

</tr>

<tr>

<td><b>English</b></td>

<td>{$output}</td>

</tr>
<tr>

<td>

</td>
 		
<td><!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_facebook_like" fb:like:layout="standard"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=undefined"></script>
<!-- AddThis Button END --></td>

</tr>
</table>
EOF;

}

r ?>