<?php
session_start();
require("db_functions.php");



//clear out any lingering values
if(isset($_SESSION['formerror']))
  unset($_SESSION['formerror']);

//get input values
$name=$_POST['name'];
$file=$_FILES["file"]["name"];
$email=$_POST['email'];
$url=$_POST['url'];
$source=$_POST['source'];
$from=$email;
$sceneid=$_POST['sceneid'];
$prodnotes=$_POST['prodnotes'];
$fileError=$_FILES["file"]["error"];

//validate file extension
 $allowed_filetypes = array('mov','mpg','avi','flv','wmv','mp4','mpeg','ogv'); 
 $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION)); // Get the extension from the filename.
 if(!in_array($ext,$allowed_filetypes))
      $_SESSION['formerror'].="File type '$ext' is not valid for this competition.<br>";
 //validate a file has been selected
 if($file==" ")
    $_SESSION['formerror'].="please upload your file.<br>"; 
//validate file
 if ($fileError > 0)
    $_SESSION['formerror'].="file upload error: $fileError).<br>";
	
	
//validate email
  if($email==null)
    $_SESSION['formerror'].="please enter your email.<br>";
//validate email format
  if(!filter_var($email, FILTER_VALIDATE_EMAIL))
    $_SESSION['formerror'].="the email address $email is not valid.<br>";
//validate name
  if($name==null)
    $_SESSION['formerror'].="please enter your name.<br>";
//validate scene
  if($sceneid==null)
    $_SESSION['formerror'].="please enter the scene you created.<br>";

  //store the data in the session for thankyou page
  $_SESSION['name']=$name;
  $_SESSION['file']=$file;
  $_SESSION['email']=$email;
  $_SESSION['sceneid']=$sceneid;
  $_SESSION['url']=$url;
  $_SESSION['prodnotes']=$prodnotes;
  
//if there are no errors, try to write the entry to the database
if (!isset($_SESSION['formerror']))				
	{
//rename the input file to make it unique
$time = time();
$tofile = $from .".". $time .".".$file;
//no errors then write to the database
if (writeToSubmissionFile($email,$name,$tofile,$url,$sceneid,$prodnotes,$file) == false)
$_SESSION['formerror'].="error writing entry to submission database, please contact contest@languageaddicts.com.<br>";
	}	

//if there are any errors (inluding failure to write submission record) then refresh the page incluing the errors
if (isset($_SESSION['formerror']))
	{
echo ('<meta http-equiv="refresh" content="0;url=comp_submit.php">');
	}
else
	{

//complete the submission process now
//move file to correct place
move_uploaded_file($_FILES["file"]["tmp_name"],"upload/pending/" . $tofile);

// mail the chiefs
$to = "contest@languageaddicts.com";
$subject = "Competition entry";
$message = "file (" .$tofile. ") submission received from " .$name. ". Url:" . $url. " Source of business: " .$source;
$headers = "From:" . "contest@languageaddicts.com";
mail($to,$subject,$message,$headers);

//mail the entrant
date_default_timezone_set('GMT');	//set timezone to GMT
$to = $email;
$dateTime =  date('H:ia - d/m/Y');
$subject = "Language Addicts video competition entry";
$message = "Hi " . $name. ",\nYour video(" .$file.") was successfully uploaded at " .$dateTime. " Thanks for entering and good luck. \nThe competition will close on ".get_competition_announcement()." if you're the winner. In the meantime you can increase your chances of winning by entering another video.\nHappy trails, \nThe Language Addicts Team"; 
$headers = "From:" . "contest@languageaddicts.com";
mail($to,$subject,$message,$headers);
//redirect to confirmation page
echo ('<meta http-equiv="refresh" content="0;url=comp_completion.php">');
}

function writeToSubmissionFile($inputemail,$inputname,$inputfilename,$inputurl,$sceneid,$inputprodnotes,$originalfile)
{
//write to entries db
global $connection; //set up
opendb();			//open db
insertSubmission($inputemail,$inputname,$inputfilename,$inputurl,$sceneid,$inputprodnotes,$originalfile,$success);
mysqli_close($connection);						//close the database connection
if ($sucess = true)
{
return true;
}else
{
return false;
}
}

?>