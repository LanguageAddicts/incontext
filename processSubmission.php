<?php
session_start();
require("db_functions.php");
//what have we been asked to do
$action = $name=$_POST['action'];
$recordID = $name=$_POST['recordID'];
//open db
global $connection; //set up
opendb(); 														//open the database 
//read in the record in question
if ($recordID != null)
{
$submission = getSubmission($recordID); 
$submissionRow = mysqli_fetch_array($submission);
switch ($action)
{
	case "rejection_email":
	$message = formatRejectionEmail($submissionRow,"R","display");
	echo $message;
	break;
	
	case "partial_rejection_email":
	$message = formatRejectionEmail($submissionRow,"P","display");
	echo $message;
	break;
	
	case "acceptance_email":
	break;
	
	case "rejection":
	processRejection($submissionRow,"R");
	break;
	
	case "partial_rejection":
	processRejection($submissionRow,"P");
	break;
	
	case "acceptance":
	processAcceptance($submissionRow);
	
	break;
	
	default:
	break;
}
}
mysqli_close($connection);										//close the database connection


function processAcceptance($submissionRow)
{

$filename=$submissionRow["FILENAME"];
$ID=$submissionRow["SUBMISSIONID"];

$status = "A";
//update DB
updateSubmission($ID, $status);
//move file to accepted folder
moveFile($filename,$status);

//send email
sendEmail($submissionRow,$status);
addContributor($submissionRow);
}



function addContributor($submissionRow)
{
	global $connection;
	$name=$submissionRow["NAME"];
	$surname=$submissionRow["SURNAME"];
	$displayname=$submissionRow["DISPLAYNAME"];
	$email=$submissionRow["EMAIL"];
	$url=$submissionRow["URL"];

	$query = "INSERT INTO contributors(email, name, surname,url, displayname) VALUES('$email', '$name', '$surname','$url','$displayname')";
	// Add contributor
	$result = mysqli_query($connection, $query) or die($query);

}

function processRejection($submissionRow,$status)
{
$filename=$submissionRow["FILENAME"];
$ID=$submissionRow["SUBMISSIONID"];
//update DB
updateSubmission($ID, $status);
//move file to rejected folder
moveFile($filename,$status);
//send email
sendEmail($submissionRow,$status);
}

function moveFile($filename,$status)
{
$oldLocation = "upload/pending/".$filename;
switch($status)
{
case "R": //rejected
$newLocation = "upload/rejected/".$filename;
break;
case "P": //partially rejected
$newLocation = "upload/nearly/".$filename;
break;
case "A": //accepted
$newLocation ="upload/accepted/".$filename;
break;
}
if (rename($oldLocation, $newLocation) == false){echo "shit negro";}

}

function formatAcceptanceEmail($submissionRow)
{
	//retrieve row data
	$email=$submissionRow["EMAIL"];
	$ID=$submissionRow["SUBMISSIONID"];
	$name=$submissionRow["NAME"];
	$submitted=$submissionRow["SUBMITTED"];
	$filename=$submissionRow["ORIGINAL_FILENAME"];
	//build message
	$lineFeed = "\n";
	$apostrophe = "'";
	$message = "Congratulations {$name}!{$lineFeed}";
	$message .= "Your video file: {$filename}, submitted on {$submitted}.{$lineFeed} has been accepted into our competition.";
	$message .= "Please feel free to enter another video and increase your chances of being selected in the prize draw.{$lineFeed}";
	$message .= "Thanks for your efforts and we hope to see more of your work";
	$message .= "{$lineFeed}";
	$message .= "Mike & Tim {$lineFeed}";
	$message .= "The Language Addicts Team {$lineFeed}";
	$message .= "www.languageaddicts.com {$lineFeed}";
	return $message;
}

function formatRejectionEmail($submissionRow,$rejectType,$mode)
{
//retrieve row data
$feedbackComments=stripslashes($submissionRow["DECISIONNOTES"]);
$email=$submissionRow["EMAIL"];
$ID=$submissionRow["SUBMISSIONID"];
$name=$submissionRow["NAME"];
$submitted=$submissionRow["SUBMITTED"];
$filename=$submissionRow["ORIGINAL_FILENAME"];
//build message
$lineFeed = "<br>";
$apostrophe = "&apos;";
if ($mode == "send")
{
$lineFeed = "\n";
$apostrophe = "'";
}
if ($rejectType == "P")
{
$encouragement = " We{$apostrophe}d encourage you to revise your video and resubmit as it is close to the standard we expect. For inspiration and ideas for visual cues please take a look at some of our previous competition entries at www.languageaddicts.com.";
}
else
{
$encouragement = " ";
}
$message = "Hi {$name},{$lineFeed}";
$message .= "Thank you for your video file: {$filename}, submitted on {$submitted}.{$lineFeed}";
$message .= "Unfortunately the video didn{$apostrophe}t meet our contest requirements.{$lineFeed}";
$message .= "{$lineFeed}";
if ($feedbackComments != " " AND $feedbackComments != null )
{
$message .= "feedback comments:{$feedbackComments}{$lineFeed}";
$message .= "{$lineFeed}";
}
$message .= "Thanks for your interest in our contest.{$encouragement} ";
$message .= "If you have any questions please email us.{$lineFeed}";
$message .= "{$lineFeed}";
$message .= "Mike & Tim {$lineFeed}";
$message .= "The Language Addicts Team {$lineFeed}";
$message .= "www.languageaddicts.com {$lineFeed}";
return $message;
}

function sendEmail($submissionRow,$status)
{
//build email
$email=$submissionRow["EMAIL"];
$subject = "Language Addicts video competition entry";
switch ($status) {
	case "A":
		$message = formatAcceptanceEmail($submissionRow);
		break;
	default:
		$message = formatRejectionEmail($submissionRow,$status,"send");
		break;
}
$headers = "From:" . "contest@languageaddicts.com";
mail($email,$subject,$message,$headers);
}
?>