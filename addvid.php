<?php session_start();
// DB functions and form display
require ("addvid_db_functions.php");
require ("db_functions.php");
// DB functions and form display

// if (checkPword() == true) {
//    traceStart();
    //set user
//    if (!isset($_POST["user"])) {
//        $_SESSION['user'] = " ";
//    }

    // Switch statement based on value passed from the button on the validate\add forms
    switch (@$_POST['button']) {
            // User hit validate button
                  
        case "Show all vids?":
        
            header("Location: ShowAllVids.php");
            break;

        case "Validate":

            // Make sure there is something to post
            if (empty($_POST['words'])) {
                die("You cannot add a video without any words. Please hit back and enter the video words!");
            }
            
            // Open database
            opendb();

            // Get the data entered in the form and prepare it for use
            preparepostdata($words, $scenario, $videoWords, $connection);

            // Prepare data for validation
            prepareValidateData($videoWords, $videoToEdit, $connection);


            // Add new words array and new word count to session
            $_SESSION['newWords'] = $newWords;
            $_SESSION['WordCount'] = $WordCount;
            $_SESSION['uniqueWordsCount'] = $uniqueWordsCount;
        
            // Now draw validate page
            include ("validatevid.inc");


            break; // Ends case block

        case "Add":

            // Get new words array and new word count from session
            $newWords = $_SESSION['newWords'];
            $newWordCount = $_SESSION['WordCount'];
            $submissionID = $_SESSION['submissionID'];         

            // Open database
            opendb();
            
            if (isset($_SESSION['submissionID'])) {
		            // Get video details
		     		// Get submission details
		            $submission = getSubmission($submissionID);
		            $submissionRow = mysqli_fetch_array($submission);
		            $submitted = $submissionRow["SUBMITTED"]; 
		            $email =  $submissionRow["EMAIL"]; 
		            $sceneid= $submissionRow["SCENEID"];
		            $contributorID = getContributorID($email);
             }
            
            

            // Protect against sql injection
            foreach (array_keys($_POST) as $key) {
                $_POST[$key] = mysqli_real_escape_string($connection, $_POST[$key]);
            }

            // Set scenario
            $scenario = $_POST[scenario];

            // User has validated the data entry values and has pressed the add button
            // Update new words array with data from form passed in via $_POST
            for ($i = 0; $i <= $newWordCount - 1; $i++) {

                $Sequence = $i + 1;

                $newWords[$i][newword] = $_POST[newword_ . $Sequence];
                $newWords[$i][wordtime] = $_POST[wordtime_ . $Sequence];
                $newWords[$i][notes] = $_POST[notes_ . $Sequence];

            }

            // Initialise unique word counter to add to video file later
            $uniqueWordCount = 0;

            for ($i = 0; $i <= $newWordCount - 1; $i++) {

                if ($newWords[$i][newword] == "checked") {
                    // Add new words to words file
                    $insertWord = $newWords[$i][word];
                    // Escape insert word
                    $insertWord = mysqli_real_escape_string($connection,$insertWord);
                    $insertNotes = $newWords[$i][notes];
                    $add_words = "insert into words(WORD, NOTES) values ('$insertWord','$insertNotes')";
                    $result = mysqli_query($connection, $add_words) or die($add_words);
                    // Add
                    $videoWords[$i] = $newWords[$i][word];

                    // Add word to addedWords array
                    $addedWords[$i][word] = $newWords[$i][word];

                    $uniqueWordCount++;

                } else {
                    // Don't add word to words file but add it to video words
                    $videoWords[$i] = $newWords[$i][word];

                    $insertWord = $newWords[$i][word];
                    if (!wordHasBeenAddedAlready($addedWords, $insertWord)) {
                        $uniqueWordCount++;
                    }
                    // Add word to addedWords array as the word exists in db already so dont want to add again
                    // if it comes up in the remaining words to add
                    $addedWords[$i][word] = $newWords[$i][word];
                }

                // Check whether word has been added already
                // 	if (!wordHasBeenAddedAlready($addedWords, $insertWord)){

            }

            // Generate video filename
            $videoName = getvideofilename($newWordCount, $videoWords);
            // Set attribution and attribution url
            $attribution = $_POST['attribution'];
            $attributionrl = $_POST['attributionurl'];

            // Add video to videos file
            addvideofile($videoName, $uniqueWordCount, $scenario, $attribution, $attributionrl, $submitted, $contributorID, $connection);
            // Get the video ID
            $videoId = getvideoid($videoName, $connection);

            // Now map words and write video words
            for ($i = 0; $i <= $newWordCount - 1; $i++) {
                $checkWord = $newWords[$i][word];
                // Escape check word
                $checkWord = mysqli_real_escape_string($connection, $checkWord);
                $checkNotes = $newWords[$i][notes];
                $wordSequence = $newWords[$i][sequence];
                $wordTime = $newWords[$i][wordtime];
                // Get wordid of new word. Ensure the notes are the same too in case this is a synonym
                // of an existing word
                $query = "select wordid from words where word = '$checkWord' and notes = '$checkNotes'";
                $result = mysqli_query($connection, $query) or die($query);
                $resultarray = mysqli_fetch_assoc($result);
                $wordId = $resultarray["wordid"];

                // Add video words
                $addVideoWords = "insert into videowords (VIDEOS_VIDEOID, SEQUENCE, WORDID, TIME)values ('$videoId','$wordSequence','$wordId', '$wordTime')";
                $result = mysqli_query($connection, $addVideoWords);

                if (!$result) {
                    die("ERROR!<br />Query is: $addVideoWords<br />Error is: " . mysqli_error($connection));
                }

            }
            
           // generate_confirm_page($newWordCount,$newWords);
             include ("confirm.inc");
            break;
      
        case "Delete last entry?":
            // Get videoid from session
            $videoID = $_SESSION['videoID'];
            // Open database
            opendb();


            deletevideo($videoID, $connection);


            // Generate the html for the default addvid page
            $displayBlock = generate_displayblock();

            // Now draw default page
            echo "<html>
<meta http-equiv=\"Content-Language\" content=\"bn\">
<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
<head>
<title>Add an Entry</title>
</head>
<body>
 $displayBlock
</body>
</html>";
            break;

        	default:
        	// This is the first time the addvid.php has been called so display default page
        	
        	if (isset($_GET['submissionID'])) {
        			// This has been called from the processSubmission script

        			// Get the submission details using the submission ID
        			$submissionID = $_GET['submissionID'];
        			// Open database
        			opendb();
        			// Store submissionID as a session variable for use later
        			$submissionID = $_GET['submissionID'];
        			//Add submission ID as session variable
        			$_SESSION['submissionID'] = $submissionID;
        			// Get scene details
        			$submission = getSubmission($submissionID);
        			$submissionRow = mysqli_fetch_array($submission);
        			$sceneid= $submissionRow["SCENEID"];
        			// Get scene words
        			$phrase = getPhrase($sceneid);
        			   }
        	
        	// Generate the html for the default addvid page
            $displayBlock = generate_displayblock();

            // Now draw default page
            echo "<html>
<meta http-equiv=\"Content-Language\" content=\"bn\">
<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
<head>
<title>Add an Entry</title>
</head>
<body>
 $displayBlock
</body>
</html>";
            break;
    }
// }

function generate_attribution_displayblock()
{
    $displayBlock = <<< DISPLAYBLOCK
	<html>
<meta http-equiv=\"Content-Language\" content=\"bn\">
<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
<head>
<title>Add attributions</title>
</head>
	<h1>Add attributions</h1>
<body>
	<form action="ShowAllVids.php" method="post">
  <table width="355" border="0">
    <tr>
      <td width="105"><div align="right">Attribute:</div></td>
      <td width="240"><label>
        <input name="attribute" type="text" id="attribute" size="40" />
      </label></td>
    </tr>
    <tr>
      <td><div align="right">Attribute URL:</div></td>
      <td><label>
        <input name="attributeurl" type="text" id="attribute url" size="40" />
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td> 
       <input type="submit" name="button" value="Add attribution?"/>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
DISPLAYBLOCK;
    return $displayBlock;

}
function generate_displayblock()
{
	global $phrase;
    // This is a heredoc. The endstring, i.e. DISPLAYBLOCK, could be any string.
    // The 1st endstring must appear at the end of the first line with nothing after it.
    // The last end string must appear at the beginning of its own line with nothing after
    // it, not even a space!
    // Variables and special characters are evaluated in the same manner as
    // double-quoted strings.
    $displayBlock = <<< DISPLAYBLOCK
	<h1>Add video details</h1>
	<form action="$_SERVER[PHP_SELF]" method="post">
	<h2> Scenario</h2>
	<p><textarea name="scenario" cols="128" rows="4" id="words"></textarea></P>
	<h2> Video words</h2>
	<p><textarea name="words" cols="128" rows="4" id="words">$phrase</textarea></P>
	<input type="submit" name="button" value="Validate">
	<table border=\"0\"> 
	<tr  height="100px">
		
    </tr>
	<tr>
		<td align="center"><input type="submit" name="button" value="Start over?"></td>
    	<td align="center"><input type="submit" name="button" value="Show all vids?"></td>
    </tr>
</table>
	</form>
DISPLAYBLOCK;
    return $displayBlock;

}

function generate_displayblock_delete()
{
    // This is a heredoc. The endstring, i.e. DISPLAYBLOCK, could be any string.
    // The 1st endstring must appear at the end of the first line with nothing after it.
    // The last end string must appear at the beginning of its own line with nothing after
    // it, not even a space!
    // Variables and special characters are evaluated in the same manner as
    // double-quoted strings.
    $displayBlock = <<< DISPLAYBLOCK
	<h1>Delete specific video and associated records</h1>
	<form action="$_SERVER[PHP_SELF]" method="post">
	<table width="467" border="1">
  		<tr>
    	<td width="168">Video to delete</td>
    	<td width="144"><input type="text" name="VideoToDelete" id="VideoToDelete" /></td>
    	<td width="133"><label><input type="submit" name="deleteButton" id="deleteButton" value="Delete" /></label></td>
  		</tr>
	</table>
	</form>	
DISPLAYBLOCK;
    return $displayBlock;

}

function generate_displayblock_toolbar()
{
    // This is a heredoc. The endstring, i.e. DISPLAYBLOCK, could be any string.
    // The 1st endstring must appear at the end of the first line with nothing after it.
    // The last end string must appear at the beginning of its own line with nothing after
    // it, not even a space!
    // Variables and special characters are evaluated in the same manner as
    // double-quoted strings.
    $displayBlock = <<< DISPLAYBLOCK
<table border=\"1\"> 
	<tr>
		<td><div align=\"center\"><input type=\"submit\" name=\"button\" value=\"Add another?\"></div></td>
    	<td><div align=\"center\"><input type=\"submit\" name=\"button\" value=\"Delete last entry?\"></div></td>
    	<td><div align=\"center\"><input type=\"submit\" name=\"button\" value=\"Show all vids?\"></div></td>
    </tr>
</table>	
DISPLAYBLOCK;
    return $displayBlock;

}

function generate_displayblock_admin()
{
    // This is a heredoc. The endstring, i.e. DISPLAYBLOCK, could be any string.
    // The 1st endstring must appear at the end of the first line with nothing after it.
    // The last end string must appear at the beginning of its own line with nothing after
    // it, not even a space!
    // Variables and special characters are evaluated in the same manner as
    // double-quoted strings.
    $displayBlock = <<< DISPLAYBLOCK
	
	<body>   
<table width="822" border="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td width="201" height="100"> </td>
    <td width="201"><a href="addvid.php"><img src="../images/Add.png" width="200" height="100" /></a></td>
    <td width="201"><a href="deletevid.php"><img src="../images/Delete.png" width="200" height="100" /></a></td>
    <td width="201"><a href="ShowAllVids.php"><img src="../images/ShowAll.png" width="200" height="100" /></a></td>
    <td width="201"><a href="ShowAllVids.php"><img src="../images/Amend.png" width="200" height="100" /></a></td>
  </tr>
 

</table>


</body>

DISPLAYBLOCK;
    return $displayBlock;

}



?>