<?php
//Draw the word list for the competition


require("db_functions.php");
require("page_elements.php");
require("common/trace_functions.php");

traceStart();											//start the trace file
error_reporting(E_ALL);
openPage();
drawHead("Language Addicts English - Word List", "js/displayphrase.js");
drawBody();
closePage();

function drawBody()
{
//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo <<<EOF

EOF;
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();

//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
drawPhraseList();


echo "</div><!--end of content-->";
}


function drawPhraseList()
{
//build block header
echo<<<EOF
<div class="blankwideblock"  id="wordlist">
<img class="icon" src="images/guidelines.png" alt="guidelines">

EOF;
//open db

global $connection; //set up
opendb(); //open the database (db_functions.php)
$limitViewSize = 200;
$phraselist = getWantedPhrases();  								//get list of wanted words

	while($phrase = mysqli_fetch_array($phraselist))
	{
		$wantedPhrases[] = $phrase;									//store the sql result in working array
	}
$total = count($wantedPhrases);
echo "<p>The wordlist is dynamic and will change as we receive enough entries for existing words. We may also add new words during the contest. Currently there are approximately $total words in the list.</p>";
//output 
echo"<br>";
echo"Total phrases: ". sizeof($wantedPhrases);
echo "<div id=\"accordion\">";
$phraseLoop =0;
foreach($wantedPhrases as $wantedPhrase)							
		{
		if (++$phraseLoop == $limitViewSize){break;}
		$currentPoints = $wantedPhrase["POINTS"];
		switch ($currentPoints)
		{
		case 1:
			$image = "ticket_1.png";
			break;
		case 2:
			$image = "ticket_2.png";
			break;
		case 3:
			$image = "ticket_3.png";
			break;
		}
		$notes=stripslashes($wantedPhrase["NOTES"]);
		$printPhrase=$wantedPhrase["PHRASE"];
		echo "<h3> <img src=\"images/{$image}\">{$printPhrase}</h3>
		<div>
			<p>
			{$notes}
			</p>
		</div>";
		}
//build block trailer
echo<<<EOF
</div>
EOF;
}







?>