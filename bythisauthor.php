<?php
session_start();														//start the session to get page number and list data
require("db_functions.php");											//import database read library


if (isset($_SESSION['authorid']))										//has an author ID been set?
{
$authorID = $_SESSION['authorid'];										//got the author ID from the session
main($authorID);																	//call the main logic
}
else {echo " ";}														//no author to display, print nothing and end


function main($authorID)
{
	$maxLines = 3; 															// how many lines do we show at any one time
	$entries = array();														// create the author entries array for later use
	$refresh = $_GET['direct'];												// was the 'more' button pressed or is this a non-refreshing request $refresh is TRUE if 'more' pressed									
	if (!isset($_SESSION['authorEntries']))									// has the authors list already been populated and stored in the session?
	{
		global $connection; 													//set up db connection using global variable
		opendb(); 																//open the database (db_functions.php)
		$videos = getAuthorLatestVideos($authorID, 50);							//get the author's most recent 50 videos
		mysqli_close($connection);												//close the database connection
		while($entry = mysqli_fetch_array($videos))
		{
			$entries[] = $entry;												//store the sql result in working array
		}
		$_SESSION['authorEntries'] = $entries;									//put the array of authors entries in the session for later use
		$_SESSION['authorPageNo'] = 1;											//set the current page number to 1
	}
	else
	{
		if ($refresh == "TRUE"){$_SESSION['authorPageNo']++;}				//increment the page number as this is a refresh
		$entries = $_SESSION['authorEntries'];								//get contributor data out of session and into working array
	}
	$pageNo = $_SESSION['authorPageNo'];									//set page number from session
	$startPos = ($pageNo - 1) * $maxLines;									//work out start element
	$totalvideos = count($entries);											//get total number of entrants
	if ($startPos > $totalvideos) 
	{
		$startPos = 0;														//list has looped around, restart at 0
		$_SESSION['authorPageNo'] = 1;										//set the current page number to 1
	}
	$displayEntries = array_slice($entries,$startPos,$maxLines);			//slice out the portion of the array we want
	printHTML($displayEntries,$startPos);											//call function to draw html fragment to return
}

function printHTML($entries,$startPos)										//function to draw html fragment to draw
{
	foreach($entries as $video)						
		{
		$videoID = $video["VIDEOID"];										//get video ID
		$link = "?vid=".$videoID;											//set video URL
		$poster = getPosterDirectory(false, $videoID);						//set poster name
		$name = $video["VIDEONAME"];										//set video name
		echo"<div class=\"gallery\">";
		echo"</div>";
		echo"<a href=\"{$link}\"><img class=\"vid_image\" src=\"{$poster}\"></a>";
		//echo"<p>{$name}</p>";         
		}	
}

?>