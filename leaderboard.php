<?php
session_start();												//start the session to get page number and list data
require("db_functions.php");									//import database read library

$maxLines = 8; 													// how many lines do we show at any one time
$contribs = array();											// create the contributor array for later use
$refresh = $_GET['direct'];										// was the 'more' button pressed or is this a non-refreshing request $refresh is TRUE if 'more' pressed
$direction=$_GET['direction'];
if (!isset($_SESSION['contribs']))
{
	global $connection; 										//set up db connection using global variable
	opendb(); 													//open the database (db_functions.php)
	$contributorlist = getContributors();  						//get list of contributors
	mysqli_close($connection);									//close the database connection
	while($contrib = mysqli_fetch_array($contributorlist))
	{
		$contribs[] = $contrib;									//store the sql result in working array
	}
	$_SESSION['contribs'] = $contribs;							//put the array of authors in the session for later use
	$_SESSION['contribPageNo'] = 1;								//set the current page number to 1
}
else
{
	switch ($direction)
	{
	case "MORE":
	if ($refresh == "TRUE"){$_SESSION['contribPageNo']++;}		//increment the page number as this is a refresh
	break;
	case "LESS":
	if ($refresh == "TRUE" and $_SESSION['contribPageNo'] > 1){$_SESSION['contribPageNo']--;}		//decrement the page number as this is a refresh, only do this if the current page number is higher than 1
	break;
	}
	$contribs = $_SESSION['contribs'];								//get contributor data out of session and into working array
}
$pageNo = $_SESSION['contribPageNo'];							//set page number from session
$startPos = ($pageNo - 1) * $maxLines;							//work out start element
$totalentries = count($contribs);								//get total number of entrants
if ($startPos > $totalentries) 
{
$startPos = 0;													//list has looped around, restart at 0
$_SESSION['contribPageNo'] = 1;									//set the current page number to 1
}
$authors = array_slice($contribs,$startPos,$maxLines);			//slice out the portion of the array we want
printHTML($authors,$startPos);									//call function to draw html fragment to return

function printHTML($authors,$startPos)
{
	$leaderOrder = $startPos + 1;								//set the leaderboard position (take account of zero index)
	echo "<ul>";												//open the html list
		foreach($authors as $auth)								//add contributor details to list
		{
		$positionAndName = "#".$leaderOrder++. " - ".$auth["name"]." ". $auth["surname"][0];					//work out position and name to display for each author
		$link = "?author=".$auth["contribid"];																	//calculate the value of the associated URL
		$submissions = $auth['entries'];																		//get number of submissions by author
		   echo "<li><a href=\" {$link} \">{$positionAndName}<em>({$submissions})</em></a> </li> ";				//print the line
		}	
	echo "</ul>";												//close the html list
}
?>