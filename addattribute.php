<?php

/**
 * @author Timothy Clements
 * @copyright 2012
 */


$displayBlock = generate_attribution_displayblock(); // Get the html code for the attribution form
echo $displayBlock; // Output the html to screen



function generate_attribution_displayblock()
{
	$displayBlock = <<< DISPLAYBLOCK
	<html>
<meta http-equiv=\"Content-Language\" content=\"bn\">
<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
<head>
<title>Add attributions</title>
</head>
	<h1>Add attributions (f_generate_addvid_form.php)</h1>
<body>
	<form action="ShowAllVids.php" method="post">
  <table width="355" border="0">
    <tr>
      <td width="105"><div align="right">Attribute:</div></td>
      <td width="240"><label>
        <input name="attribute" type="text" id="attribute" size="40" />
      </label></td>
    </tr>
    <tr>
      <td><div align="right">Attribute URL:</div></td>
      <td><label>
        <input name="attributeurl" type="text" id="attribute url" size="40" />
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
       <input type="submit" name="button" value="Add attribution?"/>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
DISPLAYBLOCK;
	return $displayBlock;

}
?>