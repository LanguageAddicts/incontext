<?php
session_start();
require("page_elements.php");

//competition submission page
openPage();
drawHead("Language Addicts English - Competition Entry Completion");
drawBody();
closePage();

function drawBody()
{
//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo <<<EOF
EOF;
echo "</body>";
}

function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();
//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
if (isset($_SESSION['name']))
{
$name=$_SESSION['name'];
$file=$_SESSION['file'];
$email=$_SESSION['email'];

echo<<<EOF
<div class="successblock" id="success">
<img class="icon" src="images/success.png" alt="success">
<div class="block_text">
EOF;
echo"<h1>Congratulations!</h1>";
$msg = "<p>Thank you <b>" . $name. "</b> for your competition entry <b>".$file."</b>.</p>
<p>You will shortly receive an email as acknowledgement. If you don&apos;t receive the mail please 
check your spam folder and add contest@languageaddicts.com to your safe senders list.</p>
<p>You will find out within 72 hours if your video has been accepted and entered into the competition.</p>
<p>The prize draw for the winning entry will happen within 1 week of competition close.</p>
<p>The winner will be paid using paypal within 2 weeks of the competition close. </p>
<p>The email address <b>" .$email. "</b> will be used as the payment address. 
Thanks for your interest in our contest and good luck!</p>";
echo"{$msg}";
echo "</div></div>";
}else
{
echo<<<EOF
<div class="successblock" id="success">
<img class="icon" src="images/error.png" alt="success">
<div class="block_text">
EOF;
echo"<h1>hmmm!</h1>";
echo"<p>something has gone wrong! You should only see this page if you sucessfully uploaded a movie to our competition. </p>";
echo "</div></div>";
}
echo "</div><!--end of content-->";
}


?>