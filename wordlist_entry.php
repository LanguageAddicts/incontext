<?php
//Draw the word list for the competition
require("db_functions.php");
require("page_elements.php");
require("common/trace_functions.php");

//open db
global $connection; //set up
opendb(); //open the database (db_functions.php)

traceStart();											//start the trace file
error_reporting(E_ALL);
openPage();
drawHead("Language Addicts English - Word List entry", "js/wordmaintenance.js" );
drawBody();
closePage();

function drawBody()
{



//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo <<<EOF

EOF;
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();

//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
drawDataEntry();
drawWordList();


echo "</div><!--end of content-->";
}
function drawDataEntry()
{
//put single edit box on screen where the words go
echo<<<EOF
<div class="shortwideblock"  id="wordlist_data_entry">
<div id="data_input">
<form id="word_entry" action="addphrase.php" method="post">
Words: <input type="text" name="word" size="100"><br>
Notes (phrases only): <textarea name="notes" cols="100" rows="10"></textarea><br>
<input type="radio" id="radioWords" name="stringtype" value="words">word(s)
<input type="radio" id="radioPhrase"name="stringtype" value="phrase" checked="checked">phrase<br>
<button id="sub">Add</button>
<br>
</form>

</div>

</div>
EOF;
}

function drawWordList()
{
//build block header
echo"<div class=\"blankwideblock\"  id=\"wordlist\">";
//ajax will populate this area
echo"</div>";
echo"<div class=\"shortwideblock\"><span id=\"result\"><span></div>";

}







?>