<?php
//Draw the word list for the competition
require("db_functions.php");
require("page_elements.php");
require("common/trace_functions.php");

//open db
//global $connection; //set up
//opendb(); //open the database (db_functions.php)

traceStart();											//start the trace file
error_reporting(E_ALL);
openPage();
drawHead("Greek translitterator","js/transliterator.js");
drawBody();
closePage();

function drawBody()
{



//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo <<<EOF

EOF;
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();

//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
echo "<div class=\"shortwideblock\"  id=\"greek_data_entry\">";
drawDataEntry();
drawWordList();
echo "</div><!--end of shortwideblock-->";


echo "</div><!--end of content-->";
}
function drawDataEntry()
{
echo<<<EOF
<h1>Greek Pronunciation Tool</h1>(Beta version 1.1)<p>
Modern Greek has spelling that relates precisely to pronunciation. This contrasts with English</p>
<p>
Got a word in the greek script that you don't know how to pronounce? 
Our greek pronunciation guide is what you need.
</p> 
<p>For example, if you type in <b>"&#955;&#8057;&#947;&#959;&#962;"</b> our guide will tell you that this is pronounced "lOghoss".
The capital letter in the word tells you which sound to stress.</p> 
EOF;
//put single edit box on screen where the word goes
echo<<<EOF

<div id="data_input">
<form id="word_entry" action="" method="post">
Words: <input type="text" name="word" id="word" size="50">
<button id="sub">translitterate!</button>
<br>
</form>
</div>
<br>
<div id="result">
</div>
EOF;
}

function drawWordList()
{
echo"<div class=\"shortwideblock\"><span id=\"result\"><span></div>";
//build block header
echo"<div class=\"blankwideblock\"  id=\"popularlist\">";
//ajax will populate this area
echo"</div>";


}
