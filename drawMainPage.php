<?php

require("common/data_formatting.php");
require("page_elements.php");

function drawMainPage($videoID, $posterID, $author)
{
openPage();
drawHeadMain($videoID, $posterID, $author);
trace("drawMainPage: videoID::",$videoID);
drawBody($videoID, $posterID, $author);
closePage();
}


function drawHeadMain($videoID, $posterID, $author)
{
echo <<<EOF
<head>
	<meta charset="utf-8">	
	<title>Language Addicts English</title>
	<meta name="description" content="teaching the world English since yesterday">	
	<!-- Define a viewport to mobile devices to use - telling the browser to assume that the page is as wide as the device (width=device-width) and setting the initial page zoom level to be 1 (initial-scale=1.0) -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<!-- Add normalize.css which enables browsers to render all elements more consistently and in line with modern standards as it only targets particular styles that need normalizing -->
	<link href="css/normalize.css" rel="stylesheet" media="all">
	<link href="css/normalize-legacy.css" rel="stylesheet" media="all">
	<link href="css/styles.css" rel="stylesheet" media="all">
	<!-- Include the HTML5 shiv print polyfill for Internet Explorer browsers 8 and below -->
	<!--[if lt IE 9]><script src="js/html5shiv-printshiv.js" media="all"></script><![endif]-->
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.2.min.js"></script>
	<script src="js/scripts.js"></script>
EOF;
drawSocialMain($videoID, $posterID, $author);
echo "</head>";
}

function drawSocialMain($videoID, $posterID, $author)
{
echo"\n<meta property=\"og:description\" content=\"Language Addicts Video Competition Entries\" />\n";
echo"<meta property=\"og:site_name\" content=\"languageaddicts.com\" />\n";
echo"<meta property=\"og:title\" content=\"Learn English through video\" />\n";
$url = "http://www.languageaddicts.com/index.php?vid=".$videoID;
echo"<meta property=\"og:url\" content=\"{$url}\"/>\n";
$poster = "http://www.languageaddicts.com".getPosterDirectory(true,$posterID);
echo"<meta property=\"og:image\" content=\"{$poster}\" />\n";
echo"<meta property=\"og:type\" content=\"article\" />\n";

}

function drawBody($videoID, $posterID, $author)
{
echo "<body class=\"body\">";
drawHeader();
drawMainContent($videoID, $posterID, $author);
drawFooter();
drawAnalytics();
//close body tag
echo "</body>";
}




function drawMainContent($videoID, $posterID, $author)
{
//new line
echo "\n";
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent($videoID, $posterID, $author);
drawBelowTheFold();
//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent($videoID, $posterID, $author)
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
drawByThisAuthor($videoID, $posterID, $author);
drawVideoContainer($videoID, $posterID, $author);
drawTopSide();
drawTopMiddleSide();
drawMiddleSide();
drawAdds();
//close content div
echo "</div><!--end of content-->";
}

function drawByThisAuthor($videoID, $posterID, $author)
{
// content block showing three at a time of the current author's work
// will be populated by AJAX call once page has rendered
//new line
echo "\n";
echo <<<EOF
	<article class="byThisAuthor" >
		<header>
		   <p><img src="images/contributor.png">{$author}</p>
		</header>	
		<div id="byThisAuthor">
		</div>
			<div id="moreFromAuthor" class="gallerymore">
			<a href="#"><img src="images/down.png"></a>
			</div>
	</article>	
	
EOF;
}

function drawVideoContainer($videoID, $posterID, $author)
{
//open videocontainer div
echo "	<div class=\"videocontainer\">";
drawVideoPlayer($videoID, $posterID, $author);
// close videocontainer div
echo "</div><!--close videocontainer-->";
}

function drawVideoPlayer($videoID, $posterID, $author)
{
$masterRecord = buildVideoMasterData($videoID);					//get video data object containing all the goodies
echo "<article class=\"videoplayer\">";									//open videoplayer div
drawVideo($masterRecord);
drawAttribution($masterRecord);
drawSocialSharing();
// close videoplayer div
echo "</article>";
}

function drawVideo($masterRecord)
{
$videoID = $masterRecord["VIDEOID"];
$posterID = $masterRecord["POSTERID"];
$movieLink = $masterRecord["MOVIELINK"];
$moviePoster = $masterRecord["POSTERLINK"];
trace("drawVideo videoID: $videoID");  //run trace

//check files exist...


$Link=$movieLink;
$poster=$moviePoster;

//new line
echo "\n";
echo "<video id=\"mainVideo\" controls poster=\"{$poster}\" preload=\"none\" onerror=\"videoFail(this)\">
						<source src=\"{$Link}.mp4\" type=\"video/mp4\">
						<source src=\"{$Link}.ogv\" type=\"video/ogg\">
						<source src=\"{$Link}.webm\" type=\"video/webm\">
						<p>Your browser can&apost play this HTML5 video</p>
	</video>";
}

function drawAttribution($masterRecord)
{
$author = $masterRecord["CONTRIBNAME"];
echo "<div class=\"author\">";
		if ($masterRecord["CONTRIBURL"] != null)
		{
		$formattedURL = nice_url($masterRecord["CONTRIBURL"]);
		echo "<p>video produced by <a href=\"".$formattedURL. "\">".$author."</a>.</p>";
		}
		else
		{
		echo "<p>video produced by ".$author.".</p>";
		}
echo "</div>";
$AttributionArray = $masterRecord["ATTRIBUTIONARRAY"];									//unpack the attribution array from the masterRecord
$atts = count($AttributionArray);
//$attribution["ATTRIBUTION"] = $attr["ATTRIBUTION"];
//$attribution["ATTRIBUTIONURL"] = $attr["ATTRIBUTIONURL"];
foreach ($AttributionArray as $att)
{

$attributionText = "with additional thanks to ";
$attributionLink = $att["ATTRIBUTION"];
if ($att["ATTRIBUTIONURL"] <> null)
	{
	$formattedURL = nice_url($att["ATTRIBUTIONURL"]);
	$attributionLink= "<a href=\"{$formattedURL}\">{$att["ATTRIBUTION"]}</a>" ; 
	}
echo $attributionText.$attributionLink."<br>";
}

}


function drawTopSide()
{
echo <<<EOF
  <aside class="top-side">
	  <article>
	  <a href="comp.php"><img src="images/contest_closed.png"></a>
	  </article>
  </aside>
EOF;
}

function drawTopMiddleSide()
{
//show how many accepted entries we have and how many pending
$accepted = getAcceptedVideoCount();
//$pending = getPendingVideoCount();
echo <<<EOF
  <aside class="top-middle-side">
		<p>Accepted entries: {$accepted} </p>
  </aside>
EOF;
//<br> Pending entries: {$pending}
}
function drawMiddleSide()
{
echo <<<EOF
  <aside class="middle-side">
		<header>Contributor Leaderboard</header>
		<div class="galleryless">
		<a id="lessLeaderBoard" href="#" ><img src="images/up.png"></a>
		</div>
		<div id="leaderboard">
		</div>
		<div class="gallerymore">
		<a id="moreLeaderBoard" href="#" ><img src="images/down.png"></a>
		</div>
  </aside>
EOF;
}

function drawAdds()
{
echo <<<EOF
<aside class="ads">
	<script type="text/javascript"><!--
	google_ad_client = "ca-pub-4488266214387278";
	/* Unit4 */
	google_ad_slot = "8382654663";
	google_ad_width = 125;
	google_ad_height = 125;
	//-->
	</script>
	<script type="text/javascript"
	src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
	</script>
</aside>
EOF;
}
function drawBelowTheFold()
{
//work out how many pages we have in total and explicitly add the links to each
echo <<<EOF
		<div class="belowthefold">
			<article class="mostRecent" id="mostRecent">
					<header>
					<img class="mostRecentImg"src="images/latest.png">Most recent
					</header>
					<div id="mostRecentContent">
					</div>
					<div class="gallerymoreMain">
					  <input type="image" src="images/left.png" alt="Submit" width="32" height="16" id="gallerylessMain"> 
EOF;
$maxLines = 10; 														//specify number of items in page view
global $connection; 												//set up connection
opendb(); 																//open the database (db_functions.php)
$videolist = getMostRecentVideos(); 						//get list of recent videos
mysqli_close($connection);									//close the database connection
$totalEntries = 0;
	while($vid = mysqli_fetch_array($videolist ))
	{
		++$totalEntries;
	}
$pageNo = 0;
$counter =0;
do
{
$buttonID = "galleryPage-".++$pageNo;					// contstruct button id
echo "<button id=\"{$buttonID}\" class=\"pageButton\">{$pageNo}</button>"; 
$counter += $maxLines;
}
while ($counter < $totalEntries);				  
echo <<<EOF
					  <input type="image" src="images/right.png" alt="Submit" width="32" height="16" id="gallerymoreMain">
					</div>
					
			</article>
		</div> <!-- end of below the fold-->	
EOF;

}




?>