<h1>Greek <span class="highlight">Albums</span></h1> 

<p>Language Addicts provides collections of related topics as album downloads. Normally an album will consist of 7 topics, each containing
roughly 20 words, giving you an average of 140 words per album.</p>

<p>All language addicts albums can be purchased directly from this site or through iTunes by clicking the iTunes button.</p>
<p>Please note that the price you pay in iTunes will be higher.</p>
<div id="albums_container">

 <!-- bundle -->
  <div class="album">
    <div class="album_image">
      <img src="legacy_images/content/greek_album_multi.jpg" width="120" height="120" alt="Greek Album 4 Image" title="Greek volumes 1 to 3 plus podcasts 1 to 7" />
    </div>
    <div class="album_details">
      <h2>Greek Bundle: Volumes 1 to 3</h2><br />
      <b> + podcast episodes 1 to 7 Free</b>
      <br />
      <br />
      Volume 1 +<br />
      Volume 2 +<br />
      Volume 3 +<br />   
      Podcasts from September 2007 to March 2008<br />
      <br/>
      Normal retail price <b>$14.97</b><br />
      Bundle price <b>$9.99</b>
        
    </div>
    
    <div class="album_price">
    <hr />
       <div class="album_displayprice">
        <span class="list_head price">$9.99</span>
       </div>
        <div class="album_wordlist">
        </div>
       <div class="album_purchase">
       <a href="http://www.payloadz.com/go?id=1193905" target="paypal" onclick="javascript:urchinTracker ('/Albums page-Greek bundle -Payloadz Add to cart')">
  <img src="http://www.paypal.com/images/x-click-but22.gif" border="0" alt=" Add to Cart" ></a>
       </div>
    </div>
  </div>
  </div>
       
  <br />
  <hr /> 
  
  <div id="albums_container">       
  <!-- album 1 -->
  <div class="album">
    <div class="album_image">
      <img src="legacy_images/content/greek_album_1.jpg" width="120" height="120" alt="Greek Album 1 Image" title="Greek Volume 1: Family and friends" />
    </div>
    <div class="album_details">
      <h2>Volume 1: Family and friends</h2>
      
 
      
      <ol> 
        <li>Greetings and Pleasantries</li> 
        <li>Being polite</li> 
        <li>Family members</li> 
        <li>Relationships</li> 
        <li>Appearances</li> 
        <li>Characteristics</li> 
        <li>People and personal details</li>
      </ol> 
    </div>
    <div class="album_price">
    <hr />
       <div class="album_displayprice">
        <span class="list_head price">$4.99</span>
       </div>
       <div class="album_wordlist">
         <a href="pdf/greek_familyandfriends.pdf" rel="external" onclick="javascript:urchinTracker ('/Greek Albums - Wordlist Download - 1')">
           <img src="legacy_images/buttons/pdf.gif" width="18" height="24" alt="PDF - Album 1" title="Download word list for album 1" style="display: inline;
           vertical-align: middle;" /></a>
         &nbsp;
         <a href="pdf/greek_familyandfriends.pdf" rel="external" onclick="javascript:urchinTracker ('/Greek Albums - Wordlist Download - 1')">
           Download word list
         </a>
       </div>
       <div class="album_purchase">
         <a href="http://payloadz.com/go?id=345329" target="paypal" onclick="javascript:urchinTracker ('/Greek Album 1 - Payloadz Add to cart')">
          <img src="legacy_images/buttons/add_to_cart.gif" width="87" height="23" alt="Add to cart button" title="Add album to cart" />
         </a>
         or
         <a href="http://clkuk.tradedoubler.com/click?p=23708&a=1410228&url=http%3A%2F%2Fitunes.apple.com%2Fgb%2Falbum%2F1-1-1-greetings-pleasantries-familiarise%2Fid259589406%3Fi%3D259589425%26uo%3D6%26partnerId%3D2003" target="itunes_store" onclick="javascript:urchinTracker ('/Greek Album 1 - Referal to iTunes')"><img height="15" width="61" alt="Language Addicts Greek - Greek Vocab Builder, Vol. 1: Family &amp; Friends" src="http://ax.phobos.apple.com.edgesuite.net/images/badgeitunes61x15dark.gif" /></a>
       </div>
    </div>
  </div>
   </div>
       
  <br />
  <hr /> 
  <div id="albums_container">   
  
  <!-- album 2 -->
  <div class="album">
    <div class="album_image">
      <img src="legacy_images/content/greek_album_2.jpg" width="120" height="120" alt="Greek Album 2 Image" title="Greek Volume 2: Home and neighbourhood" />
    </div>
    <div class="album_details">
      <h2>Volume 2: Home and neighbourhood</h2>

      <ol>
        <li>Around the house</li> 
        <li>Furniture and fittings</li> 
        <li>Household items</li> 
        <li>Household chores</li> 
        <li>Daily routine</li> 
        <li>In the kitchen</li> 
        <li>Neighbourhood</li>
      </ol> 
    </div>
    <div class="album_price">
    <hr />
       <div class="album_displayprice">
        <span class="list_head price">$4.99</span>
       </div>
       <div class="album_wordlist">
         <a href="pdf/greek_homeandneighbourhood.pdf" rel="external" onclick="javascript:urchinTracker ('/Greek Albums - Wordlist Download - 2')">
           <img src="legacy_images/buttons/pdf.gif" width="18" height="24" alt="PDF - Album 2" title="Download word list for album 2" style="display: inline;
           vertical-align: middle;" /></a>
         &nbsp;
         <a href="pdf/greek_homeandneighbourhood.pdf" rel="external" onclick="javascript:urchinTracker ('/Greek Albums - Wordlist Download - 2')">
           Download word list
         </a>
       </div>
       <div class="album_purchase">
         <a href="http://payloadz.com/go?id=371116" target="paypal" onclick="javascript:urchinTracker ('/Greek Album 2 - Payloadz Add to cart')">
          <img src="legacy_images/buttons/add_to_cart.gif" width="87" height="23" alt="Add to cart button" title="Add album to cart" />
         </a>
         or
         <a href="http://clkuk.tradedoubler.com/click?p=23708&a=1410228&url=http%3A%2F%2Fitunes.apple.com%2Fgb%2Falbum%2F2-6-3-in-the-kitchen-memorise%2Fid270746470%3Fi%3D270746496%26uo%3D6%26partnerId%3D2003" target="itunes_store" onclick="javascript:urchinTracker ('/Greek Album 2 - Referal to iTunes')"><img height="15" width="61" alt="Language Addicts Greek - Greek Vocab Builder, Vol. 2: Home &amp; Neighbourhood" src="http://ax.phobos.apple.com.edgesuite.net/images/badgeitunes61x15dark.gif" /></a>

       </div>
    </div>
  </div>
 </div>
       
  <br />
  <hr /> 
  <div id="albums_container">    
  <!-- album 3 -->
  <div class="album">
    <div class="album_image">
      <img src="legacy_images/content/greek_album_3.jpg" width="120" height="120" alt="Greek Album 3 Image" title="Greek Volume 3: Healthy living" />
    </div>
    <div class="album_details">
      <h2>Volume 3: Healthy living</h2>
    <ol>
        <li>Meeting up and going out</li> 
        <li>Hobbies and Interests</li> 
        <li>Media, Literature and arts</li> 
        <li>Health and fitness</li> 
        <li>Sporting activities</li> 
        <li>Pets and farm animals</li> 
        <li>Animals and insects</li> 
        <li>The countryside</li>
      </ol> 
    </div>
    <div class="album_price">
    <hr />
       <div class="album_displayprice">
        <span class="list_head price">$4.99</span>
       </div>
       <div class="album_wordlist">
         <a href="pdf/greek_healthyliving.pdf" rel="external" onclick="javascript:urchinTracker ('/Greek Albums - Wordlist Download - 3')">
           <img src="legacy_images/buttons/pdf.gif" width="18" height="24" alt="PDF - Album 3" title="Download word list for album 3" style="display: inline;
           vertical-align: middle;" /></a>
         &nbsp;
         <a href="pdf/greek_healthyliving.pdf" rel="external" onclick="javascript:urchinTracker ('/Greek Albums - Wordlist Download - 3')">
           Download word list
         </a>
       </div>
       <div class="album_purchase">
         <a href="http://payloadz.com/go?id=371118" target="paypal" onclick="javascript:urchinTracker ('/Greek Album 3 -Payloadz Add to cart')">
          <img src="legacy_images/buttons/add_to_cart.gif" width="87" height="23" alt="Add to cart button" title="Add album to cart" />
         </a>
         or
         <a href="http://clkuk.tradedoubler.com/click?p=23708&a=1410228&url=http%3A%2F%2Fitunes.apple.com%2Fgb%2Falbum%2F3-5-1-sporting-activities-familiarise%2Fid270745064%3Fi%3D270745328%26uo%3D6%26partnerId%3D2003" target="itunes_store" onclick="javascript:urchinTracker ('/Greek Album 3 - Referal to iTunes')"><img height="15" width="61" alt="Language Addicts Greek - Greek Vocab Builder, Vol. 3: Healthy Living" src="http://ax.phobos.apple.com.edgesuite.net/images/badgeitunes61x15dark.gif" /></a>
        
       </div>
    </div>
  </div>
  

  
 
  
  
</div>