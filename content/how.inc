<h1>Familiarise, Recognise, <span class="highlight">Memorise &reg;</span></h1>

<p>The three stages of any language Addicts' vocabulary builder.</p>

<h2><span class="highlight">How does it work?</span></h2>

<p>Simple. Using the idea of Audio-flash cards you will hear the foreign word and its English translation in three test
scenarios: Familiarise, Recognise, Memorise &reg;</p>
<br/>
<h2>Familiarise</h2>

<p>Each 'deck' consists of about 20 words. Familiarise is the first stage of each test and eases you into the test by repeating the foreign word
several times. To accustom your ear to the unfamiliar sounds of the language, this stage also includes a slowed-down version of the foreign word.</p>
<p><i>Play example</i></p>
<script language="JavaScript" src="http://www.languageaddicts.com/audio/audio-player.js"></script>
<object type="application/x-shockwave-flash" data="http://www.languageaddicts.com/audio/player.swf" id="audioplayer1" height="24" width="290">
<param name="movie" value="http://www.languageaddicts.com/audio/player.swf">
<param name="FlashVars" value="playerID=1&amp;soundFile=http://www.languageaddicts.com/audio/German_1_Familiarise.mp3">
<param name="quality" value="high">
<param name="menu" value="false">
<param name="wmode" value="transparent">
</object>
<br/>
<br/>
<h2>Recognise</h2>

<p>Having familiarised yourself with the words in the first stage, now it's time to hear the foreign word and grope round your memory for its meaning before hearing the translation</p>
<p><i>Play example</i></p>
<object type="application/x-shockwave-flash" data="http://www.languageaddicts.com/audio/player.swf" id="audioplayer2" height="24" width="290">
<param name="movie" value="http://www.languageaddicts.com/audio/player.swf">
<param name="FlashVars" value="playerID=2&amp;soundFile=http://www.languageaddicts.com/audio/German_1_Recognise.mp3">
<param name="quality" value="high">
<param name="menu" value="false">
<param name="wmode" value="transparent">
</object>
<br/>
<br/>
<h2>Memorise</h2>

<p>This is the big one! You can already recognise the foreign words but can you actually remember them? In this third and final stage you will hear the English word
and have a few seconds in which to think of the foreign word. Once you succeed in this stage you have learned the word and yes it does feel nice!</p>
<p><i>Play example</i></p>
<object type="application/x-shockwave-flash" data="http://www.languageaddicts.com/audio/player.swf" id="audioplayer3" height="24" width="290">
<param name="movie" value="http://www.languageaddicts.com/audio/player.swf">
<param name="FlashVars" value="playerID=3&amp;soundFile=http://www.languageaddicts.com/audio/German_1_Memorise.mp3">
<param name="quality" value="high">
<param name="menu" value="false">
<param name="wmode" value="transparent">
</object>
