<br/>	
		<h1>Thank you for your payment</h1> 
		<p>Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account at <a href="http://www.paypal.com">www.paypal.com</a> to view details of this transaction.</p>
	    <br/>
