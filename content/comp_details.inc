<h2>Who are we and what&apos;s our objective?</h2>
<p>We&apos;re a language learning business with a bright new idea for teaching English.
We want to establish a network of video contributors whose work we will feature on our language learning website.
We want this competition to lead to longer-term working relationships with the best content authors. </p>
<h2>Brief</h2>
<p>We want short, engaging, funny, original audio-visual material (in English) where the action is so obvious that the user doesn&apos;t have to understand the spoken words. </p>
<h3>So, like this... only better</h3>
<iframe width="280" height="157" src="http://www.youtube.com/embed/GQpG0Ff4K2c" frameborder="0" allowfullscreen></iframe>
<iframe width="280" height="157" src="http://www.youtube.com/embed/CzixZO16S8U" frameborder="0" allowfullscreen></iframe>
<!--div id="comp_rules" style="width:350px"-->
<p>
All words spoken in the videos are absolutely pertinent to what is happening visually.
Wherever possible, the video should highlight the objects or actions as they are spoken, thus making the link to the spoken word.
The genre of the video can be literally anything you like as long as it gets the message across. </p>  


<h2>Video rules</h2>
<p>Contributors will produce a short video expressing a simple idea. Videos must:</p> 
<ul>
<li>consist of no more than 10 words</li> 
<li>last no longer than 15 seconds</li> 
<li>be delivered in clear, intelligible english (local accents are perfectly acceptable and welcome)</li>
<li>contain simple everyday vocabulary</li>
<li>not contain any logo or watermark (don&apos;t worry your work will be attributed to you if we use it)</li>
<li>not contain any copyrighted material. This content must be yours to give in its entirety</li>
</ul>

<h2>Stuck for ideas?</h2>
<p>We want basic vocab in an fresh style. So the sentence &apos;The melancholy unicorn reads about taxidermy&apos; 
might be a challenge but the vocab is too difficult for beginners. On the other hand, &apos;the sausage is eating the dog&apos; is great as the language is basic but it would look interesting</p>
<p>Here are some basic ideas in case you're stuck</p>
<ul>
<li><b>The small green man is watching TV</b> <i>(easy to get across the concept of small,green, man and TV)</i></li>
<li><b>The dog is driving the car </b><i>(dog in the driver's seat, what else could he be doing but driving?)</i></li>
<li><b>My name is Barack Obama, what's your name? </b><i>(well known person with a distinctive name, so clearly this is someone introducing themself)</i></li>
<li><b>what&apos;s your name?.. My name is Barack Obama</b><i>(one person asking another their name. Using someone whose name you already know makes this obvious)</i></li>
<li><b>I'm Thirsty</b> <i>(say this whilst gesturing to throat then drink a glass of water and show obvious relief)</i></li>
<li><b>the boys are playing football</b><i>(pretty obvious)</i></li>
<li><b>I love you, mummy</b><i>(child gestures to self for &apos;I&apos; and to mummy for &apos;you&apos;. Followed by cuddle)</i></li>
<li><b>dad, where's the football? here</b><i>(child asks father where ball is, father finds it)</i></li>

</ul>
<!--/div-->

