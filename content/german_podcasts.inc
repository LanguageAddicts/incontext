﻿<h1>German <span class="highlight">Podcasts</span></h1> <h2>Get archive episodes, transcriptions and <a href="#subscribelink">subscribe.</a></h2><p>Please note that only the last 3 podcasts are available for free download</p><h2>Latest episodes</h2>
<table style="border: 1px solid black;width: 100%;"><tr class="PodcastTableHeader"><td width=110><b>Episode</b></td><td width=150><b>Date</b></td> </td><td width=100 colspan="2"><b>Get it!</b></td></tr>
  <tr>
  <td><a href="#11">11. In the garden</a></td><td>July 2010</td><td colspan="2"><b>Free</b></td></tr>
  <tr>
  <td><a href="#10">10. Personal details</a></td><td>June 2010</td><td colspan="2"><b>Free</b></td></tr>
  <tr>
    <td><a href="#9">09. Home routine</a></td><td>May 2010</td><td colspan="2"><b>Free</b></td></tr>
  <tr>
    <td><a href="#8">08. Social ties</a></td>  <td>April 2010</td><td colspan="2"><b>Archived</b></td></tr>
  <tr>
    <td><a href="#7">07. Household chores.</a></td><td>March 2010</td><td colspan="2"><b>Archived</b></td></tr>
  <tr>
    <td><a href="#6">06. Relationships</a></td><td>Febuary 2010</td colspan="2"><td colspan="2"><b>Archived</b></td></tr>
</table> 
<br/><h2>Archive material</h2><p>buy individual volumes or get all six in the discounted bundle</p>
 
 <table style="border: 1px solid black;width: 100%;"><tr class="PodcastTableHeader"><td width=110><b>Episode</b></td><td width=150><b>Date</b></td> </td><td width=100 colspan="2"><b>Get it!</b></td></tr><tr><td><a href="german_podcasts_volume_1.htm">Vol.1 (podcasts 1-5)</a></td>
    <td>Sep 09 - Jan 10</td><td>$1.99</td><td><a href="http://www.payloadz.com/go?id=1288642" target="paypal"><img src="http://www.paypal.com/images/x-click-but22.gif" border="0" alt=" Add to Cart" ></a></td></tr><!-- bundle -->
 </table>
<br/>
  <h2>Episode Listing</h2>
  <br/>
  
  <table class="table_width" width="550" cellpadding="0" cellspacing="2" style="border: 1;">
    <tr class="PodcastTableRow">
      <td><table class="table_width" width="550" cellpadding="0" cellspacing="2" style="border: 1;">
        <!-- start of 11 -->
        <tr id="11" class="PodcastTableRow">
          <td colspan="3"><h2>11. Juy 2010 - In the garden</h2></td>
        </tr>
        <tr class="PodcastTableHeader">
          <td class="PodcastTableCell">English</td>
          <td class="PodcastTableCell">German</td>
          <td class="PodcastTableCell">Phonetic</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to mow the lawn</td>
          <td>den Rasen m&auml;hen</td>
          <td>dayn RAH-zen MAY-hen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the fence</td>
          <td>der Zaun</td>
          <td>derr TZOWN</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the yard</td>
          <td>der Hof</td>
          <td>derr HOHFF</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the hosepipe</td>
          <td>der Gartenschlauch</td>
          <td>derr GAR-ten-shlauch (ch as in loch)</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the climbing frame</td>
          <td>Kletterger&uuml;st</td>
          <td>dass KLETT-er-geh-roost</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the grass</td>
          <td>das Grass</td>
          <td>dass GRAAS</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the paddling pool</td>
          <td>das Planschbechen</td>
          <td>dass PLANSCH-beh-chen (ch as in lock)</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the trampoline</td>
          <td>das Trampolin</td>
          <td>dass TRAM-po-line</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the barbecue</td>
          <td>der Grill</td>
          <td> derr GRILL</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the clothesline</td>
          <td>W&auml;scheleine</td>
          <td>SHLAH-fen</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!-- end of 11 -->
        <!-- start of 10 -->
        <tr id="10" class="PodcastTableRow">
          <td colspan="3"><h2>10. June 2010 - Personal details</h2></td>
        </tr>
        <tr class="PodcastTableHeader">
          <td class="PodcastTableCell">English</td>
          <td class="PodcastTableCell">German </td>
          <td class="PodcastTableCell">Phonetic</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the young man </td>
          <td>der junge Mann</td>
          <td>der YUNG-eh MANN</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the young woman</td>
          <td>die junge Frau</td>
          <td>dee YUNG-eh FROW</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the old woman</td>
          <td>die alte Frau</td>
          <td>dee AL-teh FROW</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the child</td>
          <td>das Kind</td>
          <td>dass KINT</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the people</td>
          <td>die Leute</td>
          <td>dee LOY-teh</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the name</td>
          <td>der Name</td>
          <td>der NAH-meh</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to come from</td>
          <td>herkommen</td>
          <td>HERR-koh-men</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the ancestors</td>
          <td>die Vorfahren</td>
          <td>dee FOR-far-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the sex</td>
          <td>das Geschlecht</td>
          <td>dass GEH-shlecht (ch as in loch)</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the widow</td>
          <td>die Witwe</td>
          <td>dee VIT-veh</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!-- end of 10 -->
        <!-- start of 9 -->
        <tr id="9" class="PodcastTableRow">
          <td colspan="3"><h2>9. May 2010 - Home Routine</h2></td>
        </tr>
        <tr class="PodcastTableHeader">
          <td class="PodcastTableCell">English</td>
          <td class="PodcastTableCell">German </td>
          <td class="PodcastTableCell">Phonetic</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to get up</td>
          <td>aufstehen</td>
          <td>OWF-shteh-hen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to take a bath</td>
          <td>baden</td>
          <td>BAA-den</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the razor</td>
          <td>der Rasierapparat</td>
          <td>DEHR ra-ZEER-ap-ahr-att</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the breakfast</td>
          <td>das Fr&uuml;hst&uuml;ck</td>
          <td>DASS FREW-shtuk</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to eat</td>
          <td>essen</td>
          <td>ES-sen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to make breakfast</td>
          <td>Fr&uuml;hst&uuml;ck machen</td>
          <td>FREW-shtuk MAH-ken</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the lunch</td>
          <td>das Mittagessen</td>
          <td>DASS MIT-tag-es-sen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the dinner</td>
          <td>das Abendessen</td>
          <td>DASS AH-bend-es-sen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the routine</td>
          <td>die Routine</td>
          <td>DEE roo-TEEN-eh</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to sleep</td>
          <td>schlafen</td>
          <td>sh-LAH-fen</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!-- end of 9 -->
        <!-- start of 8 -->
        <tr id="8" class="PodcastTableRow">
          <td colspan="3"><h2>8. April 2010 - Social Ties</h2></td>
        </tr>
        <tr class="PodcastTableHeader">
          <td class="PodcastTableCell">English</td>
          <td class="PodcastTableCell">German </td>
          <td class="PodcastTableCell">Phonetic</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the friend/boyfriend</td>
          <td> der Freund</td>
          <td> dehr FROYnd</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the affair</td>
          <td> die Aff&auml;re</td>
          <td> DEE aff-FEH-reh</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the friend/girlfriend</td>
          <td> die Freundin</td>
          <td> dee FROYnd-in</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the engagement </td>
          <td>die Verlobung</td>
          <td>dee fehr-LO-bung</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the fianc&eacute;e</td>
          <td>die Verlobte</td>
          <td>dee fer-LOB-te</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to get divorced</td>
          <td>sich trennen</td>
          <td>sish TREN-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the mistress</td>
          <td>die M&amp;aumltresse </td>
          <td>dee meh-TRESS</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the partner</td>
          <td>der Lebenspartner</td>
          <td>dehr LEHB-ens-part-ner</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to get on well together</td>
          <td> sich verstehen</td>
          <td> sish fer-STE-hen</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!-- end of 8 -->
        <!-- start of 7 -->
        <tr id="7" class="PodcastTableRow">
          <td colspan="3"><h2>7. March 2010 - Household chores</h2></td>
        </tr>
        <tr class="PodcastTableHeader">
          <td class="PodcastTableCell">English</td>
          <td class="PodcastTableCell">German </td>
          <td class="PodcastTableCell">Phonetic</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to dust</td>
          <td>abstaugen</td>
          <td>AP-shtaw-gen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the housework</td>
          <td>die Hausarbeit</td>
          <td>DEE HAWs-ah-bite</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to wash up</td>
          <td>abwaschen</td>
          <td>AP-vash-</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the clothes line</td>
          <td>die W&auml;scheleine</td>
          <td>dee VESHE-lie-neh</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to polish</td>
          <td>polieren</td>
          <td>POH-leer-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to decorate</td>
          <td>dekorieren</td>
          <td>deh-koh-REER-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to mop up</td>
          <td>aufwischen</td>
          <td>AWF-vish-un</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to throw away</td>
          <td>wegwerfen</td>
          <td>VEGG-verf-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to recycle</td>
          <td>wiederverwenden</td>
          <td> VEE-der-fer-vend-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to wipe</td>
          <td>abwischen</td>
          <td>AP-vish-en</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!-- end of 7 -->
        <!-- start of 6 -->
        <tr id="6" class="PodcastTableRow">
          <td colspan="3"><h2>6. Febuary 2010 - Relationships</h2></td>
        </tr>
        <tr class="PodcastTableHeader">
          <td class="PodcastTableCell">English</td>
          <td class="PodcastTableCell">German </td>
          <td class="PodcastTableCell">Phonetic</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the argument</td>
          <td>die Auseinandersetzung</td>
          <td>DEE OWSS-eyn-ander-set-tzung</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to compromise</td>
          <td>kompromittieren</td>
          <td>kom-prom-it-EER-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>the discussion</td>
          <td>die Diskussion</td>
          <td>DEE dis-kuss-see-OHN</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to gossip</td>
          <td>plaudern</td>
          <td>PLOW-dern</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to hate</td>
          <td>hassen</td>
          <td>HAS-sen</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to kiss</td>
          <td>k&uuml;ssen</td>
          <td>koo-SEN</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to shout</td>
          <td>schreien</td>
          <td>SHRY-en</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to whisper</td>
          <td>fl&uuml;stern</td>
          <td>FLUH-stern</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to cuddle</td>
          <td>kuscheln</td>
          <td>KUH-sheln</td>
        </tr>
        <tr class="PodcastTableRow">
          <td>to feel</td>
          <td>sich f&uuml;hlen</td>
          <td> SICH FOOL-en</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <!-- end of 6 -->
        <!-- start of 5 -->
        <!-- end of 5 -->
        <!-- start of 4 -->
        <!-- end of 4 -->
        <!-- start of 3 -->
        <!-- end of 3 -->
        <!-- start of 2 -->
        <!-- end of 2 -->
        <!-- episode 1-->
        <!-- end of 1 -->
      </table></td>
    </tr>
  </table>
  <table style="border: 1px solid black;backcolor:gray;">
    <tr>
      <td colspan="5"><h3 id="subscribelink" align="center">Subscribe to this podcast</h3></td>
    </tr>
    <tr></tr>
    <tr>
      <td colspan="5"><input class="inputRssUrl" type="text" size="70" align="center" value="http://www.languageaddicts.com/podcasts/german/german.rss"/></td>
    </tr>
    <tr>
      <td colspan="5"></td>
    </tr>
    <tr>
      <td><a class="addClearance" href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?id=329204245"
onclick="javascript:urchinTracker ('/podcast/German/itunes_subscription')"> <img src="http://www.languageaddicts.com/legacy_images/itunes.gif" alt="Subscribe with iTunes"/></a></td>
      <td><a class="addClearance" href="http://add.my.yahoo.com/rss?url=http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/yahoo_subscription')"><img src="http://www.languageaddicts.com/legacy_images/my_yahoo.gif" alt="Subscribe with My Yahoo!"/></a></td>
      <td><a class="addClearance" href="http://fusion.google.com/add?feedurl=http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/google_subscription')"><img src="http://www.languageaddicts.com/legacy_images/Google_Reader.gif" alt="Subscribe with Google Reader"/></a></td>
      <td><a class="addClearance" href="http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/rss_subscription')"><img src="http://www.languageaddicts.com/legacy_images/rss.gif" alt="Subscribe through RSS"/></a></td>
      <td><a href="#" onclick="window.open('http://www.podtrac.com/PodtracPlayer/podtracplayer.aspx?podcast=http://www.languageaddicts.com/podcasts/german/german.rss', 'linkname', 'height=235, width=450, scrollbars=no'); return false;">Listen now</a></td>
    </tr>
  </table>
  <br/><hr/><br/>