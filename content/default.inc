<h1>Vocabulary learning resources - <span class="highlight">MP3 language audio</span></h1>

<p>Language Addicts is a vocabulary learning resource, providing foreign language audio lessons for your IPOD/MP3 player.</p>

<p>There are many reasons to start learning another language - family ties, work or settling in a new country. Even so, finding
the time can be a real challenge.</p>
  
<p>This is why our language audio method fits in around you. You can learn neatly grouped sets of vocab through your headphones
at your convenience - In the car, on the train, at the gym, jogging or walking to work.</p>
<!--
<br/>

<object type="application/x-shockwave-flash" data="http://widgets.clearspring.com/o/4a37fac83c457251/4a679b6953e52b21/4a381495a8801718/3874e489/-cpid/97eb2a224886d83a" id="W4a37fac83c4572514a679b6953e52b21" width="400" height="160"><param name="movie" value="http://widgets.clearspring.com/o/4a37fac83c457251/4a679b6953e52b21/4a381495a8801718/3874e489/-cpid/97eb2a224886d83a" /><param name="wmode" value="transparent" /><param name="allowNetworking" value="all" /><param name="allowScriptAccess" value="always" /></object>

<br/>
-->
<p>Our language audio albums give you vocab sets to learn in 3 stages: Familiarise, Recognise and Memorise.</p>

<p>In the Familiarise stage you will hear the foreign word several times, sandwiched by its English translation. You'll hear the
word slowed-down too, to help with those unfamiliar sounds.</p>

<p>Once the words start to sink in move on to the Recognise stage - you'll hear the foreign word and have a few moments to recall
the English. This reinforces the learning process.</p>

<p>The final stage – Memorise – is the true test. You'll hear the English word and have a few seconds to translate it. Once you
can do that, the word is yours.</p>

<p>You will find our vocabulary learning resources easy to use and a great help in your quest to master that language.</p>

