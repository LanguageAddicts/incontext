<h1>Greek Podcasts Volume 2</span></h1> 



<p>

Catch up on archive podcasts you missed and download the second five episodes for <em>$1.99</em>.



</p>

<a href="http://www.payloadz.com/go?id=1248463" target="paypal"><img src="http://www.paypal.com/images/x-click-but22.gif" border="0" alt=" Add to Cart" ></a>

<br/>

<p>You can also download all 6 volumes of archive podcasts as a discounted bundle.</p>

        <hr/>

<h2>View volume contents</h2>

<form id="here"name="jump">

<p align="left">

<select name="menu">

<option value="#">select an episode</option>

<option value="#10">June 2008 - Countries</option>

<option value="#9">May 2008 - Months of the year</option>

<option value="#8">April 2008 - Counting to ten</option>

<option value="#7">March 2008 - Body parts</option>

<option value="#6">February 2008 - Fruit and vegetables</option>





</select>

<input type="button" onClick="location=document.jump.menu.options[document.jump.menu.selectedIndex].value;" value="Go to episode">

</p>

</form>





<table class="table_width" width="450" cellpadding="0" cellspacing="2" style="border: 1;">



<tr id="10" class="HistoricalPodcastTableRow"><td colspan="3"><h2>10. June 2008 - Countries</h2></td></tr>

<tr class="PodcastTableHeader">

<td class="PodcastTableCell">English</td>

<td class="PodcastTableCell">Greek</td>

<td class="PodcastTableCell">Phonetic</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>The United States</td>

<td>οι ΗΠΑ</td>

<td>ee EE-pah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Turkey</td>

<td>η Τουρκία </td>

<td>ee tour-KEE-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Greece</td>

<td>η Ελλάδα</td>

<td>ee el-AH-tha</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Cyprus</td>

<td>η Κύπρος</td>

<td>ee KEE-pross</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>England</td>

<td>η Αγγλία</td>

<td>ee ah-GLEE-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Germany</td>

<td>η Γερμανία</td>

<td>ee yer-mah-NEE-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>France</td>

<td>η Γαλλία</td>

<td>ee gha-LEE-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Australia</td>

<td>η Αυστραλία</td>

<td>ee af-stra-LEE-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Canada</td>

<td>ο Καναδάς</td>

<td>oh ka-na-THASS</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>China</td>

<td>η Κίνα</td>

<td>ee KEE-nah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Russia</td>

<td>η Ρωσία</td>

<td>ee ross-EE-ah</td>

</tr>

<tr><td>&nbsp;</td></tr>

<tr id="9" class="HistoricalPodcastTableRow"><td colspan="3"><h2>9. May 2008 - Months of the year</h2></td></tr>

<tr class="PodcastTableHeader">

<td class="PodcastTableCell">English</td>

<td class="PodcastTableCell">Greek</td>

<td class="PodcastTableCell">Phonetic</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>January</td>

<td>Ιανουάριος</td>

<td>ee-an-oo-AH-ree-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>Febuary</td>

<td>Φεβρουάριος</td>

<td>fev-roo-AH-ree-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>March</td>

<td>Μάρτιος</td>

<td>MAR-tee-os</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>April</td>

<td>Απρίλιος</td>

<td>ah-PRIL-ee-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>May</td>

<td>Μάιος</td>

<td>MY-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>June</td>

<td>Ιούνιος</td>

<td>ee-OO-nee-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>July</td>

<td>Ιούλιος</td>

<td>ee-OO-lee-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>August</td>

<td>Αύγουστος</td>

<td>AH-voo-stoss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>September</td>

<td>Σεπτέμβριος</td>

<td>sep-TEM-vree-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>October </td>

<td>Οκτώβριος</td>

<td>ok-TOV-ree-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>November</td>

<td>Νοέμβριος</td>

<td>no-EM-vree-oss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>December</td>

<td>Δεκέμβριος</td>

<td>the-KEM-vree-oss</td>

</tr>

<tr><td>&nbsp;</td></tr>

<tr id="8" class="HistoricalPodcastTableRow"><td colspan="3"><h2>8. April 2008 - Counting to ten</h2></td></tr>

<tr class="PodcastTableHeader">

<td class="PodcastTableCell">English</td>

<td class="PodcastTableCell">Greek</td>

<td class="PodcastTableCell">Phonetic</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>zero</td>

<td>μηδέν</td>

<td>mee-THEN</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>one</td>

<td>ένα </td>

<td>EH-na</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>two</td>

<td>δύο</td>

<td>THEE-oh</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>three</td>

<td>τρία</td>

<td>TREE-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>four</td>

<td>τέσσερα</td>

<td>TESS-er-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>five</td>

<td>πέντε</td>

<td>PEN-teh</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>six</td>

<td>έξι</td>

<td>to EKS-ee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>seven</td>

<td>εφτά</td>

<td>EF-tah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>eight</td>

<td>οχτώ</td>

<td>och-TOH</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>nine </td>

<td>εννέα</td>

<td>en-nay-ah</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>ten</td>

<td>δέκα </td>

<td>THE-ka</td>

</tr>

<tr><td>&nbsp;</td></tr>

<tr id="7" class="HistoricalPodcastTableRow"><td colspan="3"><h2>7. March 2008 - Body parts</h2></td></tr>

<tr class="PodcastTableHeader">

<td class="PodcastTableCell">English</td>

<td class="PodcastTableCell">Greek</td>

<td class="PodcastTableCell">Phonetic</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the arm</td>

<td>το μπράτσο</td>

<td>to BRA-tso</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the body</td>

<td>το σώμα</td>

<td>to SOH-ma</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the face</td>

<td>το πρόσωπο</td>

<td>to PRO-soh-poh</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the finger</td>

<td>το δάχτυλο</td>

<td>to THACH-tee-lov</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the foot as well as leg</td>

<td>το πόδι</td>

<td>to POH-thee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the hand</td>

<td>το χέρι</td>

<td>to CHE-ree</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the head</td>

<td>το κεφάλι</td>

<td>to ke-FAH-lee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the knee</td>

<td>το γόνατο</td>

<td>to GHON-ah-to</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the shoulder</td>

<td>ο ώμος</td>

<td>o OH-moss</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the thumb </td>

<td>ο αντίχειρος</td>

<td>o a-DEE-chee-rass</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the toe</td>

<td>το δάχτυλο του ποδιου</td>

<td>to THACH-tee-lo too po-thee-o</td>

</tr>

<tr><td>&nbsp;</td></tr>

<tr id="6" class="HistoricalPodcastTableRow"><td colspan="3"><h2>6. February 2008 - Fruit and vegetables</h2></td></tr>

<tr class="PodcastTableHeader">

<td class="PodcastTableCell">English</td>

<td class="PodcastTableCell">Greek</td>

<td class="PodcastTableCell">Phonetic</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the vegetables   </td>

<td> τα λαχανικά</td>

<td> ta la-chan-ee-KAH</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the aubergine  </td>

<td> η μελιτζάνα  </td>

<td>ee me-litz-AH-na</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the courgette  </td>

<td> το κολοκυθάκι</td>

<td> to ko-loh-kee-THAH-kee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the tomato     </td>

<td>η ντομάτα</td>

<td> ee doh-MAH-da</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the onion    </td>

<td>το κρεμμύδι  </td>

<td>to krem-EE-thee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the cucumber    </td>

<td>το αγγούρι</td>

<td> to ah-GOUR-ee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the fruits    </td>

<td>τα φρούτα  </td>

<td>ta FROO-ta</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the orange    </td>

<td>το πορτοκάλι  </td>

<td>to port-oh-KAH-lee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the peach    </td>

<td>το ροδάκινο  </td>

<td>to roh-THAH-kee-no</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the fig      </td>

<td>το σύκο    </td>

<td>to SEE-koh</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the melon    </td>

<td>το πεπόνι  </td>

<td>to peh-POH-nee</td>

</tr>

<tr class="HistoricalPodcastTableRow">

<td>the apple    </td>

<td>το μήλο    </td>

<td>to MEE-lo</td>

</tr>

<tr><td>&nbsp;</td></tr>



</table>





