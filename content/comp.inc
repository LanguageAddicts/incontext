<div id="comp_head" title="state the obvious">

<div id="comp_banner">
<img src="/legacy_images/shield.png" alt="Number of entries">
</div>


<div id="comp_buttons">


<!--div style="margin-bottom: 26px;">
				<a class="button" STYLE="text-decoration:none;color:"black"" href="/comp_submit.htm"  return false;"><span>submit</span></a>
</div--!>
<!--div  style="margin-bottom: 26px;">
				<a class="button" STYLE="text-decoration:none;color:"black"" href="#winners"  return false;"><span><em>winners</em></span></a>
</div-->

<span class='st_sharethis_large' displayText='ShareThis'></span>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_plusone_large' displayText='Google +1'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
<span class='st_email_large' displayText='Email'></span>
</div>
</div>
</div><!--comp buttons -->


<div id="comp_body">

<h1>Can you do a visual cue?</h1>
<h2>Winners and showcase <a href="winners_3.htm">here</a></h2>
<div class="vidblock" id="comp_spiel">

<div id="comp_text">
<p>Here at <em>Language Addicts</em> we have a simple mission, to teach the world to speak English. We're building a new site where visitors will <em>learn English through videos</em>, which is why we need your creative genius and why we're running this contest.</p>
<p>The key element to our videos is what we call the 'visual cue' and this is how the learner figures out the meaning of the words in the video without any text or translation. Watch our competition video to find out more...</p>
</div> <!-- end of comp text-->

<div  id="comp_video">
<video id="comp_vid" controls="visible" height="200">
  <source src="movies/compvid.mp4" type="video/mp4">
  <source src="movies/compvid.ogg" type="video/ogg">
  <source src="movies/compvid.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/compvid.webm">
Download it</a> instead.</p>
  </object>

</video>

</div> <!-- end of comp video-->
</div> <!-- end of comp_spiel-->



<br>
<div class="halfblock" id="details">

<p><img class="icon" src="/legacy_images/trophy.png" alt="prizes">1st = <em>$200</em><br><br> 2nd = $50<br> 3rd = $25<br><br> Any videos we use will be attributed to you and link to your homepage</p>
</div>

<div class="halfblock"  id="target">
<p><img class="icon" src="/legacy_images/target.png" alt="prizes">We want short videos that use visual cues to convey the meaning of simple, everyday vocab. The goal is to use a <em>visual cue for each word in the video</em>. </p>
</div>

<div class="halfblock"  id="timing">
<p><img class="icon" src="/legacy_images/stopwatch.png" alt="deadline">All entries to be submitted <em>by midnight GMT on June 16th</em>. Winners announced on this page on June 30th. </p>
</div>


<div class="halfblock"  id="winners">
<p><img class="icon" src="/legacy_images/idea.png" alt="ideas">Get some ideas and inspiration from our <em>previous winners...</em></p>
<p><a href="winners.htm">December 2012 - January 2013</a></p>
<p><a href="winners_2.htm">February - March 2013</a></p>
<p><a href="winners_3.htm">April - June 2013</a></p>
</div>


<div class="block"  id="checklist">
<img class="icon" src="/legacy_images/guidelines.png" alt="guidelines">
<p>
<ul>
<li>Are there <em>visual cues</em> on as many words as possible?</li>
<li>Is the video short (15 seconds or less)?</li>
<li>Are the words in the video <em>basic beginners English?</em></li>
<li>Is the spoken English clearly intelligible to the listener? (local accents are fine btw)</li>
<li>is your video engaging and fun to watch?</li>
<li><em>No written text</em> in the videos please</li>
<li>Read the full terms of the competition <a href="comp_rules.htm"><em>here</em></a></li>
</ul>
</p>
</div>

<!--div class="shortwideblock"  id="checklist">
<a href="comp_submit.htm"><img class="icon" src="/legacy_images/send.png" alt="send"></a>
<p>Before you send us your video, be sure you have understood the aims of the contest and that your video is short, fun, has simple language and has visual cues for each word. <a href="comp_submit.htm">Submit here</a>.</p>
</div--!>


</div> <!-- end of comp body-->
