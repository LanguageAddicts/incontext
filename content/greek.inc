<H1> Learn intermediate Greek vocab on your iPod</H1>
<!-- Our Greek iPod audio products are sharp, clear and comprehensible. -->
<p/>
<p>Language Addicts Greek offers you an easy way to build your greek vocabulary using our podcast and downloads.</p>
<object width="340" height="285"><param name="movie" value="http://www.youtube.com/v/z5lQ_PHuE64&hl=en_GB&fs=1&rel=0&color1=0x3a3a3a&color2=0x999999&border=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/z5lQ_PHuE64&hl=en_GB&fs=1&rel=0&color1=0x3a3a3a&color2=0x999999&border=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="340" height="285"></embed></object>

<h2 id="promo">Greek vocabulary builder downloads</h2>
<table>
<tr bgcolor="#FFFFCC">
  <td><a href="greek_albums.htm"><img src="legacy_images/content/greek_album_1.jpg" width="65" height="65" alt="Greek Album 1 Image" title="Greek Volume 1: Family and friends" /></a></td>
  <td>Download 1 - topics 1 to 7</td>
  <td>$4.99</td>
  <td>
  <a href="http://payloadz.com/go?id=345329" target="paypal" onclick="javascript:urchinTracker ('/Greek Album 1 - Payloadz Add to cart')">
          <img src="legacy_images/buttons/add_to_cart.gif" width="87" height="23" alt="Add to cart button" title="Add album to cart" />
         </a>
  </td>
</tr>
<tr bgcolor="#E6E6FA">
  <td><a href="greek_albums.htm"><img src="legacy_images/content/greek_album_2.jpg" width="65" height="65" alt="Greek Album 2 Image" title="Greek Volume 2: Home and neighbourhood" /></a></td>
  <td>Download 2 - topics 8 to 14</td>
  <td>$4.99</td>
  <td><a href="http://payloadz.com/go?id=371116" target="paypal" onclick="javascript:urchinTracker ('/Greek Album 2 - Payloadz Add to cart')">
          <img src="legacy_images/buttons/add_to_cart.gif" width="87" height="23" alt="Add to cart button" title="Add album to cart" />
         </a>
  </td>
</tr>
<tr bgcolor="#FFFFCC">
  <td><a href="greek_albums.htm"><img src="legacy_images/content/greek_album_3.jpg" width="65" height="65" alt="Greek Album 3 Image" title="Greek Volume 3: Healthy living" /></a></td>
  <td width="210">Download 3 - topics 15 to 21</td>
  <td width="50">$4.99</td>
  <td width="80">
  <a href="http://payloadz.com/go?id=371118" target="paypal" onclick="javascript:urchinTracker ('/Greek Album 3 -Payloadz Add to cart')">
          <img src="legacy_images/buttons/add_to_cart.gif" width="87" height="23" alt="Add to cart button" title="Add album to cart" />
         </a>
  </td>
</tr>

<tr bgcolor="#E6E6FA">
  <td><a href="greek_albums.htm"><img src="legacy_images/content/greek_album_3.jpg" width="65" height="65" alt="Greek Bundle" title="Greek volumes 1 to 3 plus podcasts 1 to 7" /></a></td>
  <td width="210"><em>Download All topics (1 to 21) plus podcasts episodes 1 to 7 included free</em></td>
  <td width="50">$9.99</td>
  <td width="80">
  <a href="http://www.payloadz.com/go?id=1193905" target="paypal" onclick="javascript:urchinTracker ('/Greek bundle -Payloadz Add to cart')">
  <img src="http://www.paypal.com/images/x-click-but22.gif" border="0" alt=" Add to Cart" ></a>
  </td>
</tr>
</table>
<p/>
<h2>Listen to the podcast</h2>
 Our product range includes our <a href="greek_podcasts.htm">free monthly podcast</a> which iTunes currently ranks as the most popular Greek language podcast. This is a great taster comprising of ten simple Greek words delivered to you using the Language Addicts method.
<p>

<!---->
<a href="#" onclick="window.open('http://www.podtrac.com/PodtracPlayer/podtracplayer.aspx?podcast=http://www.languageaddicts.com/podcasts/greek/greek.rss', 'linkname', 'height=235, width=450, scrollbars=no'); return false;">
<img src="http://www.podtrac.com/PodtracPlayer/playerbutton1.jpg" alt="Podtrac Player"/>
</a>
</p>
