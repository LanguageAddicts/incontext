

<div id="comp_body">
<br>
<div id="social_buttons" class="clear">
<span class='st_sharethis_large' displayText='ShareThis'></span>

<span class='st_facebook_large' displayText='Facebook'></span>

<span class='st_twitter_large' displayText='Tweet'></span>

<span class='st_linkedin_large' displayText='LinkedIn'></span>

<!--span class='st_plusone_large' displayText='Google +1'></span-->

<span class='st_pinterest_large' displayText='Pinterest'></span>

<span class='st_email_large' displayText='Email'></span>

</div>
<br class="clear">
<div>
<h1>Results of the February 2013 - March 2013 competition</h1>
<p>Our second competition received a total of 59 entries. The choice between third and fourth place ended up being so difficult that we chickened out of chosing and awarded a joint third prize this time. Check out the winners and the showcased entries below.</p>
</div>
<div class="comp_row_1">

<h1>Winners</h1>
<div class="comp_col_1">
<h2>First prize</h2>
<iframe width="560" height="315" src="http://www.youtube.com/embed/KLuj7irgAY8" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.youtube.com/pikameleon">Joel Mendez</a></p>
<p><em>Comments:</em> An all round great movie, with good visual cues on each word, high production quality. A foreign person learning english would be able to learn these words no problem.</p>
</div> <!--end of commentary-->
</div> <!--end of comp col 1-->

</div><!--end of comp row 1-->


<div class="comp_row_1">
 <!--second prize uses winner class on this page-->
  <div class="comp_col_1">
  <h2>Second prize</h2>
  <iframe width="560" height="315" src="http://www.youtube.com/embed/CkU8rDasZLc" frameborder="0" allowfullscreen></iframe>
  <div class="video_commentary">
    <p><em>Submitted by:</em><a href="http://www.youtube.com/channel/UCDDff7jBT2MZ2TCTGbM8gUw/videos?view=0&flow=grid">Johnny Duke</a></p>
    <p><em>Comments:</em> Well timed and clear cues with each word. This entry is what this competition is all about!  </p>
  </div> <!--end of commentary-->
  </div> <!--end of comp col 1/2-->
</div><!--end of comp row 1/2-->

<div class="comp_row_23">

<div class="comp_col_23">
<h2> Joint Third prize</h2>
<iframe width="356" height="200" src="http://www.youtube.com/embed/tYw1BYXbWK8" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em>Eric Harris</p>
<p><em>Comments:</em> We loved this video, the way it scientifically links each word to each action/object and it looks great too. </p>
</div><!-- commentary end-->
</div> <!--end of comp col 23-->

<div class="comp_col_23">
<h2> Joint Third prize</h2>
<iframe width="356" height="200" src="http://www.youtube.com/embed/dWAGEyb-Fr4" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.youtube.com/victorkolbe">Victor Kolbe </a></p>
<p><em>Comments:</em> Great fun video where every word is timed precisely with a visual cue - spot on.  </p>
</div><!-- commentary end-->
</div> <!--end of comp col 23-->

</div><!--end of comp row 23-->

<div class="hr"></div>
<br>
<br>
<p></p>
<h1 class="clear">Showcase</h1>
<p><em>This section showcases a handful of our more notable entries</em></p>
<br>
<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/W1X9hn9USl0" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.ldeemdesign.com/">Lukas Deem </a> </p>
<p><em>Comments:</em> Great fun movie, good visual cues.</p> 

</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="203" src="http://www.youtube.com/embed/tmT9MKFHF4M" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Nathaniel Rhone</p>
<p><em>Comments:</em> fun video, we liked the combination of real background with drawn character. Visual cues were good (jumping especially), although it's not clear that the girl is the narrator's sister. </p>

</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/Bs-eJR3Sb3A" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Sharod Bell </p>
<p><em>Comments:</em> Great production quality and acting. Need less complex language and visual cues. </p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->

<!-- ROW 2 -->

<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/jKwk5tHmMis" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Inga Zenilo  </p>
<p><em>music by:</em><a href="http://audionautix.com">audionautix.com</a></p>
<p><em>Comments:</em> Great visual cues although we did find it a bit scary!</p> 

</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/spDJj5WvnZA" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em>Nate Benson </p>
<p><em>Comments:</em> nice animation, good visual cues.</p>

</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="203" src="http://www.youtube.com/embed/mJIlTPMRsZI" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Antoni Zielinski </p>
<p><em>Comments:</em> Really good visual cues, sound a bit quiet though. </p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->


</div><!-- comp body end-->