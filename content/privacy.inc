<h1>Privacy <span class="highlight">Policy</span></h1>

<h2>Our visitors</h2>

<p>Our Web Server retains NO information about our visitors regarding domain or email address. The only information we collect
is your email address if you make a purchase or contact us via our contact form. This is retained as proof of purchase in case
you raise a product query. We do not share our information with anyone. We will not contact you unless you initiate contact
with us. The advertising links on our site do not capture information unless the user selects to follow the advertising link.
We have no control over customer privacy once they leave our site.</p>

<h2>Cookies</h2>

<p>LanguageAddicts.com does not store any cookies on your machine.</p>

<h2>Your Privacy</h2>

<p>If our information practices change at some time in the future we will prominently post the policy changes to our Web site to
notify you of these changes. If you feel that this site is not following its stated information policy or have any questions
regarding this policy, you are encouraged to <a href="index.php?view=contact">contact us</a>.</p> 
