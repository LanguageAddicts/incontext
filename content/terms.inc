<h1>Terms of <span class="highlight">Use</span></h1>

<p>The following statement explains the terms of sale and terms of use of Language Addicts.</p>

<h3>Terms of Sale</h3>
<p>These terms and conditions constitute an agreement with you ('the Buyer') and Language Addicts ('the Supplier'). By placing an order on
<a href="http://www.languageaddicts.com/" rel="external">www.languageaddicts.com</a> ('the Site') you accept these terms and conditions.
The terms and conditions are subject to change at any time, and it is your responsibility to check these terms and conditions regularly in
case there are any changes. Continuing to use the Site after an change has been made is your acceptance of the changes.</p>

<ol>

<li><p><span class="b list_head">Formation of Contract</span></p>
<p>1.1 All orders for Goods are offers by you to purchase and accepted only when you receive a confirmation email from us to the effect that
your order has been processed. This will form a contract, which incorporates these terms and conditions, between you and us ('the Contract').</p>
<p>1.2 Where the use of the Goods is stated to be subject to any instructions or warnings, they are supplied on the condition that such
instructions or warnings will be strictly adhered to.</p>
</li>

<li><p><span class="b list_head">Prices</span></p>
<p>2.1 All prices for the Goods are inclusive of sales tax and any other applicable taxes, [which the Buyer shall be required to pay to the Supplier
as part of the order value].</p>
<p>2.2 The price of an item is the price on the day of your order. We try to ensure that all prices on display on this Site are accurate but if
there is a price error you should notify us as soon as possible. Where an item's correct price is higher than the price stated on your order we
will contact you to let you know the correct price and give you the opportunity of reconfirming your order or cancelling.</p>
<p>2.3 In the case of Special offers the length of time that price will be offered will be stated on the Site.</p>
</li>

<li><p><span class="b list_head">Payment</span></p>
<p>3.1 Payment must be made by Paypal on the date on which we accept your order. Payment will be debited from your Paypal account before despatch
of the Goods to you.</p>
<p>3.2 Upon receiving your order we will verify your payment details.</p>
<p>3.3 All product prices are shown in US Dollars. Paypal will perform any currency conversion, if necessary.</p>
</li>

<li><p><span class="b list_head">Delivery</span></p>
<p>4.1 We can deliver to any valid email address.</p>
<p>4.2 All delivery times are estimates only and, while we will endeavour to avoid delay, we will not be liable to you for any loss or damage
arising from delay in delivery, but in any event we will endeavour that such delivery shall not exceed 30 days.</p>
<p>4.3 We may deliver the Goods in installments if it is deemed necessary.</p>
<p>4.4 If we are unable to perform the whole or part of the Contract due to any cause or event beyond our reasonable control we may, at our
option, by notice in writing to you, cancel or suspend the Contract in whole or in part without liability and without prejudice to our rights
to receive payment of the price for all Goods previously delivered.</p>
<p>4.5 If you receive the wrong goods or you cannot access the Goods, you must contact us within 28 calendar days and confirm to us what has
happened.</p>
</li>

<li><p><span class="b list_head">Risk</span></p>
<p>5.1 The Goods are at your risk from the time of delivery.</p>
<p>5.2 Ownership of the Goods does not pass to you until, delivery provided, that we have received in full all sums due.</p>
</li>

<li><p><span class="b list_head">Security</span></p>
<p>6.1 We take your online privacy seriously. However, although we use the Paypal secure payment gateway, the security of any payments transmitted
and processed via the Internet cannot be guaranteed. Any losses incurred or sustained by you as a result of transmitting information by means
of email or other Internet link will be borne solely and exclusively by you.</p>
</li>

<li><p><span class="b list_head">Limitation of Liability</span></p>
<p>7.1 Nothing in these Terms of Sale excludes or limits our liability for death or personal injury caused by our negligence.</p>
<p>7.2 Subject to 7.1 above, we will not be liable to you for any loss or damage in circumstances where or to the extent that:</p>
<p>7.2.1 we are not in breach of a legal duty owed to you;</p>
<p>7.2.2 such loss or damage is not a reasonably foreseeable result of any such breach; or</p>
<p>7.2.3 any increase in loss or damage results from a breach by you of any term of this contract.</p>
<p>7.3 Except as expressly provided in these Terms of Sale and save for fraudulent misrepresentation, all conditions, representations and
warranties (express or implied, statutory or otherwise) are excluded to the extent permitted by law including without limitation any implied
warranties or conditions as to quality, fitness for purpose and reasonable skill and care.</p>
<p>7.4 Under no circumstances, including negligence, shall we or any affiliated company or individual be held liable for any direct, indirect,
incidental, special or consequential damage (including, without limitation whether for loss of profit, loss of business, lost data, viruses
that may infect your computer, depletion of goodwill or otherwise), costs, expenses or other claims for economic loss (howsoever caused)
which arise out of or in connection with the Contract or your use of this Site generally.</p>
<p>7.5 We do not warrant that the Site or the products and materials contained on the Site are error free, or that errors will be corrected,
or that this Site or the server from which it is run is free of viruses or other potentially harmful codes.</p>
<p>7.6 Neither our employees not our agents are authorised to make any representations or give any warranty concerning the Goods unless these
are confirmed in writing by us. In entering into the Contract you acknowledge that you do not rely on and waive any claim for any breach of
any representation or warranty which is not so confirmed.</p>
</li>

<li><p><span class="b list_head">Indemnity</span></p><p>8.1 You agree to defend, indemnify and hold us and any affiliated company or individual harmless
from any and all liabilities, costs and expenses, including reasonable legal fees, related to any violation of these Terms of Sale by you or
your authorised users, or in connection with the use of the Site or the Internet or the placement or transmission of any message or information
on this site by you or your authorised users.</p>
</li>

<li><p><span class="b list_head">Termination/Cancellation</span></p>
<p>9.1 You may cancel your order before delivery or within 28 calendar days after the date of delivery.</p>
<p>9.2 We will credit your payment account with the cost of the unwanted Goods within 30 days of the date of your cancellation.</p>
<p>9.3 Please destroy the unwanted Goods plus any copies made within 30 days of cancellation. You are required to take reasonable care of the
Goods while in your possession.</p>
<p>9.4 If Goods are defective or damaged, please contact us. We reserve the right to check if items are damaged or defective before agreeing
to reimbursement and ask for proof of damage or defect. We will refund you the cost of damaged or defective goods.</p>
<p>9.5 Refunds will be credited to your payment account and will not be paid in cash.</p>
</li>

<li><p><span class="b list_head">General</span></p>
<p>10.1 You may cancel your order before delivery or within 28 calendar days after the date of delivery.</p>
<p>10.2 Our failure to insist upon the strict performance of any of your obligations under the Sales Terms will not be construed as a waiver
and will not affect our rights to require strict performance of such obligations.</p>
<p>10.3 If any provision of these Terms of Sale or part thereof is found to be invalid or unenforceable, the invalidity or unenforceability
of such provision or part will not affect any other provision or the remainder of the provision in which such invalid or unenforceable part
is contained, which will remain in full force and effect.</p>
<p>10.4 All notices will be in writing and maybe served by either party on the other by hand or by email.</p>
<p>10.5 Nothing in these Terms of Sale which form the agreement between you and us will give nor is intended to give rights to any third party
under the Contracts (Rights of Third Parties) Act 1999 or otherwise.</p>
<p>10.6 This site may provide links to the web sites and services of third parties. Such links are provided for your convenience only, and their
provision does not constitute or imply control of or an endorsement of the content of such third party web sites by us. You acknowledge that the
use of such third party web sites is governed by the terms and conditions of use as applicable to such websites.</p>
<p>10.7 These terms and conditions will be governed by and construed in accordance with English law and the parties will submit to exclusive
jurisdiction of the English courts.</p>
</li>
</ol>

<h3>Terms of Use</h3>
<p>Your use of this website constitutes acceptance by you of the following Terms of Use:</p>

<ol>
<li><p><span class="b list_head">Copyright and Trademarks</span><br />
This Site is owned and operated by Language Addicts, and the information and materials appearing on the Site ('the Content') are displayed for
personal, non-commercial use only. All software used on this Site and all Content included on this Site (including without limitation Site design,
text, graphics, audio and video and the selection and arrangement thereof) is the property of Language Addicts or its suppliers and is protected by
international copyright laws.</p>
<p>None of the Content may be downloaded, copied, reproduced, republished, posted, transmitted, stored, sold or distributed without the prior
written permission of the copyright holder. This excludes the downloading of one copy of extracts from the Site on any single computer for personal,
non-commercial home use only, provided that all copyright and proprietary notices are kept intact. Modification of any of the Content or use of
any of the Content for any purpose other than as set out herein (including without limitation on any other website or computer network) is prohibited.
Requests to republish any of the Content and to use quotations or extracts from any products by Language Addicts should be submitted in writing to the
address set out below.</p>
</li>

<li><p><span class="b list_head">Links to Third Party Web Sites</span><br />
The Site may include links to third party Internet websites which are controlled and maintained by others. These links are included solely for the
convenience of users and do not constitute any endorsement by Language Addicts of the sites linked or referred to, nor does Language Addicts have any
control over the content of any such sites.</p>
</li>

<li><p><span class="b list_head">Price and Availability of Titles</span><br />
Whilst every effort is made to ensure accuracy of prices and availability of the
titles featured on this Site, this cannot be guaranteed and price and availability information may change without notice.</p>
</li>

<li><p><span class="b list_head">Liability Disclaimer</span><br />
The Site is provided by Language Addicts in good faith but Language Addicts does not make
any representations or warranties of any kind, express or implied, in relation to all or any part of the Site or the Content or any websites to which
the Site is linked, and all warranties and representations are hereby excluded to the extent permitted by law.</p>
<p>The contents of the Site do not constitute advice and should not be relied upon in making, or refraining from making, any decision.</p>
<p>There is no guarantee that the Site will be free of infection by viruses or anything else which may be harmful or destructive.</p>
<p>To the extent permitted by law, Language Addicts hereby disclaims all liability (howsoever arising) in connection with any loss and/or damage,
arising out of or in connection with any use of, or inability to use, all or any part of the Content, the Site and/or any website to which the Site
is linked, or any action taken (or refrained from being taken) as a result of using any of these.</p>
</li>

<li><p><span class="b list_head">Changes to Conditions of Use</span><br />
Language Addicts reserves the right to add to or change these conditions of use and agrees to ensure that a note of the date and clause number of
any such amendments will be included as part of the conditions. Any changes will be posted to this page and it is your responsibility as a user
to ensure that you are aware of any such changes from time to time. Changes will become effective 24 hours after first posting and you will be
deemed to have accepted any change if you continue to access the Site after that time.</p>
</li>

<li><p><span class="b list_head">User Information</span><br />
Language Addicts collects information on what pages are accessed or visited by consumers,
as well as information volunteered by the consumer, such as using the contact form. Language Addicts uses this information for internal review,
in order to improve the content of the Site and to notify consumers about updates to the Site.</p>
<p>Language Addicts guarantees that if you supply your postal address on-line you will only be sent the information for which you provided your
address but if you do not wish to receive e-mail from Language Addicts in the future, or if you wish to be removed from mailing lists, please
let us know.</p>
</li>
</ol>

<h3>Disclaimer</h3>
<p>Information contained on UK websites operated by Language Addicts is provided in good faith, but Language Addicts makes no representations
or warranties of any kind, express or implied, as to the operation of the site, the information, the content, materials or products included on
this website. Except to the extent as provided by applicable law, Language Addicts disclaims all responsibility as to the information, content,
materials or products arising from the use of this site, whether direct, indirect, consequential or otherwise.</p>

<h3>Legal Notice</h3>
<p>This website is owned and operated by Language Addicts and the information and materials appearing on this website ('the Content') are
displayed for personal, non-commercial use only.</p>
<p>Please read this Legal Notice, the Terms of Use and our Privacy Policy carefully, as by accessing this website you are agreeing to abide
by the all of the conditions set out in these documents. Language Addicts expressly reserves all rights in the content, and compilation of all
content, in the website (including without limitation website design, text, graphics, audio and video and the selection and arrangement thereof)
is the property of Language Addicts or its suppliers and is protected by international copyright laws.</p>

<h3>Copyright</h3> 
<p>The Content and any software on this website may be used as a shopping and reference resource or for the purposes of private, non-commercial
viewing only. None of the Content may be downloaded, distributed, reproduced, copied, modified, republished, posted, transmitted, stored or
sold without the express, prior written permission of Language Addicts. Any permitted uses of specified Content expressly stated by Language
Addicts on the website, including the downloading, reproduction, copying, modification, distribution, republication, display or performance
of such specified Content is only permissible for personal, non-commercial use, provided that all copyright and proprietary notices are kept
intact.</p>

<h3>Permissions</h3> 
<p>Requests to republish any of this website's content and to use quotations or extracts from any products by Language Addicts should be submitted
in writing to the address set out below.</p>

<h3>Trade Marks</h3> 
<p>The Language Addicts logo and the 'Familiarise, Recognise, Memorise' strapline are registered trade marks or trade marks of Language Addicts,
and no users of this site are permitted to copy or reproduce these trade marks, or allow anyone else to do so for any reason, without the prior
written consent of Language Addicts.</p>

<h3>Warranty and Liability Disclaimer</h3> 
<p>This website is provided by Language Addicts in good faith on an 'as is', 'as available' basis and Language Addicts does not guarantee in good
faith on an 'as is', 'as available' basis and Language Addicts does not guarantee the accuracy, timeliness, completeness, performance or fitness
for a particular purpose of this website. Further, Language Addicts makes no representations or warranties of any kind, express or implied, as
to the operation of this website, the information, the content, materials or products included. Except to the extent as provided by the applicable
law, Language Addicts disclaims all responsibility as to the information, content, materials or products arising from the use of this website,
whether direct, indirect, consequential or otherwise.</p>


<p>By using this website, you agree to indemnify Language Addicts and our agents, and any third party contributors to this website, against any loss,
damages or expenses Language Addicts incurs arising out of any materials submitted by you and/or resulting from, or alleged to result from, your
use of the content (including software), or the interactive areas in a manner that breaches or is alleged to breach the terms of use of this
website.</p>

<h3>Applicable Law</h3>
<p>This website, including the content and information contained herein, shall be governed by the laws of England and the courts of England shall
be the place of jurisdiction. As such, the laws of England shall govern the disclaimer, legal notices, terms and conditions.</p>

<h3>No Representations of Warranties</h3>
<p>The accuracy, timeliness, completeness, performance or fitness for a particular purpose of this website. Further, Language Addicts makes no
representations or warranties of any kind, express or implied, as to the operation of this website, the information, the content, materials
or products included. Except to the extent as provided by the applicable law, HarperCollins disclaims all responsibility as to the information,
content, materials or products arising from the use of this website, whether direct, indirect, consequential or otherwise.</p>

<p class="b">Copyright &copy; Language Addicts</p>