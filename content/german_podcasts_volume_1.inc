<h1>German Podcasts Volume 1</span></h1> 
<p>
Catch up on archive podcasts you missed and download the first five episodes for <em>$1.99</em>.
</p>
<a href="http://www.payloadz.com/go?id=1288642" target="paypal"><img src="http://www.paypal.com/images/x-click-but22.gif" border="0" alt=" Add to Cart" ></a> 
<br/>
<h2>View volume contents</h2>

<form id="here"name="jump">
<p align="left">
<select name="menu">
<option value="#">select an episode</option>
<option value="#5">January 2010 - Appearances</option>
<option value="#4">December 2009 - In the kitchen</option>
<option value="#3">November 2009 - Characteristics</option>
<option value="#2">October 2009 - My house</option>
<option value="#1">September 2009 - Greetings and pleasantries</option>
</select>
<input type="button" onClick="location=document.jump.menu.options[document.jump.menu.selectedIndex].value;" value="Go to episode">
</p>
</form>

<table class="table_width" width="450" cellpadding="0" cellspacing="2" style="border: 1;">

<tr>
<tr id="5" class="PodcastTableRow"> 
<td colspan="3"><h2>5. January 2010 - Appearances</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  
<td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow"><td>fat</td><td>dick</td><td>DIK</td></tr>
<tr class="PodcastTableRow"><td>thin</td><td>d&uuml;nn</td><td>DOON</td></tr>
<tr class="PodcastTableRow"><td>the figure</td><td>die Figur</td><td>dee fig-OOR</td></tr>
<tr class="PodcastTableRow"><td>handsome</td><td>gut aussehend</td><td>goot AUS-say-hend</td></tr>
<tr class="PodcastTableRow"><td>pretty</td><td>h&uuml;bsch</td><td>HOOBsh</td></tr>
<tr class="PodcastTableRow"><td>strong</td><td>stark</td><td>shTARk</td></tr>
<tr class="PodcastTableRow"><td>fair</td><td>hell</td><td>HELL</td></tr>
<tr class="PodcastTableRow"><td>tanned</td><td>gebr&auml;unt</td><td>geBROYnt</td></tr>
<tr class="PodcastTableRow"><td>bald</td><td>kahl</td><td>KAAL</td></tr>
<tr class="PodcastTableRow"><td>glasses</td><td>die Brille</td><td>dee BRIL-LE</td></tr> 
<tr><td>&nbsp;</td></tr>
<!-- end of 5 -->
<!-- start of 4 -->
<tr id="4" class="PodcastTableRow"> 
<td colspan="3"><h2>4. December 2009 - In the kitchen</h2></td></tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  
<td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 
<tr class="PodcastTableRow"><td>to bake</td><td>backen</td><td>BAK-en</td></tr>
<tr class="PodcastTableRow"><td>the kettle</td><td>(der)</td><td>dehr KEH-sel</td></tr>
<tr class="PodcastTableRow"><td>the chopping board</td><td>Brett (das)</td><td>das BRET</td></tr>
<tr class="PodcastTableRow"><td>the coffee pot</td><td>Kaffeekanne (die)</td><td>KAH-fee-kan-eh</td></tr>
<tr class="PodcastTableRow"><td>the draining board</td><td>Abtropfbrett (das)</td><td>das AB-tropf-brett</td></tr>
<tr class="PodcastTableRow"><td>the can opener</td><td>Dosen&ouml;ffner (der)</td> <td>DOE-sen-oh-fner</td></tr>
<tr class="PodcastTableRow"><td>the crockery</td><td>Geschirr (das)</td><td>das ge-SHEER</td></tr>
<tr class="PodcastTableRow"><td>to peel</td><td>sch&auml;len</td><td>SHEH-len</td></tr>
<tr class="PodcastTableRow"><td>the recipie</td><td>Rezept (das)</td><td>das re-TZEPT</td></tr>
<tr class="PodcastTableRow"><td>the spoon</td><td>L&ouml;ffel (der)</td><td>dehr LOH-fell</td></tr>
<tr><td>&nbsp;</td></tr>
<!-- end of 4 -->
<!-- start of 3 -->
<tr id="3" class="PodcastTableRow"> 
<td colspan="3"><h2>3. November 2009 - Characteristics</h2></td></tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  
<td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 
<tr class="PodcastTableRow"><td>open-minded</td><td>aufgeschlossen</td><td>AUFgeshlosun</td></tr>
<tr class="PodcastTableRow"><td>weird</td><td>seltsam</td><td>SELTsam</td></tr>
<tr class="PodcastTableRow"><td>crazy</td><td>verr&uuml;ckt</td><td>ferROOKed</td></tr>
<tr class="PodcastTableRow"><td>cheerful</td><td>erfreulich</td><td>errFROYlikh</td></tr>
<tr class="PodcastTableRow"><td>grumpy</td><td>brummig</td><td>BRUMmig</td></tr>
<tr class="PodcastTableRow"><td>silly</td><td>doof</td><td>DOWF</td></tr>
<tr class="PodcastTableRow"><td>witty</td><td>witzig</td><td>VITZig</td></tr>
<tr class="PodcastTableRow"><td>laid back</td><td>gelassen</td><td>geLASSen</td></tr>
<tr class="PodcastTableRow"><td>sensible</td><td>vern&uuml;nftig</td><td>ferNOONFtig</td></tr>
<tr class="PodcastTableRow"><td>immature</td><td>unreif</td><td>unRIFE</td></tr>
<tr><td>&nbsp;</td></tr>
<!-- end of 3 -->
<!-- start of 2 -->
<tr id="2" class="PodcastTableRow"> 
<td colspan="3"><h2>2. October 2009 - My House</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  
<td class="PodcastTableCell">German</td> 
<td class="PodcastTableCell">Phonetic</td>
</tr> 
<tr class="PodcastTableRow"><td>the balcony</td><td>der Balkon</td><td>dehr BAHL-con</td></tr>
<tr class="PodcastTableRow"><td>the drawing room</td><td>der Salon</td><td>dehr SAH-lon</td></tr>
<tr class="PodcastTableRow"><td>the porch</td><td>Vorbau</td><td>dehr FOR-bow</td></tr>
<tr class="PodcastTableRow"><td>the shower</td><td>die Dusche</td><td>dee DOO-shuh</td></tr>
<tr class="PodcastTableRow"><td>the wall</td><td>die Wand</td><td>dee VAND</td></tr>
<tr class="PodcastTableRow"><td>the cloakroom</td><td>die Garderobe</td><td>dee gah-duh-ROHW-buh</td></tr>
<tr class="PodcastTableRow"><td>the gamesroom</td><td>das Spielzimmer</td><td>dass SHPEEL-tzim-uh</td></tr>
<tr class="PodcastTableRow"><td>the fireplace</td><td>der Kamin</td><td> dehr KAM-een</td></tr>
<tr class="PodcastTableRow"><td>the stairs</td><td>die treppe</td><td> dee TRE-puh</td></tr>
<tr class="PodcastTableRow"><td>the window</td><td>das Fenster</td><td> dass FEN-stuh</td></tr>
<tr><td>&nbsp;</td></tr>
<!-- end of 2 -->
<!-- episode 1--> 
<tr id="1" class="PodcastTableRow"> 
<td colspan="3"><h2>1. September 2009 - Greetings and pleasantries</h2></td> 
</tr> 
<tr class="PodcastTableHeader">
<td class="PodcastTableCell">English</td>  
<td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 
<tr class="PodcastTableRow"><td>welcome!</td><td>Willkommen!</td><td>vill-KOMM-en</td></tr>
<tr class="PodcastTableRow"><td>have fun</td><td> Viel Spass!</td><td>FEEL shPASS!</td></tr>
<tr class="PodcastTableRow"><td>pleased to meet you</td><td> Sch&ouml;n Dich kennenzulernen!</td><td>shERRN DIKH ken-nen-tzoo-LEHR-nen!</td></tr>
<tr class="PodcastTableRow"><td>to introduce</td><td>vorstellen</td><td>FOR-shtel-len</td></tr>
<tr class="PodcastTableRow"><td>to hug</td><td>umarmen</td><td>um-ARM-en</td></tr>
<tr class="PodcastTableRow"><td>with pleasure</td><td>gerne</td><td> GEHR-neh</td></tr>
<tr class="PodcastTableRow"><td>thank you</td><td>danke</td><td>DAN-kuh</td></tr>
<tr class="PodcastTableRow"><td>congratulations</td><td>Herzlichen Gl&uuml;ckwunsch!</td><td>HEHrtz-likh-en GLUEk-vunsh</td></tr>
<tr class="PodcastTableRow"><td>have a nice trip!</td><td>Gute Reise!</td><td>GOO-teh RIY-suh</td></tr>
<tr class="PodcastTableRow"><td>sleep well</td><td>Schlaf gut!</td><td>sh-LAAF GOOT</td></tr>

</tr>

</table>





