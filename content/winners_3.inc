

<div id="comp_body">
<br>
<div id="social_buttons" class="clear">
<span class='st_sharethis_large' displayText='ShareThis'></span>

<span class='st_facebook_large' displayText='Facebook'></span>

<span class='st_twitter_large' displayText='Tweet'></span>

<span class='st_linkedin_large' displayText='LinkedIn'></span>

<!--span class='st_plusone_large' displayText='Google +1'></span-->

<span class='st_pinterest_large' displayText='Pinterest'></span>

<span class='st_email_large' displayText='Email'></span>

</div>
<br class="clear">
<div>
<h1>Results of the April 2013 - June 2013 competition</h1>
<p>Our third competition received a total of 50 entries. It was great to see almost every entrant picked up on the visual cue concept.</p>
</div>
<div class="comp_row_1">

<h1>Winners</h1>
<div class="comp_col_1">
<h2>First prize</h2>

<div  id="comp_video">
<video id="comp_vid" controls height="350" poster="posters/1.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/1.mp4" type="video/mp4">
  <source src="movies/1.ogv" type="video/ogg">
  <source src="movies/1.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/1.webm">
Download it</a> instead.</p>
</video>
</div> <!-- end of comp video-->

<div class="video_commentary">
<p><em>Submitted by:</em> Jenna Neish and co-creator Cory Nakabayashi</p>
<p><em>Comments:</em> We had to watch this one twice to get the subtle cue technique. Beautifully made and engaging - 10 out of 10.</p>
</div> <!--end of commentary-->
</div> <!--end of comp col 1-->

</div><!--end of comp row 1-->


<br>
<br>
<br>
<div class="comp_row_23">

<div class="comp_col_23">
<h2> Second prize</h2>
<div  id="comp_video">
<video id="comp_vid" controls height="200" poster="posters/2.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/2.mp4" type="video/mp4">
  <source src="movies/2.ogv" type="video/ogg">
  <source src="movies/2.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/2.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->

<div class="video_commentary">
<p><em>Submitted by:</em>Adam Luchies</p>
<p><em>Comments:</em> Great fun video with really clear cues. Kids of all ages will like this one.</p>
</div><!-- commentary end-->
</div> <!--end of comp col 23-->

<div class="comp_col_23">
<h2> Third prize</h2>

<div  id="comp_video">
<video id="comp_vid" controls height="200" poster="posters/3.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/3.mp4" type="video/mp4">
  <source src="movies/3.ogv" type="video/ogg">
  <source src="movies/3.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/3.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->

<div class="video_commentary">
<p><em>Submitted by:</em> Cameron Kirschner</p>
<p><em>Comments:</em> lots of words and each one cued properly and clearly. Never seen anyone so happy to be washing their hands. </p>
</div><!-- commentary end-->
</div> <!--end of comp col 23-->

</div><!--end of comp row 23-->

<div class="hr"></div>
<br>
<br>
<p></p>
<h1 class="clear">Showcase</h1>
<p><em>This section showcases a handful of our more notable entries</em></p>
<br>
<div class="comp_row_featured">

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/A.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/A.mp4" type="video/mp4">
  <source src="movies/A.ogv" type="video/ogg">
  <source src="movies/A.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/A.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->

<div class="video_commentary">
<p><em>Submitted by:</em> Kate S.  </p>
<p><em>Comments:</em> This was a race to see how many words Kate could pack into 15 seconds (we lost count). Pointing at something is a pretty effective cue too.</p> 

</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/B.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/B.mp4" type="video/mp4">
  <source src="movies/B.ogv" type="video/ogg">
  <source src="movies/B.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/B.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->

<div class="video_commentary">
<p><em>Submitted by:</em>Kathleen Libby</p>
<p><em>Comments:</em> This was a nice adjective-packed movie, and the use of opposites is a powerful technique. where did she get hold of the T-Rex though?</p>

</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">
                                                                    <div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/C.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/C.mp4" type="video/mp4">
  <source src="movies/C.ogv" type="video/ogg">
  <source src="movies/C.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/C.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->

<div class="video_commentary">
<p><em>Submitted by:</em> Alex Taylor</p>
<p><em>Comments:</em> Great production and cues on most words. </p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->

<!-- ROW 2 -->

<div class="comp_row_featured">

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/D.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/D.mp4" type="video/mp4">
  <source src="movies/D.ogv" type="video/ogg">
  <source src="movies/D.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/D.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em><a href="http://www.youtube.com/user/TheWinterFilm">Boris Winter</a> </p>

<p><em>Comments:</em> Quirky but it works </p> 

</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">


<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/E.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/E.mp4" type="video/mp4">
  <source src="movies/E.ogv" type="video/ogg">
  <source src="movies/E.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/E.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em>Egidijus Kurtinaitis </p>
<p><em>Comments:</em> Loved the cue technique in this one and great production too.</p>

</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/F.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/F.mp4" type="video/mp4">
  <source src="movies/F.ogv" type="video/ogg">
  <source src="movies/F.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/F.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.youtube.com/user/Pikameleon">Joel Mendez</a> </p>
<p><em>Comments:</em> Another great entry from last month's winner. </p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->

<!--       -->
<!-- ROW 3 -->
<!--       -->

<div class="comp_row_featured">

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/G.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/G.mp4" type="video/mp4">
  <source src="movies/G.ogv" type="video/ogg">
  <source src="movies/G.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/G.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em> Justin bresee  </p>

<p><em>Comments:</em> Different style of movie which gets the point across clearly. </p> 

</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">


<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/H.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/H.mp4" type="video/mp4">
  <source src="movies/H.ogv" type="video/ogg">
  <source src="movies/H.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/H.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em>Elijah </p>
<p><em>Comments:</em> Really good attempt to visually cue each and every word. Not to mention a splendid moustache.</p>

</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/I.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/I.mp4" type="video/mp4">
  <source src="movies/I.ogv" type="video/ogg">
  <source src="movies/I.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/I.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em> Ryan Webster</p>
<p><em>Comments:</em> How could anyone not understand this? Clear and simple.  </p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->

<!--       -->
<!-- ROW 4 -->
<!--       -->

<div class="comp_row_featured">

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/J.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/J.mp4" type="video/mp4">
  <source src="movies/J.ogv" type="video/ogg">
  <source src="movies/J.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/J.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.youtube.com/RoosterReelCinema">Brandon Crooks</a>  </p>

<p><em>Comments:</em> A lot of work went into this, great effort.</p> 

</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">


<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/K.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/K.mp4" type="video/mp4">
  <source src="movies/K.ogv" type="video/ogg">
  <source src="movies/K.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/K.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em><a href="http://www.youtube.com/user/ReadySetGoPictures">Lane Stevens</a> </p>
<p><em>Comments:</em>Reminds us of the look of the video game Limbo. Cues not quite perfect but intriguing to watch. </p>

</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<div  id="comp_video">
<video id="comp_vid" controls height="150" poster="posters/L.jpg" preload="none" onerror="videoFail(this)">
  <source src="movies/L.mp4" type="video/mp4">
  <source src="movies/L.ogv" type="video/ogg">
  <source src="movies/L.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/L.webm">
Download it</a> instead.</p>
</video>
</div> <!--end of comp video-->
<div class="video_commentary">
<p><em>Submitted by:</em> Inga Zenilo </p>
<p><em>Comments:</em> This video is great and only narrowly missed a top spot. Loved the style and good cues. </p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->


</div><!-- comp body end-->