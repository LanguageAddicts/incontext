<h1>Greek Podcasts Volume 4</span></h1> 



<p>

Catch up on archive podcasts you missed and download the fourth set of five episodes for <em>$1.99</em>.



</p>

<a href="http://www.payloadz.com/go?id=1248470" target="paypal"><img src="http://www.paypal.com/images/x-click-but22.gif" border="0" alt=" Add to Cart" ></a>

<br/>

<p>You can also download all 6 volumes of archive podcasts as a discounted bundle.</p>

        <hr/>

<h2>View volume contents</h2>

<form id="here"name="jump">

<p align="left">

<select name="menu">

<option value="#20">April 2009 - Nature</option>

<option value="#19">March 2009 - Places to shop</option>

<option value="#18">February 2009 - materials</option>

<option value="#17">January 2009 - big numbers</option>

<option value="#16">December 2008 - Telling the time</option>





</select>

<input type="button" onClick="location=document.jump.menu.options[document.jump.menu.selectedIndex].value;" value="Go to episode">

</p>

</form>





<table class="table_width" width="450" cellpadding="0" cellspacing="2" style="border: 1;">

			<!-- episode 20--> 

		<tr id="20" class="HistoricalPodcastTableRow"> 

		<td colspan="3"><h2>20. April 2009 - Nature</h2></td> 

		</tr> 

		<tr class="PodcastTableHeader"> 

		<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">Greek </td> 

		<td class="PodcastTableCell">Phonetic</td></tr> 



	<tr class="HistoricalPodcastTableRow"> 		

<td>the countryside </td>  <td> η εξοχή </td>  <td> ee eks-oh-chee (ch as in loch)</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the field </td>  <td> το χωράφι </td>  <td> toh cho-RAH-fee (ch as in loch)</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the flowers </td>  <td> τα λουλούδια </td>  <td> ta loo-LOO-thee-ah</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the hill </td>  <td> ο λόφος </td>  <td> oh LOH-foss</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the lake </td>  <td> η λίμνη </td>  <td> ee LIM-nee</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the mountain </td>  <td> το βουνό </td>  <td> toh vour-NOH</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the plants </td>  <td> τα	φυτά </td>  <td> ta fee-TAH</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the river </td>  <td> το ποτάμι </td>  <td> toh poh-TAH-mee</td></tr>

<tr class="HistoricalPodcastTableRow"> 		

<td>the tree </td>  <td> το δέντρο </td>  <td> toh THEN-troh</td></tr>

<tr><td>&nbsp;</td></tr>

	<!-- End of episode 20-->

	

		<!-- episode 19--> 

		<tr id="19" class="HistoricalPodcastTableRow"> 

		<td colspan="3"><h2>19. March 2009 - Places to shop</h2></td> 

		</tr> 

		<tr class="PodcastTableHeader"> 

		<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">Greek </td> 

		<td class="PodcastTableCell">Phonetic</td></tr> 

		

<tr class="HistoricalPodcastTableRow"> 		

<td>the bakery</td>  <td>ο φούρνος</td>  <td>oh - four-NOSS</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the bookshop</td>  <td>το βιβλιοπωλείο</td>  <td>toh viv-lee-oh-poh-LEE-oh</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the butcher's</td>  <td>το κρεοπωλείο</td>  <td>toh kre-oh-poh-LEE-oh</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the florist</td>  <td>το ανθοπωλείο</td>  <td>toh an-tho-poh-LEE-oh</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the greengrocer's</td>  <td>το μανάβικο</td>  <td>toh ma-NAV-ee-koh</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the grocer's</td>  <td>το μπακάλικο</td>  <td>toh bak-AH-lee-koh</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the market</td>  <td>η αγορά</td>  <td>ee ah-gor-AH</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the shop</td>  <td>το κατάστημα</td>  <td>to kah-TAS-tee-mah</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the shopping centre</td>  <td>το εμπορικό κέντρο</td>  <td>to em-poh-ree-KOH KEN-tro</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>the superstore</td>  <td>το πολυκατάστημα</td>  <td>to pol-ee-ka-TAST-ee-mah</td></tr>



		<tr><td>&nbsp;</td></tr>

		<!-- end of episode 19-->

		



 

		<!-- episode 18--> 

		<tr id="18" class="HistoricalPodcastTableRow"> 

		<td colspan="3"><h2>18. February 2009 - materials</h2></td> 

		</tr> 

		<tr class="PodcastTableHeader"> 

		<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">Greek </td> 

		<td class="PodcastTableCell">Phonetic</td></tr> 



<tr class="HistoricalPodcastTableRow"> 

<td>concrete </td>  <td> το μπετόν </td>  <td> to bey-TON</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>glass </td>  <td> το γυαλί </td>  <td> to ya-LEE</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>iron </td>  <td> το σίδερο </td>  <td> to SEE-the-ro</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>materials </td>  <td> τα υλικά </td>  <td> ta ee-lee-KAH</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>plastic </td>  <td> το πλαστικό </td>  <td> to plah-stee-KOH</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>rock </td>  <td> ο βράχος </td>  <td> o BRAH-choss (Ch as is loch)</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>rubber </td>  <td> το λάστιχο </td>  <td> to LAH-stee-choh (ch as in loch)</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>steel </td>  <td> το ατσάλι </td>  <td> to at-SAH-lee</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>stone </td>  <td> η πέτρα </td>  <td> ee PEH-trah</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>wood </td>  <td> το ξύλο </td>  <td> to KSEE-loh</td></tr>

<tr><td>&nbsp;</td></tr>

		<!-- end of episode 18--> 



		

		<!-- episode 17--> 

		<tr id="17" class="HistoricalPodcastTableRow"> 

		<td colspan="3"><h2>17. January 2009 - big numbers</h2></td> 

		</tr> 

		<tr class="PodcastTableHeader"> 

		<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">Greek </td> 

		<td class="PodcastTableCell">Phonetic</td></tr> 



		<tr class="HistoricalPodcastTableRow"> 

<td>one hundred </td>  <td> εκατό </td>  <td> eh-ka-TO</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>one hundred and one </td>  <td> εκατόν ένα </td>  <td> eh-ka-TON EH-na</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>two hundred </td>  <td> διακόσια </td>  <td> thee-ah-KOSS-ee-ah</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>three hundred </td>  <td> τριακόσια </td>  <td> tree-ah-KOSS-ee-ah</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>one thousand </td>  <td> χίλια </td>  <td> CHI-lee-ah</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>two thousand </td>  <td> δύο χιλιάδες </td>  <td> THEE-oh chi-lee-AH-thez</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>three thousand </td>  <td> τρεις χιλιάδες </td>  <td> trees chi-lee-AH-thez</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>one million </td>  <td> ένα εκατομμύριο </td>  <td> EH-na eh-ka-to-MEE-ree-oh</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>hundredth </td>  <td> εκατοστός </td>  <td> eh-ka-toss-TOSS</td></tr>

<tr class="HistoricalPodcastTableRow"> 

<td>thousandth </td>  <td> χιλιοστός </td>  <td> chi-lee-oss-TOSS</td></tr>



<tr><td>&nbsp;</td></tr>

		<!-- end of episode 17--> 

		

		<!-- episode 16--> 

		<tr id="16" class="HistoricalPodcastTableRow"> 

		<td colspan="3"><h2>16. December 2008 - Telling the time</h2></td> 

		</tr> 

		<tr class="PodcastTableHeader"> 

		<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">Greek </td> 

		<td class="PodcastTableCell">Phonetic</td></tr> 

		

<tr class="HistoricalPodcastTableRow"> 		

<td>at ten </td>  <td> στις δέκα </td>  <td> stees THE-kah</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's 8 am </td>  <td> Είναι οχτώ το πρωί </td>  <td> IH-neh och-TOH to proh-EE</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's 8 pm </td>  <td> Είναι οχτώ το βράδι </td>  <td> IH-neh och-TOH to VRAH-thee</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's almost six </td>  <td> Είναι σχεδόν έξι </td>  <td> IH-neh ske-THON EK-see</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's exactly five </td>  <td> Είναι πέντε ακριβώς </td>  <td> IH-neh PEN-teh ak-ree-VOS</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's five past four </td>  <td> Είναι τέσσερις και πέντε </td>  <td> IH-neh TESS-er-iss keh PEN-teh</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's half past four </td>  <td> Είναι τεσσεράμιση</td>  <td> IH-neh tess-er-AM-ee-see</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's one o'clock </td>  <td> Είναι μία η ώρα </td>  <td> IH-neh MEE-ah OR-ah</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's quarter past four </td>  <td> Είναι τέσσερις και τέταρτο </td>  <td> IH-neh TESS-er-iss keh TET-ar-to</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>it's quarter to four </td>  <td> Είναι τέσσερις παρά τέταρτο </td>  <td> IH-neh TESS-er-iss pa-RAH TET-ar-to</td></tr> 

<tr class="HistoricalPodcastTableRow"> 

<td>what time is it </td>  <td> τι ώρα είναι </td>  <td> tee -OR-ah IH-neh</td></tr> 

		

<tr><td>&nbsp;</td></tr> 

</table>





