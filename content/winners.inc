

<div id="comp_body">
<br>
<div id="social_buttons" class="clear">
<span class='st_sharethis_large' displayText='ShareThis'></span>

<span class='st_facebook_large' displayText='Facebook'></span>

<span class='st_twitter_large' displayText='Tweet'></span>

<span class='st_linkedin_large' displayText='LinkedIn'></span>

<!--span class='st_plusone_large' displayText='Google +1'></span-->

<span class='st_pinterest_large' displayText='Pinterest'></span>

<span class='st_email_large' displayText='Email'></span>

</div>
<br class="clear">
<div>
<h1>Results of the December 2012 - January 2013 competition</h1>
<p>Our inaugural competition received a total of 76 entries. The winners as well as some other noteworthy entries are shown here. We hope you enjoyed taking part in the contest as much as we enjoyed judging it - X Factor eat your heart out! </p>
</div>
<div class="comp_row_1">

<h1>Winners</h1>
<div class="comp_col_1">
<h2>First prize</h2>
<iframe width="560" height="315" src="http://www.youtube.com/embed/MQ6FipvjgPA" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Ernestas Jasilionis</p>
<p><em>music from:</em> <a href="http://audionautix.com">audionautix.com</a></p>
<p><em>Comments:</em> What an amazing video! Beautifully made, fun to watch and most important, good visual cues linking the spoken words the the action in the video.</p>
</div> <!--end of commentary-->
</div> <!--end of comp col 1-->

</div><!--end of comp row 1-->


<div class="comp_row_23">

<div class="comp_col_23">
<h2>Second prize</h2>
<iframe width="356" height="200" src="http://www.youtube.com/embed/bNhaYbRhLRQ" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Tom Cowan</p>
<p><em>Comments:</em> Great production quality. Fun to watch. Loved the cartoon technique. The visual cues were timed perfectly with the spoken words. All in all a great looking and blindingly obvious video!</p>
</div> <!--end of commentary-->
</div> <!--end of comp col 23-->

<div class="comp_col_23">
<h2>Third prize</h2>
<iframe width="356" height="200" src="http://www.youtube.com/embed/oD7yxLIsd_c" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.youtube.com/user/EpicVideoChannel">Fawad Ahmed</a></p>
<p><em>Comments:</em> These guys completely understood the goal of the competition and timed each word with an obvious gesture. They made their statements really obvious through actions. It was great to see that such an effective video is possible with just a few friends and a video camera.</p>
</div><!-- commentary end-->
</div> <!--end of comp col 23-->

</div><!--end of comp row 23-->

<div class="hr"></div>
<br>
<br>
<p></p>
<h1 class="clear">Showcase</h1>
<p><em>This section showcases a handful of our more notable entries</em></p>
<br>
<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/CUO3LzjUwvU" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Liam Vickers</p>
<p><em>Comments:</em> Wow! This is a stunning short video, really well made and exciting.</p> 
<p><em>Improvement:</em>It would have had a top spot if the words had been clearer and visual cues had been used</p>
</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="203" src="http://www.youtube.com/embed/PsLdFrtETgY" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Abi Codner</p>
<p><em>Comments:</em> This video tackled a more difficult concept (thirst) and got the message across through clear gestures.</p>
<p><em>Improvement:</em>voice a bit louder in the mix</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/G8vUGHS1dpU" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Brytni Lee</p>
<p><em>Comments:</em>We really liked the visual cue technique of showing an image timed with each spoken word. This is an effective alternative to the use of arrows or highlighting..**Note: This video was submitted before we clarified the rules on leaving text out of the videos</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->


<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/bM_rRwXSHR0" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> McKay Olson</p>
<p><em>Comments:</em>Really fun to watch with a funny conclusion. The overacting works to get the message across
<p><em>Improvement:</em>
Timing the gestures with the words. For example, the spoken word 'where' would be more obvious when coupled with the 'hand over the eyes' gesture.</p>
</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/SwnjF8dJvHE" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://greglandgraf.wordpress.com">Greg Landgraf</a></p>
<p><em>Comments:</em> Nicely timed and clear video with good visual cues. The strong, clear voice is also a real asset.</p>
<p><em>Improvement:</em>
make it clearer that the man is a cook (something silly like a chef&apos;s hat?). pedantic but the chef holds up a single carrot and says &apos;carrots&apos;</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="203" src="http://www.youtube.com/embed/TUJu5RrQL9c" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> <a href="http://www.clinics.webs.com">Jyotsna Puthran</a></p>
<p><em>Comments:</em>really fun, engaging video (how do you train a cat to read?)</p>
<p><em>Improvement:</em>
visual cues on cat and book</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->


<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/YaBS0CIj8Q0" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em>Thomas Violet</p>
<p><em>Comments:</em>
good humour and attempt at a difficult word (&apos;really&apos;)

</p>
<p><em>Improvement:</em>
more exaggerated gestures as visual cues</p>
</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/1nXP8wg3nJA" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Joel Mendez</p>
<p><em>Comments:</em> Really like the freeze frame as a visual cue
</p>
<p><em>Improvement:</em>
Add visual cues in the second part of the video (for flip and table)</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/GmTwKeDTH-U" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em>  <a href="http://www.youtube.com/user/EpicVideoChannel">Fawad Ahmed</a></p>
<p><em>Comments:</em>great use of gestures as visual cues for each word</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->




<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/EgvzgPns544" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em>Emiliano Lozano</p>
<p><em>Comments:</em>
Great use of highlighting for visual cues and exciting to watch.
</p>
</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/vQ_O42__I0k" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em><a href="http://greglandgraf.wordpress.com">Greg Landgraf</a></p>
<p><em>Comments:</em> Really clear and well timed visual cues</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/83b-u_5Mu_I" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Dan Davison</p>
<p><em>Comments:</em>
Fun quirky video with great visual cues</p>
<p><em>Improvement:</em>
visual cue for the word 'brown'</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->



<div class="comp_row_featured">

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/eF_I8BQWYhk" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em>Ernestas Jasilionis</p>
<p><em>music from:</em> <a href="http://audionautix.com">audionautix.com</a></p>
<p><em>Comments:</em>

Visually stunning and engaging  </p>
<p><em>Improvement:</em>
Visual cues could be clearer
</p>
</div> <!--end of commentary-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="203" src="http://www.youtube.com/embed/DLyG0P8vdtQ" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Jason Sapp</p>
<p><em>Comments:</em> 
Good visual cues and good attempt at visually cueing a verb using arrows for the word &apos;watching&apos;. 
</p>
<p><em>Improvement:</em>
Better quality graphic for cues</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

<div class="comp_col_featured">

<iframe width="270" height="152" src="http://www.youtube.com/embed/HcD0z3N_4no" frameborder="0" allowfullscreen></iframe>
<div class="video_commentary">
<p><em>Submitted by:</em> Tom Cowan</p>
<p><em>Comments:</em>

Great production quality and natural flow. Also good scene set up </p>
<p><em>Improvement:</em>
Visual cues timed with the spoken words</p>
</div><!-- commentary end-->
</div> <!--end of comp col featured-->

</div><!--end of comp row featured-->


</div><!-- comp body end-->