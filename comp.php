<?php
//competition details page where the ins and outs of the process are desribed


require("db_functions.php");
require("common/trace_functions.php");
require("page_elements.php");

traceStart();											//start the trace file
error_reporting(E_ALL);
openPage();
drawHead("Language Addicts English - Competition");
drawBody();
closePage();


function drawBody()
{
//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo "</body>";
}

function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();

//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
drawWinners();
//drawTiming();
//drawHeading();
drawCompSpiel();
drawVisualCues();

echo "<br>";
drawDetails();
drawTarget();
drawInspiration();
//drawWinners();
drawChecklist();
//drawSubmission();



echo "</div><!--end of content-->";
}


function drawWinners()
{
echo<<<EOF
<div class="shortwideblock" id="winners">
<img class="icon" src="images/winners.png" alt="why">
<div class="block_text">


<h2>Prolific Producer Prize</h2>
<p>With a combined total of 61 entries from Joi, Andrew and Zalan it was a close run thing. However, with an outstanding 29 accepted 
entries the winner of the $150 prolific Producer prize goes to <a href="http://vidyadharfilmmaker.blogspot.in/">Vidyadhar Kagita</a>. 
To see all of Vidyadhar's work take a look at his <a href="http://www.languageaddicts.com/?author=70">gallery</a> on the main page. </p>

<h2>Gifted Producer Prize</h2>
<p>It's always difficult to pick the best video and this time was no different. 
In the end we chose the animated scene from <a href="http://www.languageaddicts.com/?vid=229">Adam Luchies</a>, which we liked for its sense of fun, quality presentation and innovative audio cue on the word 'buy'.</p>

<p>A big thank you to everyone who took part, especially those who took on board our pernickety rejections and revised and resubmitted their work. We look forward to working with you again soon. </p>
</div> <!-- end of block_text-->
</div> <!-- end of winners-->
EOF;
}
function drawCompSpiel()
{
echo<<<EOF

<div class="shortwideblock" id="comp_spiel">
<div class="grey-out">
<div id="comp_text">
<h1>We will <a href="#sweeteners">pay</a> you to enter our contest</h1>
<h2>You read that right and no we're not crazy</h2>

<p>We’re building a library of short movies that each aim to demonstrate the meaning of English <a href="wordlist.php">words</a>. We want more movies, your movies, in our library.
If your movie is one of the first 500 to pass our <a href="#quality_gate">quality control</a> you will get <a href="#sweeteners">$2</a>.
Naturally, you are also in with a chance of winning a <a href="#prizes">prize</a>.</p>


</div> <!-- end of comp text-->

<div  id="comp_video">
<video poster="images/promovideo.jpg" id="comp_vid" controls="visible" height="300">
  <source src="movies/promovideo.mp4" type="video/mp4">
  <source src="movies/promovideo.ogg" type="video/ogg">
  <source src="movies/promovideo.webm" type="video/webm">
  <p>Your browser can&apost play this HTML5 video. <a href="movies/promovideo.webm">
Download it</a> instead.</p>
  </object>

</video>

</div> <!-- end of comp video-->
</div> <!-- end of grey-out-->
</div> <!-- end of comp_spiel-->

EOF;
}

function drawVisualCues()
{
echo<<<EOF
<div class="shortwideblock" id="visual_cues">
<div class="grey-out">
<!--div class="vidblock" id="comp_spiel2"-->
<img class="icon" src="images/target.png" alt="why">
<div class="block_text">


<h2>The brief - visual cues</h2>
<p>Our movies will be watched by people who don&apos;t understand English yet. 
So we use a technique called <span class="bold">'visual cues'</span> to help people learn for themselves. 
A visual cue is a visual link between the word you can hear and the thing it describes. 
See some of our <a href="/index.php?author=68"><b>previous entries here</b></a> for inspiration.
Don’t worry if your video isn't accepted. We’ll tell you why it didn’t make the grade and you can revise it and resubmit. 
Find a few tips and ideas <a href ="#inspiration">here</a>. </p>
</p>
</div> <!-- end of block_text-->


<!--/div--> <!-- end of comp_spiel2-->
</div> <!-- end of grey-out-->
</div> <!-- end of visual_cues-->
EOF;
}

function drawInspiration()
{
echo<<<EOF
<div class="shortwideblock" id="inspiration">
<div class="grey-out">
<!--div class="vidblock" id="inspiration"-->
<img class="icon" src="images/tips.png" alt="why">
<div class="block_text">


<h2>Tips and tricks</h2>
<p>We gave up trying to make our own visual cue videos because our movie making skills are lame. 
However, we did get a few ideas along the way about what techniques work.
In no particular order, here are some techniques you might try to convey word meanings:
<ul>
<li><b>Create and submit just one movie to start with</b>. This will confirm that you are making the right kind of content before you go on to make more.</li>
<li>For nouns (i.e. objects in the world you can see like cats and bananas) highlight the object in some way when the word is spoken. Some techniques:<ul> <li>outlines around the object</li> <li>add an arrow pointing to it</li><li> change its colour</li><li>show a separate picture of the object in another part of the screen</li></ul></li>
<li>Use opposites to convey meaning i.e. This cup is <u>full</u>, this cup is <u>empty</u></li>
<li>Use things and people that are globally recognisable. If the viewer already knows what or who something is they can focus more on the word meanings</li>
<li>Play your movie to someone else with the sound off. Can they guess what words are being spoken? If not, your movie might not make sense to a non-English speaker either</li>
<li>If a person is appearing in the video, they can point at objects and use gestures to convey meaning</li>
<li>Make sure the actual speech in the video sounds natural. Our visitors will speak English the way they hear it in your movies so don't make them talk wierd ;-)</li>
<li>Repetition. Repeating a key word helps to anchor the meaning</li>
<li>Short sentences work better than long ones</li>
<li>Make a series of movies around a theme. This will allow you to re-use a formula, props or a scene for the Prolific Producer prize</li>
<li>It must be engaging and/or effective, the viewer has to get something out of watching it</li>
<li>Interesting and well timed sound effects add emphasis and fun</li>
<li>Create a scene in a game like Minecraft and add a voiceover</li>

</ul>
</div> <!-- end of block_text-->

<!--/div--> <!-- end of comp_spiel2-->
</div> <!-- end of grey-out-->
</div> <!-- end of visual_cues-->
EOF;
}

function drawDetails()
{
echo<<<EOF
<div class="shortwideblock" id="details">
<div class="grey-out">
<img class="icon" src="images/trophy.png" alt="why">
<div class="block_text">
<h2 id="prizes">Prizes</h2>
<p>There are two prizes in this contest: one for the most Prolific Producer and one for the most Gifted Producer.</p>

<h3>Prolific Producer</h3>
<p>This <span class="highlight_text">$150</span> prize goes to the individual contributor 
who submits the highest number of accepted videos before the the contest closes. 
In the event of two or more contributors tying for first place, the prize money will be evenly split.</p>
<h3>Gifted Producer</h3>
<p>This <span class="highlight_text">$50</span> prize will be awarded to the best accepted video. 
It will be the video that stands out from the crowd in some special way. 
It might be the best looking, the most fun or maybe the most innovative or effective.</p>
<h3>Stretch goal</h3>
<p>If more than 200 videos are accepted into our library during the contest period we will 
double the prize money to <span class="highlight_text">$300</span> and <span class="highlight_text">$100</span> respectively</p>
<h3 id="sweeteners">Sweeteners</h3>
<p>We want to encourage you to make as many good videos as possible so we're going to pay <span class="highlight_text">$2</span> 
for every one of the first 500 accepted entries we receive.
Entries we receive after that will go into the main competition as normal. 
Make 10 or more accepted videos and we'll pay you an additional one-off <span class="highlight_text">$10</span>.
Your name and a link to your site will be displayed alongside your work whenever it is played on our site.
Important! To be eligible for the contest your video must be of a high enough <a href="#quality_gate">standard</a> to get into the library.
All payments are made by <img class="inlineimg" src="images/paypal.png"> at the <a href="#timing">end</a> of the contest.
</p>
</div>
</div> <!-- end of grey-out-->
</div>
EOF;
}

function drawTarget()
{
echo<<<EOF
<div class="shortwideblock"  id="mechanics">
<div class="grey-out">
<img class="icon" src="images/mechanics.png" alt="prizes">
<div class="block_text">
<h2>How it works</h2>
<p>This competition is all about building a large library of fun and engaging videos. Our video <a href="#quality_gate">guidelines</a> and <a href="#details">prizes</a> are designed to encourage this.</p>
<ol> 
<li>	Create a video that meets our <a href="#quality_gate">guidelines</a></li>
<li>	Submit the finished video via our <a href="comp_submit.php">online submission form</a> </li>
<li>	Receive official receipt email if your video is successfully uploaded to our server</li>
<li>	Receive acceptance email from Language Addicts if your video is accepted into our video library OR </li>
<li>	Receive official rejection email with explanation as to why your video didn&apos;t make it. We'll be constructive and you are free to edit your video and re-submit </li>
<li>	If your video is accepted it will appear in our <a href="/">gallery</a> with your name and link
<li>	At the end of contest, receive payment for your accepted videos and, if you won, your prize money</li>
</ol>
<p>All accepted entries will be displayed on our <a href="/index.php">main page</a> so you can keep track of the competition. 
From here you&apos;ll also be able to share your entry on the social media platform of your choice. Click <a href="comp_rules.php">here</a> for full terms and conditions.
</p>
</div>
</div> <!-- end of grey-out-->
</div>
EOF;
}

function drawTiming()
{
echo<<<EOF
<div class="shortwideblock"  id="timing">
<div class="grey-out">
<p><img class="icon" src="images/stopwatch.png" alt="deadline"><span class="highlight_text">Competition closed</span> at<b> 23:59 <a href="http://www.timeanddate.com/library/abbreviations/timezones/eu/bst.html">BST</a> (British Summer Time) on June 30th 2014</b>. Winners will be announced on this page on or before July 20th 2014. </p>
<p class="highlight_text"> All contestants with an accepted video will be informed by email once the final results are known.</p>
</div> <!-- end of grey-out-->
</div>
EOF;
}


function drawChecklist()
{
echo<<<EOF
<div class="shortwideblock"  id="checklist">
<div class="grey-out">
<img class="icon" src="images/quality.png" alt="guidelines">
<div class="block_text">
<h2 id="quality_gate">Quality Control</h2>
<p>To get into the contest, your video must pass our quality checks:</p>
<ul>
<li>	Are there <a href ="#visual_cues">visual cues</a> on as many words as possible?</li>
<li>	 Does the video have good production quality?</li>
<ul><li>Preferred aspect ratio 16:9 (widescreen). Resolution - the higher the better</li></ul>
<li>	Is the video short (15 seconds or less)? </li>
<li>	Does the video contain at least one word from our <a href="wordlist.php">wordlist</a>? (even better if you can fit in more than one) </li>
<li>	Is the spoken English clearly intelligible to the listener? (local accents are fine by the way) </li>
<li>	Is your video engaging and fun to watch? </li>
<li>	Our visitors can&apos;t read English so no written text in the videos please </li>
<li>	Please note that the <b>judges decision is final</b>. If we reject your video we will provide honest and constructive feedback as to why. You are always free to amend and re-submit a rejected video. </li> 
<li>	Ulimately, the video must help someone learn English. If it doesn't, it won't get in.</li>
</ul>

</div>	<!--end of block_text-->
</div> <!-- end of grey-out-->
</div> <!--end of shortwideblock-->
EOF;
}

function drawSubmission()
{
echo <<<EOF
<div class="shortwideblock"  id="checklist">
<a href="comp_submit.php"><img class="icon" src="images/send.png" alt="send"></a>
<p>Before you send us your video, be sure you have understood the aims of the contest and that your video is short, fun, has simple language and has visual cues for each word. <a href="comp_submit.php">Submit here</a>.</p>
</div>
EOF;
}


?>