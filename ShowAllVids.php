<?php session_start();
/**
 * @author Timothy Clements
 * @copyright 2011
 */
// DB functions and form display
	require ("addvid_db_functions.php");
	require ("db_functions.php");

    // Switch statement based on value passed from the button on the validate\add forms

    // There is a switch on the button whic is setup thus
    //	<input type="submit" name="button" value="delete">
    switch (@$_POST['button'])
    {

            // User clicked on "attribute" for a particular video in the show all vids form
        case "Add attribution?";

            // Get values from the attt
            $attribute = $_POST['attribute'];
            $attributeUrl = $_POST['attributeurl'];
            $videoToAttribute = $_SESSION['VideoToAttribute'];

            // Open database
            opendb();

            //First get the latest sequence for the video id
            $query = "SELECT sequence FROM attribution WHERE VIDEOS_VIDEOID ='$videoToAttribute' ORDER BY sequence DESC LIMIT 1";
            $result = mysqli_query($connection, $query) or die($query);
            $resultarray = mysqli_fetch_assoc($result);
            $sequence = $resultarray["sequence"];
            // Increment sequence by one
            ++$sequence;

            $query = "INSERT INTO attribution (videos_videoid, sequence, attribution, attributionurl) VALUES('$videoToAttribute', '$sequence', '$attribute','$attributeUrl')";
            $result = mysqli_query($connection, $query) or die($query);
            header("Location: ShowAllVids.php");
            // print("<script language = 'javascript'>alert('Video deleted');</script>");
            // exit();


            break;

        case "attribute":
            // User clicked on "attribute" for a particular video in the show all vids form

            $_SESSION['VideoToAttribute'] = $_POST['VideoToAttribute']; // Add the video id to the session
            header("Location: addattribute.php"); // Call the upload script
            
            break;

        case "upload":
            // User clicked on "upload" for a particular video in the show all vids form

            $videoToUpload = $_POST['VideoToUpload']; // Get the videoid of the video they want to upload
            $_SESSION['videoToUpload'] = $videoToUpload; // Add it to the session
            $_SESSION['uploadType'] = "Video"; // Set the upload type to video and add it to the session
            header("Location: upload.php"); // Call the upload script

            break;
            
            
        case "thumb":
            // User clicked on "upload" for a particular video in the show all vids form
            $thumnToUpload = $_POST['ThumbToUpload']; // Get the videoid of the video they want to upload
            $_SESSION['thumbToUpload'] = $thumnToUpload; // Add it to the session
            $_SESSION['uploadType'] = "Thumb"; // Set the upload type to video and add it to the session
            header("Location:upload.php"); // Call the upload script

            break;

        case "submit":

            $thumbToUpload = $_POST['thumbToUpload'];
            $_SESSION['thumbToUpload'] = $thumbToUpload;
            $_SESSION['uploadType'] = "Thumb";
            if (isset($_POST['thumbToUpload']))
            {
                echo $thumbToUpload;
            } else
            {
                echo 'thumb not set';
            }

            echo $thumbToUpload;
            // header("Location: upload.php");

            break;

        case "Add":

            // Get new words array and new word count from session
            $newWords = $_SESSION['newWords'];
            $newWordCount = $_SESSION['WordCount'];
            $deleteEditVideo = $_SESSION['DeleteEditVideo'];

            // Open database
            opendb();

            // Protect against sql injection
            foreach (array_keys($_POST) as $key)
            {
                $_POST[$key] = mysqli_real_escape_string($connection, $_POST[$key]);
            }

            // Set scenario
            $scenario = $_POST[scenario];

            // User has validated the data entry values and has pressed the add button
            // Update new words array with data from form passed in via $_POST
            for ($i = 0; $i <= $newWordCount - 1; $i++)
            {

                $Sequence = $i + 1;

                $newWords[$i][newword] = $_POST[newword_ . $Sequence];
                $newWords[$i][wordtime] = $_POST[wordtime_ . $Sequence];
                $newWords[$i][notes] = $_POST[notes_ . $Sequence];

            }

            // Initialise unique word counter to add to video file later
            $uniqueWordCount = 0;

            for ($i = 0; $i <= $newWordCount - 1; $i++)
            {

                if ($newWords[$i][newword] == "checked")
                {
                    // Add new words to words file
                    $insertWord = $newWords[$i][word];
                    $insertNotes = $newWords[$i][notes];
                    $add_words = "insert into words(WORD, NOTES) values ('$insertWord','$insertNotes')";
                    $result = mysqli_query($connection, $add_words) or die($add_words);
                    // Add
                    $videoWords[$i] = $newWords[$i][word];

                    // Add word to addedWords array
                    $addedWords[$i][word] = $newWords[$i][word];

                    $uniqueWordCount++;

                } else
                {
                    // Don't add word to words file but add it to video words
                    $videoWords[$i] = $newWords[$i][word];

                    $insertWord = $newWords[$i][word];
                    if (!wordHasBeenAddedAlready($addedWords, $insertWord))
                    {
                        $uniqueWordCount++;
                    }
                    // Add word to addedWords array as the word exists in db already so dont want to add again
                    // if it comes up in the remaining words to add
                    $addedWords[$i][word] = $newWords[$i][word];
                }

                // Check whether word has been added already
                // 	if (!wordHasBeenAddedAlready($addedWords, $insertWord)){

            }

            // Generate video filename
            $videoName = getvideofilename($newWordCount, $videoWords);
            // Set attribution and attribution url
            $attribution = $_POST['attribution'];
            $attributionrl = $_POST['attributionurl'];

            // Add video to videos file
            addvideofile($videoName, $uniqueWordCount, $scenario, $attribution, $attributionrl, $submitted, $contributorID, $connection);
            // Get the video ID
            $videoId = getvideoid($videoName, $connection);

            // Now map words and write video words
            for ($i = 0; $i <= $newWordCount - 1; $i++)
            {
                $checkWord = $newWords[$i][word];
                // Escape check word
                $checkWord = mysqli_real_escape_string($connection, $checkWord);
                $checkNotes = $newWords[$i][notes];
                $wordSequence = $newWords[$i][sequence];
                $wordTime = $newWords[$i][wordtime];
                // Get wordid of new word. Ensure the notes are the same too in case this is a synonym
                // of an existing word
                // $query = "select wordid from words where word = '$checkWord' and notes = '$checkNotes'";
                // Don't check notes in showallvids as they may well be different
                $query = "select wordid from words where word = '$checkWord'";
                $result = mysqli_query($connection, $query) or die($query);
                $resultarray = mysqli_fetch_assoc($result);
                $wordId = $resultarray["wordid"];

                // Add video words
                $addVideoWords = "insert into videowords (VIDEOS_VIDEOID, SEQUENCE, WORDID, TIME)values ('$videoId','$wordSequence','$wordId', '$wordTime')";
                $result = mysqli_query($connection, $addVideoWords);

                if (!$result)
                {
                    die("ERROR!<br />Query is: $addVideoWords<br />Error is: " . mysqli_error($connection));
                }

            }


            // Before creating the new video delete the edit video, i.e. the original of this video
            deletevideo($deleteEditVideo, $connection);
            // Now update the newly created video which was based on edit details of the original video with
            // the videoid of the original video. Need to do this as the filenames have already been allocated
            updatevideoid($deleteEditVideo, $videoId, $connection);
            // Now update video words with old videoid too
            updatevideowordids($deleteEditVideo, $videoId, $connection);

            // Add videoid to session
            $_SESSION['videoID'] = $videoId;
            // Now draw validate page
            include ("confirm.inc");


            break;

        case "delete":
            // User has hit one of the delete buttons
            // Check there is a value to delete
            // The value is from the hidden element of the delete button setup thus in the html
            // 	<input type="hidden" name="VideoToDelete" value="$row[0]">
            if (isset($_POST['VideoToDelete']))
                // Set the value of the video to delete from $_POST

                $videoToDelete = $_POST['VideoToDelete'];
            // Open database
            opendb();
            // Delete video
            if (deletevideo($videoToDelete, $connection))
            {
                // Display message video was deleted
                //	print("<script language = 'javascript'>alert('Video deleted');</script>");
                // exit();
                header("Location: ShowAllVids.php");
                // print("<script language = 'javascript'>alert('Video deleted');</script>");
                // exit();
            }
            break;

        case "edit":
            // User has hit one of the edit buttons
            // Check there is a value to edit
            // The value is from the hidden element of the edit button setup thus in the html
            // 	<input type="hidden" name="VideoToEdit" value="$row[0]">
            if (isset($_POST['VideoToEdit']))
                // Set the value of the video to edit from $_POST

                $videoToEdit = $_POST['VideoToEdit'];
            	$videoToEdit = trim($videoToEdit);
            
            // Open database
            opendb();

            // Get scenario and attributions

            $query = "select scenario, attribution, attributionurl from videos where videoid = '$videoToEdit'";
            $result = mysqli_query($connection, $query) or die($query);

            if (!$result)
            {
                die("ERROR!<br />Query is: $query<br />Error is: " . mysqli_error());
            }

            $resultarray = mysqli_fetch_assoc($result);
            $scenario = $resultarray["scenario"];
            $attribution = $resultarray["attribution"];
            $attributionurl = $resultarray["attributionurl"];

            // Get wordids

            $videoWordIds = "select wordid from videowords where videos_videoid = '$videoToEdit'";

            $result = mysqli_query($connection, $videoWordIds) or die($videoWordIds);


            if (!$result)
            {
                die("ERROR!<br />Query is: $videoWordIds<br />Error is: " . mysqli_error());
            }

            $videoWordIds = array();


            while ($row = mysqli_fetch_assoc($result))
            {
                $videoWordIds[] = $row['wordid'];

            }


            $videoWords = array();

            foreach ($videoWordIds as $Wordid)
            {
                $query = "select word from words where wordid = '$Wordid'";


                $result = mysqli_query($connection, $query) or die($query);

                if (!$result)
                {
                    die("ERROR!<br />Query is: $query<br />Error is: " . mysqli_error());
                }

                // Set number to how many records the query retrieved
                // $number = mysqli_num_rows($result);
                // Just get first row from array - only expecting one row anyway
                $resultarray = mysqli_fetch_assoc($result);
                $videoWords[] = $resultarray["word"];

            }


            prepareValidateData($videoWords, $videoToEdit, $connection);

            // Add new words array and new word count to session
            $_SESSION['newWords'] = $newWords;
            $_SESSION['WordCount'] = $WordCount;
            $_SESSION['DeleteEditVideo'] = $videoToEdit;

            // Now draw validate page
            include ("validatevid.inc");

            break;

        case "Add new vid?":
            header("Location: addvid.php");
            break;
        

        default:
        	// This is the first time the addvid.php has been called so display default page
        	// Open database
        	opendb();
        	$query = "select * from videos order by videoid DESC";
        	$result = mysqli_query($connection, $query) or die($query);
        	$NoVideos = mysqli_num_rows($result);
        	$Video=array();
        	$Item=0;
        	    while ($row = mysqli_fetch_row($result)) {
				
				// Word timings added
        		$Colour ="Red"; // set default colour
        		if (TimingsBeenAdded($row[0],$connection))
        		{
        			$Colour="White";
        		}
        		$row[9]=$Colour;
        		
        		
        		// Thumb path
        		$thumbPath = moviePlaceHolder($row[1], "../");
        		$row[10]=$thumbPath;
        		$Videos[]=$row;
        	}
        	$_SESSION['Videos'] = $Videos;
        	$_SESSION['NoVideos'] = $NoVideos; 
        	//
        	include ("ShowAllVids.inc");
        	break; // This is the first time the addvid.php has been called so display default page
           
    }

   
?>