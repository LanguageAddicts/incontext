<?php
//Draw the word list for the competition


require("db_functions.php");
require("page_elements.php");
require("common/trace_functions.php");

traceStart();											//start the trace file
error_reporting(E_ALL);
openPage();
drawHead("Language Addicts English - Word List");
drawBody();
closePage();

function drawBody()
{
//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo <<<EOF

EOF;
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();

//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
drawWordList();


echo "</div><!--end of content-->";
}


function drawWordList()
{
//build block header
echo<<<EOF
<div class="blankwideblock"  id="wordlist">
<img class="icon" src="images/guidelines.png" alt="guidelines">

EOF;
//open db

global $connection; //set up
opendb(); //open the database (db_functions.php)
//get word list

$wordlist = getWantedWordsByGroup(true);  								//get list of wanted words
mysqli_close($connection);									//close the database connection
	while($word = mysqli_fetch_array($wordlist))
	{
		$wantedWords[] = $word;									//store the sql result in working array
	}
$total = count($wantedWords);
echo "<p>The wordlist is dynamic and will change as we receive enough entries for existing words. We may also add new words during the contest. Currently there are approximately $total words in the list.</p>";
//output 
echo"<br>";
echo "<table class=\"wordlisttable\">";
$group = null;
$style=null;
$groupNumber = 0;
$threshhold = 3;  													// this is the threshhold number of existing vids to exclude new entries
foreach($wantedWords as $wantedWord)								//add contributor details to list
		{
		if ($wantedWord["videoCount"] <= $threshhold)
			{
				if ($group != $wantedWord["GROUP"])
				{	
					++$groupNumber;
					$style=setBackgroundStyle($groupNumber);
					echo "<tr class=\"$style\">";
					echo "<td class=\"WantedWordGroup\">{$wantedWord["GROUP"]} </td>";
					$group = $wantedWord["GROUP"];
				}
				else
				{
					echo "<tr class=\"$style\">";
					echo "<td/>";
				}
				echo "<td class=\"WantedWordPhrase\">{$wantedWord["WORD"]}</td>"; 
				//echo "<td>{$wantedWord["videoCount"]}</td>";
				echo "<td>  </td>"; //empty buffer column
				echo "<td class=\"WantedWordComments\" style=\"font-style:italic;font-size:small\">{$wantedWord["COMMENTS"]}</td>";
				echo "</tr>";
				}
			}	
echo "</table>";
//build block trailer
echo<<<EOF
</div>
EOF;
}
function setBackgroundStyle($groupNumber)
{
if ($groupNumber % 2 == 0)
//even number
{$style="tableRowStyle1";}
else
//odd number
{$style="tableRowStyle2";}
return $style;
}






?>