<?php

function printHead($includeScripts = false, $vidrow = null)
{
$thumb = null;
if ($vidrow != null)
{
$thumb = $vidrow["VIDEOID"].".jpg";
}
echo"<head>
  <meta http-equiv=\"content-type\" content=\"text/html; charset=windows-1250\">
  <meta name=\"generator\" content=\"PSPad editor, www.pspad.com\">
  <meta property=\"og:title\" content=\"The monkey's in the Tree\" />
<meta property=\"og:description\" content=\"learn english soooooo easy\" />
<meta property=\"og:image\" content=\"http://www.english.languageaddicts.com/thumbs/brokenlink.jpg\" />
  <title>The Monkey is in the Tree</title>
  <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/layout.css\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/player.css\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/tabs.css\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/sliderkit-core.css\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/sliderskin.css\" media=\"screen, projection\" />
  
  <!--extra bit needed for carousel compatibility-->
  
  <!-- Compatibility fix -->
<!--[if IE 6]>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/sliderkit-demos-ie6.css\" />
<![endif]-->

<!--[if IE 7]>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/sliderkit-demos-ie7.css\" />
<![endif]-->

<!--[if IE 8]>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles/sliderkit-demos-ie8.css\" />
<![endif]-->

  <!--end of carousel bit -->
  
  <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"favicon.ico\">";
if($includeScripts == true)
{
echo"<!-- Embed the necesary scripts for the player-->
      <script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-latest.min.js\"></script>
      <script src=\"flowplayer/flowplayer-3.2.6.min.js\"></script>
      <script type=\"text/javascript\" src=\"scripts/EDPlayerControl.js\"></script>
      <script type=\"text/javascript\" src=\"scripts/tabs.js\"></script>
      <script type=\"text/javascript\" src=\"scripts/jquery.sliderkit.1.9.2.pack.js\"></script>
      <script type=\"text/javascript\">
       var addthis_config = {\"data_track_addressbar\":true, \"ui_offset_top\":0, \"ui_open_windows\":true, \"ui_offset_left\":-100};
       </script>";
      //<script type=\"text/javascript\" src=\"scripts/jquery.carouFredSel-5.5.0.js\"></script>";
      activate_gallery();


} 

echo"</head>";
}

function activate_gallery()
{
echo"\n 
<!-- Slider Kit execution -->
<script type=\"text/javascript\">
    $(window).load(function(){
        
        $(\"#carousel_demo1\").sliderkit({
 					auto:false,
 					shownavitems:5,
					start:2

				});
;

    });
</script>";
}

function activate_gallery_caroufredsel()
{
echo"\n <script type=\"text/javascript\" language=\"javascript\">
			/*	CarouFredSel: an infinite, circular jQuery carousel.*/
$(function() {

$(\"#gallery\").carouFredSel({
	padding: 5,
	items: {
		visible: 5,
		start: \"random\"
	},
	scroll: {
		items: 1,
		fx: \"scroll\"
	},
	auto: false,
	prev: {
		button: \"#gallery_prev\",
		key: \"left\"
	},
	next: {
		button: \"#gallery_next\",
		key: \"right\"
	}
}) });


</script>";
}



function printDocType()
{
echo"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";
}



?>