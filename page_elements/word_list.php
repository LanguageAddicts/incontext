<?php
/* get a full list of words from the database and display in a nice alphabetical list */


function displayWordList()
{
echo"<div id=\"wordlist\" class=\"curved\">";
//print out the alphabet list down the left
printTabs();
//get full wordlist
$wordlist = getFullWordList();

while ($Videorow = mysqli_fetch_array($wordlist))
{
$wordid = $Videorow["WORDID"];
//is this a new letter of the alphabet?
$letter = strtolower(substr($Videorow["WORD"], 0,1));
$word = $Videorow["WORD"];
manageSections($letter, $lastletter);
//word frequency
$freq =  getWordfrequency($wordid);
$printFreq = "(". $freq.")";
$fontSizeClass = getFontSize($freq);
//add word to list
echo"<a class=\"$fontSizeClass\" href=\"index.php?word=$wordid&litteral=$word\">$word $printFreq</a></br>";
}
  //finish off the final set
  echo "</div>";  
  //finish off the word list section
  echo "</div>"; 
}

function manageSections($letter, &$lastletter)
{
  if ($letter != $lastletter)
  {
    if ($lastletter != null)
    {//finish off the previous set
    echo "</div>";
    fillTheGaps($letter, $lastletter);    
    }
    //start the next set
    echo "<div>";  
    $lastletter = $letter;
  }
}

function fillTheGaps($letter, $lastletter)
{
//see if the next letter is the right one alphabetically
$alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
$gap = (strpos($alphabet, strtoupper($letter)) - strpos($alphabet, strtoupper($lastletter)));
while(--$gap > 0)
{
//there's a gap, fill it
echo "<div></div>"; 
}
}    


function printTabs()
{
//print the tabs
echo "<div id=\"vtab\">
    <ul>
        <li class=\"A\"></li>
        <li class=\"B\"></li>
        <li class=\"C\"></li>
        <li class=\"D\"></li>
        <li class=\"E\"></li>
        <li class=\"F\"></li>
        <li class=\"G\"></li>
        <li class=\"H\"></li>
        <li class=\"I\"></li>
        <li class=\"J\"></li>
        <li class=\"K\"></li>
        <li class=\"L\"></li>
        <li class=\"M\"></li>
        <li class=\"N\"></li>
        <li class=\"O\"></li>
        <li class=\"P\"></li>
        <li class=\"Q\"></li>
        <li class=\"R\"></li>
        <li class=\"S\"></li>
        <li class=\"T\"></li>
        <li class=\"U\"></li>
        <li class=\"V\"></li>
        <li class=\"W\"></li>
        <li class=\"X\"></li>
        <li class=\"Y\"></li>
        <li class=\"Z\"></li>
    </ul>";

}

function displayWordList_trad()
{
$lastletter = " ";
/* read all words from database*/
echo"<div  id=\"wordlist\">";

$wordlist = getFullWordList();
while ($Videorow = mysqli_fetch_array($wordlist))
{
  $wordid = $Videorow["WORDID"];
  //is this a new letter of the alphabet?
  $letter = strtolower(substr($Videorow["WORD"], 0,1));
  $word = "  ".$Videorow["WORD"];
  if ($letter != $lastletter)
  {
    //this is a new letter, line break
    //echo"</br>"; 
    //show nice icon
    echo"<img src=\"images/$letter.png\" alt=\"A\" />";
    $lastletter = $letter;
  }
//get the word frequency
$freq =  getWordfrequency($wordid);
$printFreq = "(". $freq.")";
//$printFreq = " ";
$fontSizeClass = getFontSize($freq);
echo"<a class=\"$fontSizeClass\" href=\"index.php?word=$wordid\">$word $printFreq</a>";
}
echo"</div>";
}


function getFontSize($frequency)
{
// determine small/medium/large font
$fontsize = "small_text"; 
switch(true)
{
case ($frequency < 3):
  //small
  $fontsize = "small_text";
  break;
case ($frequency > 5):
  //large
  $fontsize = "large_text";
  break;
default:
  //medium
  $fontsize = "medium_text";
  break;
}
return $fontsize;
}

?>