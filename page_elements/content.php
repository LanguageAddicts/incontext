<?php
require("player.php");
require("word_list.php");
//prevent php warning about session variables with the same name as normal variables
ini_set('session.bug_compat_warn', 0);
ini_set('session.bug_compat_42', 0);


function printContent($chosenVideo, $wordid)
{
echo"<!--Content -->
<div id=\"content\">" ;
displayVideoDetails($chosenVideo, $wordid);
social_links();
addPlaylist_gallery($wordid);

echo"<!--end of content section --></div>";

displayWordList();

 
}




function displayVideoDetails($vidrow, $wordid)
{

trace("display video details", $vidrow);
$videoid =  $vidrow["VIDEOID"];
trace("fileid: $videoid");
$name = movieName($vidrow);
//echo "</br>vidname:$name. vidID:.$videoid</br>";
echo"<div id=\"player_control\" class=\"curved\">";
createWordList($vidrow,$list);
buildVideoPanel($vidrow, $list);
//add word buttons
addWordPanels($vidrow); 
//attribution line
addAttribution($vidrow);
echo"<!--end of player control --></div>";

}

function addAttribution($vidrow)
{
$subsequent = false;
$attributionarray = $vidrow["ATTRIBUTIONARRAY"];
if ($attributionarray != null)
{
echo"<div  id=\"attribution_panel\">";
foreach ($attributionarray as $attrib) 
{
 //no values means it's home made
if (empty($attrib["ATTRIBUTIONURL"])== TRUE && empty($attrib["ATTRIBUTION"]) == TRUE)
{
$attribution = " ";
$URL = "http://www.themonkeysinthetree.com";
}
elseif (empty($attrib["ATTRIBUTIONURL"])== TRUE && empty($attrib["ATTRIBUTION"]) == FALSE)
{
$attribution = $attrib["ATTRIBUTION"];
$URL = " ";
}
elseif (empty($attrib["ATTRIBUTIONURL"])== FALSE && empty($attrib["ATTRIBUTION"]) == TRUE)
{
$attribution = " ";
$URL = " ";
}
elseif (empty($attrib["ATTRIBUTIONURL"])== FALSE && empty($attrib["ATTRIBUTION"]) == FALSE)
{
$attribution = $attrib["ATTRIBUTION"];
$URL = $attrib["ATTRIBUTIONURL"];
}

if ($attribution != " ")

{
if ($subsequent == false)
   {$links = "<a href=\"". $URL ."\">" . $attribution . "</a>";
    $subsequent = true;}
else
   {$links .= " and " . "<a href=\""  .$URL  ."\">" .$attribution ."</a>";}

}
}
$standard = "video based fully or in part on a work created by ";
$finaltext = $standard . $links;
echo $finaltext;
echo"</div>";
}
}


function addWordPanels($vidrow)
{
$enabled = " ";
$button_image=" ";
$save_image = " ";
$save_name=" ";
$speech_image="images/speak_word.png";
$i=0;

//start speech button div
echo"<div  id=\"button_panel\">";

$VidWordsArray = $vidrow["VIDEOWORDSARRAY"];
foreach ($VidWordsArray as $wordrow) 
{
setActionButtonValues($wordrow,$vidrow, $enabled,$button_image,$save_image,$save_name);
//$imgID="act". $wordrow["POSITION"];
//$pos= $wordrow["POSITION"];
//$panID ="pan". $wordrow["POSITION"]; 
$wordid = $wordrow["WORDID"]; 
$word = $wordrow["WORDNAME"];
$PositionInMiliseconds = $wordrow["POSITION"] * 1000; 
$bubbleID = "word_bubble_".$PositionInMiliseconds ;


//print speech bubble for the word
echo"<div id=\"$bubbleID\" class=\"word_bubble\">";
if($enabled != "DISABLED")
{
echo"<a class=\"word_link_text\" href=\"index.php?word=$wordid&litteral=$word\">$word </a>";
}
else
{
//no onward link from this word so no href
echo"<a class=\"word_link_text\" >$word </a>";
}
echo"</div>";
}

echo"<!-- end of button panel div --></div>";
//whack in the word panel div
//echo"<div id=\"word_panel\"></div>";
}

function createWordList($vidrow,&$list)
{
$list = "[";
$found = 0;
$VidWordsArray = $vidrow["VIDEOWORDSARRAY"];
foreach ($VidWordsArray as $wordrow) 
{
if ($found > 0) {$list .=", ";}
$list .= $wordrow["POSITION"] * 1000;
++$found; 
}
$list .="]";
}

function setActionButtonValues(&$wordrow,&$vidrow, &$enabled,&$button_image, &$save_image, &$save_name)
{
$target_button  =" ";
$button =" ";

//Action button values
if($wordrow["VIDEOCOUNT"] > 1)
{
$enabled = " ";
$target_button = "images/target_select.png";
$button = "images/select.png"; 
}
else
{
$enabled = "DISABLED";
$target_button = "images/target_select_disabled.png";
$button = "images/select_disabled.png"; 
}
$vidwordid = $wordrow["WORDID"];
if ($vidwordid == $vidrow["TARGET"])
{$button_image=$target_button;}  
  else
{$button_image=$button;}

//Save button values
if( $wordrow["WORDKNOWN"] ==true)
{
$save_image="\"images/study.png\"";
$save_name = "\"study\"";
}
else
{
$save_image="\"images/save-icon.png\"";
$save_name = "\"Save\"";
}

}


function addPlaylist_gallery($wordid)
{
trace("addplaylist ".$wordid);
//get list of last x vids shown and display details in a row
/*echo"\n<div id=\"gallery_wrapper\">
<div class=\"image_carousel\">
<div id=\"gallery\"><!--Start of gallery-->\n";*/

echo"				<div id=\"carousel_demo1\" class=\"sliderkit carousel-demo1\">
            <div class=\"sliderkit-nav\">
            <div class=\"sliderkit-nav-clip\">
            <ul>";

   $playlist = getVideoFiledetailsByWord($wordid);
   while(($row = mysqli_fetch_array($playlist)))// && ($i++ < 6))
   {
       $ID = $row['VIDEOID'] ;
       $Videos_Record = getVideoFiledetails($ID);
      $Thumb = moviePlaceHolder($ID);
      $word = $row['WORD'];
      //echo $row['VIDEONAME'];
      $Name = str_replace("_", " ", $row['VIDEONAME']);
      //echo"<img src=\"$Thumb\" width=\"95\" height=\"95\" alt=\"$Thumb\"/>\n";
      echo"<li><a href=\"index.php?vid=$ID&litteral=$word\" title=\"$Name\"><img src=\"$Thumb\" width=\"75\" height=\"75\" alt=\"pic\" /></a></li>";
      /*echo"<div>
      <a href=\"index.php?vid=$ID\">play</a>
      </div>"; */

      //echo"<a href=\"index.php?vid=$ID\" ><img src=\"$Thumb\" width=\"75\" height=\"75\" alt=\"$Thumb\" /></a>";
   } 

echo"</ul>
     </div>
     <div class=\"sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev\"><a href=\"#\" title=\"Scroll to the left\"><span>Previous</span></a></div>
     <div class=\"sliderkit-btn sliderkit-nav-btn sliderkit-nav-next\"><a href=\"#\" title=\"Scroll to the right\"><span>Next</span></a></div>
     </div>
     </div>
     <!-- // end of carousel-demo1 -->";


}



function addPlaylist($wordid)
{
trace("addplaylist ".$wordid);
//get list of last x vids shown and display details in a row
echo"<div id=\"video_gallery\" class=\"curved\"><!--Start of gallery-->";
echo"<table class=\"video_gallery\">";
echo"<tr>";
   //print "playlist wordid: ".$wordid;
   $playlist = getVideoFiledetailsByWord($wordid);
   while(($row = mysqli_fetch_array($playlist)) && ($i++ < 6))
   {
       $ID = $row['VIDEOID'] ;
       $Videos_Record = getVideoFiledetails($ID);
      $Thumb = moviePlaceHolder($ID);
      echo "<td><a href=\"index.php?vid=$ID\" >";
      echo "<img src=\"$Thumb\" alt=\"$Thumb\" />";
      echo "</a></td>";
   } 
echo"</tr>";
echo "</table>";
echo"<!--end of gallery --></div>";
}

function social_links()
{
echo"<div id=\"social\">
<!-- AddThis Button BEGIN -->
<div class=\"addthis_toolbox addthis_default_style addthis_32x32_style\">
<a class=\"addthis_button_preferred_1\" ></a>
<a class=\"addthis_button_preferred_2\" ></a>
<a class=\"addthis_button_preferred_3\" ></a>
<a class=\"addthis_button_preferred_4\" ></a>
<a class=\"addthis_button_compact\" ></a>
</div>
<script type=\"text/javascript\" src=\"http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4fa6c355778a6ef9\"></script>
<!-- AddThis Button END -->
</div>";
}

?>