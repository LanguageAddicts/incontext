<?php
<body>
  
  <!--outer wrapper -->
  <div id="container">
  
  
  
  <?php require("header.html"); ?>
  
  
  <!--Wrapper for content, sidebar and footer -->
  <div id="content-container">
  
  <?php require("content.html"); ?>
  
  <?php require("sidebar.html"); ?>
  
  <?php require("footer.html"); ?>
 
  
  <!--end of content-container section -->
  </div>
  <!--end of container section -->
  </div>



  </body>
?>
