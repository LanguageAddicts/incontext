<?php
session_start();
 
require("db_functions.php");

$maxLines = 10; 
$videos = array();
$refresh = $_GET['direct'];									// was the 'more' button pressed or is this a non-refreshing request $refresh is TRUE if 'more' pressed	
$pageNo = $_GET['page'];			
				
global $connection; 										//set up
opendb(); 													//open the database (db_functions.php)
$videolist = getMostRecentVideos(); 						//get list of recent videos
mysqli_close($connection);									//close the database connection
while($video = mysqli_fetch_array($videolist))
{
	$videos[] = $video;
}
$totalentries = count($videos);
$numberOfPages = ceil($totalentries / $maxLines);
if (!isset($_SESSION['galleryPage']))
{
	$_SESSION['galleryPage'] = 1;
}
else
{
	if ($pageNo == null)
	{
	if ($refresh == "NEXT")									
	{
	if ($_SESSION['galleryPage'] == $numberOfPages)
		{$_SESSION['galleryPage'] = 1;}												//loop back round to 1
		else
		{$_SESSION['galleryPage']++;}												//increment the page number
	}				
	if ($refresh == "PREV")
		{
	if ($_SESSION['galleryPage'] == 1)
		{$_SESSION['galleryPage'] = $numberOfPages;}												//loop back round to last page
		else
		{$_SESSION['galleryPage']--;}												//decrement the page number
	}	
	}
	else
	{
	$_SESSION['galleryPage'] = $pageNo;
	}
}
$pageNo = $_SESSION['galleryPage'];
$startPos = ($pageNo - 1) * $maxLines;							//work out start element
if ($startPos > $totalentries) {$startPos = 0;}					//list has looped around, restart at 0
$videos1 = array_slice($videos,$startPos,$maxLines);			//slice out the portion of the array we want

printHTML($videos1,$numberOfPages);

function printHTML($videos,$numberOfPages)
{
													
		foreach($videos as $video)								//add video details to list
		{
		$link = "?vid=".$video['VIDEOID'];
		$poster =  getPosterDirectory(false,$video['VIDEOID']);
		$auth = $video['name']. " ". $video['surname'][0]. " - ". $video['SUBMITTED'];
		echo "<div class=\"mainGallery\">";
		echo "<a href=\"{$link}\"><img class=\"vid_image\" src=\"{$poster}\"></a>";
		echo "<p>{$auth}</p>";
		echo "</div>";
		}	
		/* add in navigation
		echo "<div class=\"gallerymoreMain\">";
		echo"<input type=\"image\" src=\"images/left.png\" alt=\"Submit\" width=\"32\" height=\"16\" id=\"galleryLessMain\"> <a href=\"#1\">1</a> <a href=\"#2\">2</a> <input type=\"image\" src=\"images/right.png\" alt=\"Submit\" width=\"32\" height=\"16\" id=\"gallerymoreMain\">";
		echo "</div>";
		*/
}

?>