<?php
//Draw the word list for the competition


require("db_functions.php");
require("page_elements.php");
require("common/trace_functions.php");

//open db
global $connection; //set up
opendb(); //open the database (db_functions.php)

traceStart();											//start the trace file
error_reporting(E_ALL);
openPage();
drawHead("Language Addicts English - Word List entry", "js/submissions.js" );
drawBody();
closePage();

function drawBody()
{



//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo <<<EOF

EOF;
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();

//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
drawWordList();
echo "</div><!--end of content-->";
}


function drawWordList()
{
echo"<div id=\"dialog-confirm\"></div>";
//build block header
echo"<div class=\"blankwideblock\"  id=\"wordlist\">";
//ajax will populate this area
echo"</div>";
}







?>