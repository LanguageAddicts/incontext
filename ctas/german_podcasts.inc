<h1>German Podcasts</span></h1> 

<p>This page contains the written form of the words from the monthly podcast. You are given the word in English, German and phonetic English
pronunciation guide.</p>

<form id="here"name="jump">
<p align="left">
<select name="menu">
<option value="#">select an episode</option>
<option value="#7">March 2010 - Household chores</option>
<option value="#6">Febuary 2010 - Relationships</option>
<option value="#5">January 2010 - Appearances</option>
<option value="#4">December 2009 - In the kitchen</option>
<option value="#3">November 2009 - Characteristics</option>
<option value="#2">October 2009 - My House</option>
<option value="#1">September 2009 - Greetings and pleasantries</option>


</select>
<input type="button" onClick="location=document.jump.menu.options[document.jump.menu.selectedIndex].value;" value="Go to episode">
</p>
</form>

<p><a name="Subscription details" href="#" onclick="return false;"></a></p>
<div id="subscribe" style="width: 550px;">
<h2>Subscribe to this podcast</h2>
<div id="feed" style="width: 550px;">
<h3>Feed URL</h3>
<input class="inputRssUrl" type="text" size="50" value="http://www.languageaddicts.com/podcasts/german/german.rss"/>
</div>

<div id="oneClicks" style="width: 465px; padding-top: 15px;">
<h3>One-click subscriptions</h3>
<p><a class="addClearance" href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewPodcast?id=329204245"
onclick="javascript:urchinTracker ('/podcast/German/itunes_subscription')">
<img src="http://www.languageaddicts.com/images/itunes.gif" alt="Subscribe with iTunes"/></a></p>

<p><a class="addClearance" href="http://add.my.yahoo.com/rss?url=http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/yahoo_subscription')">
<img src="http://www.languageaddicts.com/images/my_yahoo.gif" alt="Subscribe with My Yahoo!"/></a></p>

<p><a class="addClearance" href="http://fusion.google.com/add?feedurl=http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/google_subscription')">
<img src="http://www.languageaddicts.com/images/Google_Reader.gif" alt="Subscribe with Google Reader"/></a></p>

<p><a class="addClearance" href="http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/rss_subscription')">
<img src="http://www.languageaddicts.com/images/rss.gif" alt="Subscribe through RSS" style="display: inline; vertical-align: middle;"/></a>
<a class="addClearance" href="http://www.languageaddicts.com/podcasts/german/german.rss"
onclick="javascript:urchinTracker ('/podcast/German/rss_subscription')">
RSS</a></p>
</div>
</div>

<p>Listen now</p>

<p>
<a href="#" onclick="window.open('http://www.podtrac.com/PodtracPlayer/podtracplayer.aspx?podcast=http://www.languageaddicts.com/podcasts/german/german.rss', 'linkname', 'height=235, width=450, scrollbars=no'); return false;">
<img src="http://www.podtrac.com/PodtracPlayer/playerbutton1.jpg" alt="Podtrac Player"/>
</a>
</p>
<br/>
<h2>Episode Listing</h2>
<table class="table_width" width="550" cellpadding="0" cellspacing="2" style="border: 1;">

<!-- start of 7 -->
<tr id="7" class="PodcastTableRow"> 
<td colspan="3"><h2>March 2010 - Household chores</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow">
<td>to dust</td><td> abstaugen</td><td> AP-shtaw-gen
</td></tr>
<tr class="PodcastTableRow">
<td>the housework</td><td> die Hausarbeit</td><td> DEE HAWs-ah-bite
</td></tr>
<tr class="PodcastTableRow">
<td>to wash up</td><td> abwaschen</td><td>AP-vash-en
</td></tr>
<tr class="PodcastTableRow">
<td>the clothes line</td><td>die W&auml;scheleine</td><td>dee VESHE-lie-neh
</td></tr>
<tr class="PodcastTableRow">
<td>to polish</td><td>polieren</td><td>POH-leer-en
</td></tr>
<tr class="PodcastTableRow">
<td>to decorate</td>  <td>dekorieren</td>  <td>deh-koh-REER-en
</td></tr>

<tr class="PodcastTableRow">
<td>to mop up</td>  <td> aufwischen</td>  <td>AWF-vish-un
</td></tr>

<tr class="PodcastTableRow">
<td>to throw away</td>  <td> wegwerfen</td>  <td>VEGG-verf-en
</td></tr>

<tr class="PodcastTableRow">
<td>to recycle</td>  <td> wiederverwenden</td>  <td> VEE-der-fer-vend-en
</td></tr>

<tr class="PodcastTableRow">
<td>to wipe</td>  <td>abwischen</td>  <td>AP-vish-en</td></tr> 
<tr><td>&nbsp;</td></tr>
<!-- end of 7 -->

<!-- start of 6 -->
<tr id="6" class="PodcastTableRow"> 
<td colspan="3"><h2>Febuary 2010 - Relationships</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow">
<td>the argument</td>  <td> die Auseinandersetzung</td>  <td> DEE OWSS-eyn-ander-set-tzung
</td></tr>
<tr class="PodcastTableRow">
<td>to compromise</td>  <td> kompromittieren</td>  <td> kom-prom-it-EER-en
</td></tr>
<tr class="PodcastTableRow">
<td>the discussion</td>  <td> die Diskussion</td>  <td> DEE dis-kuss-see-OHN
</td></tr>
<tr class="PodcastTableRow">
<td>to gossip</td>  <td> plaudern</td>  <td> PLOW-dern
</td></tr>
<tr class="PodcastTableRow">
<td>to hate</td>  <td> hassen</td>  <td> HAS-sen
</td></tr>
<tr class="PodcastTableRow">
<td>to kiss</td>  <td> k&uuml;ssen</td>  <td> koo-SEN
</td></tr>
<tr class="PodcastTableRow">
<td>to shout</td>  <td> schreien</td>  <td> SHRY-en
</td></tr>
<tr class="PodcastTableRow">
<td>to whisper</td>  <td> fl&uuml;stern</td>  <td> FLUH-stern
</td></tr>
<tr class="PodcastTableRow">
<td>to cuddle</td>  <td> kuscheln</td>  <td> KUH-sheln
</td></tr>
<tr class="PodcastTableRow">
<td>to feel</td>  <td> sich f&uuml;hlen</td>  <td> SICH FOOL-en</td></tr> 
<tr><td>&nbsp;</td></tr>
<!-- end of 6 -->
<!-- start of 5 -->
<tr id="5" class="PodcastTableRow"> 
<td colspan="3"><h2>January 2010 - Appearances</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow">
<td>fat</td>  <td> dick</td>  <td> DIK
</td></tr>
<tr class="PodcastTableRow">
<td>thin</td>  <td> d&uuml;nn</td>  <td> DOON
</td></tr>
<tr class="PodcastTableRow">
<td>the figure</td>  <td> die Figur</td>  <td> dee fig-OOR
</td></tr>
<tr class="PodcastTableRow">
<td>handsome</td>  <td> gut aussehend</td>  <td> goot AUS-say-hend
</td></tr>
<tr class="PodcastTableRow">
<td>pretty</td>  <td> h&uuml;bsch</td>  <td> HOOBsh
</td></tr>
<tr class="PodcastTableRow">
<td>strong</td>  <td> stark</td>  <td> shTARk
</td></tr>
<tr class="PodcastTableRow">
<td>fair</td>  <td> hell</td>  <td> HELL
</td></tr>
<tr class="PodcastTableRow">
<td>tanned</td>  <td> gebr&auml;unt</td>  <td> geBROYnt
</td></tr>
<tr class="PodcastTableRow">
<td>bald</td>  <td> kahl</td>  <td> KAAL
</td></tr>
<tr class="PodcastTableRow">
<td>glasses</td>  <td> die Brille</td>  <td> dee BRIL-LE</td></tr> 
<tr><td>&nbsp;</td></tr>
<!-- end of 5 -->

<!-- start of 4 -->
<tr id="4" class="PodcastTableRow"> 
<td colspan="3"><h2>December 2009 - In the kitchen</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 


<tr class="PodcastTableRow">
<td>to bake </td>  <td>backen	</td>  <td>BAK-en</td></tr>
<tr class="PodcastTableRow">
<td>the kettle</td>  <td> Kessel (der)	</td>  <td>dehr KEH-sel</td></tr>
<tr class="PodcastTableRow">
<td>the chopping board</td>  <td> Brett (das)</td>  <td>	das BRET</td></tr>
<tr class="PodcastTableRow">
<td>the coffee pot</td>  <td> Kaffeekanne (die)</td>  <td>	dee KAH-fee-kan-eh</td></tr>
<tr class="PodcastTableRow">
<td>the draining board </td>  <td>Abtropfbrett (das)	</td>  <td>das AB-tropf-brett</td></tr>
<tr class="PodcastTableRow">
<td>the can opener </td>  <td>Dosen&ouml;ffner (der)	</td>  <td>DOE-sen-oh-fner</td></tr>
<tr class="PodcastTableRow">
<td>the crockery </td>  <td>Geschirr  (das)	</td>  <td>das ge-SHEER</td></tr>
<tr class="PodcastTableRow">
<td>to peel </td>  <td> sch&auml;len</td>  <td>	SHEH-len</td></tr>
<tr class="PodcastTableRow">
<td>the recipie</td>  <td> Rezept (das)</td>  <td>	das re-TZEPT</td></tr>
<tr class="PodcastTableRow">
<td>the spoon</td>  <td> L&ouml;ffel (der)</td>  <td>	dehr LOH-fell</td></tr>
<tr><td>&nbsp;</td></tr>
<!-- end of 4 -->

<!-- start of 3 -->
<tr id="3" class="PodcastTableRow"> 
<td colspan="3"><h2>November 2009 - Characteristics</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow">
<td>open-minded</td>  <td> aufgeschlossen</td>  <td>	AUFgeshlosun</td></tr>
<tr class="PodcastTableRow">
<td>weird </td>  <td>seltsam	</td>  <td>SELTsam</td></tr>
<tr class="PodcastTableRow">
<td>crazy</td>  <td> verr&uuml;ckt	</td>  <td>ferROOKed</td></tr>
<tr class="PodcastTableRow">
<td>cheerful</td>  <td> erfreulich</td>  <td>	errFROYlikh</td></tr>
<tr class="PodcastTableRow">
<td>grumpy</td>  <td> brummig</td>  <td>	BRUMmig</td></tr>
<tr class="PodcastTableRow">
<td>silly </td>  <td>doof	</td>  <td>DOWF</td></tr>
<tr class="PodcastTableRow">
<td>witty </td>  <td>witzig	</td>  <td>VITZig</td></tr>
<tr class="PodcastTableRow">
<td>laid back </td>  <td>gelassen	</td>  <td>geLASSen</td></tr>
<tr class="PodcastTableRow">
<td>sensible </td>  <td> vern&uuml;nftig</td>  <td>	ferNOONFtig</td></tr>
<tr class="PodcastTableRow">
<td>immature</td>  <td> unreif</td>  <td>	unRIFE</td></tr>
<tr><td>&nbsp;</td></tr>
<!-- end of 3 -->

<!-- start of 2 -->
<tr id="2" class="PodcastTableRow"> 
<td colspan="3"><h2>October 2009 - My House</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow">
<td>the balcony  </td>  <td> der Balkon </td>  <td> dehr BAHL-con</td></tr>
<tr class="PodcastTableRow">
<td>the drawing room </td>  <td> der Salon </td>  <td> dehr SAH-lon</td></tr>
<tr class="PodcastTableRow">
<td>the porch</td>  <td> der Vorbau </td>  <td> dehr FOR-bow</td></tr>
<tr class="PodcastTableRow">
<td>the shower </td>  <td> die Dusche </td>  <td> dee DOO-shuh</td></tr>
<tr class="PodcastTableRow">
<td>the wall </td>  <td> die Wand </td>  <td> dee VAND</td></tr>
<tr class="PodcastTableRow">
<td>the cloakroom </td>  <td> die Garderobe </td>  <td> dee gah-duh-ROHW-buh</td></tr>
<tr class="PodcastTableRow">
<td>the gamesroom </td>  <td> das Spielzimmer </td>  <td> dass SHPEEL-tzim-uh</td></tr>
<tr class="PodcastTableRow">
<td>the fireplace </td>  <td> der Kamin </td>  <td> dehr KAM-een</td></tr>
<tr class="PodcastTableRow">
<td>the stairs </td>  <td> die treppe </td>  <td> dee TRE-puh</td></tr>
<tr class="PodcastTableRow">
<td>the window </td>  <td> das Fenster </td>  <td> dass FEN-stuh</td></tr>
<tr><td>&nbsp;</td></tr>
<!-- end of 2 -->

	<!-- episode 1--> 
<tr id="1" class="PodcastTableRow"> 
<td colspan="3"><h2>September 2009 - Greetings and pleasantries</h2></td> 
</tr> 
<tr class="PodcastTableHeader"> 
<td class="PodcastTableCell">English</td>  <td class="PodcastTableCell">German </td> 
<td class="PodcastTableCell">Phonetic</td></tr> 

<tr class="PodcastTableRow">
<td>
welcome! </td>  <td> Willkommen! </td>  <td> vill-KOMM-en</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
have fun </td>  <td> Viel Spass! </td>  <td> FEEL shPASS!</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
pleased to meet you </td>  <td> Sch&ouml;n Dich kennenzulernen! </td>  <td> shERRN DIKH ken-nen-tzoo-LEHR-nen!</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
to introduce </td>  <td> vorstellen </td>  <td> FOR-shtel-len</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
to hug </td>  <td>  umarmen </td>  <td> um-ARM-en</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
with pleasure </td>  <td>  gerne </td>  <td> GEHR-neh</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
thank you </td>  <td> danke </td>  <td> DAN-kuh</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
congratulations </td>  <td> Herzlichen Gl&uuml;ckwunsch! </td>  <td> HEHrtz-likh-en GLUEk-vunsh</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
have a nice trip! </td>  <td> Gute Reise! </td>  <td> GOO-teh RIY-suh</td></tr>
	<tr class="PodcastTableRow"> 		
<td>
sleep well </td>  <td> Schlaf gut! </td>  <td> sh-LAAF GOOT</td></tr>
<tr><td>&nbsp;</td></tr>

<!-- end of 1 -->


</table>