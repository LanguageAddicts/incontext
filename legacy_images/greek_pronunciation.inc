<h1>Greek Pronunciation Tool</h1>(Beta version 1.0)<p>

Welcome to our new Greek transliterator!</p>

<p>

Got a word in the greek script that you don't know how to pronounce? 

Our greek pronunciation guide is what you need.

</p> 





<p>For example, if you type in <b>"&#955;&#8057;&#947;&#959;&#962;"</b> our guide will tell you that this is pronounced "lOghoss".

The capital letter in the word tells you which sound to stress.</p> 



<!-- Put the Greek into the box below and click transliterate.</span></p>

<br/> -->

<form method="post" action="transliterator.php" >

<input type="text" name="greek" size="20" class="searchBox">

<input type="submit" value="Transliterate!">



</form>

<br/>



<table class=ShowHide>

<tr class=highlightTableRow>

<td><b>Greek</b></td>

<td>##########</td>

</tr>

<tr>

<td><b>English</b></td>

<td>@@@@@@@@@@</td>

</tr>
<tr>

<td>

</td>
 		
<td><!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_facebook_like" fb:like:layout="standard"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=undefined"></script>
<!-- AddThis Button END --></td>

</tr>
</table>



<p>

Our transliterator is currently in beta, but will work well for most words. 

Feel free to <a href="contact.htm" title="contact"> 

contact</a>  us if you find any errors or have any suggestions for improvements!</p>

<br/>


