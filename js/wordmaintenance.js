$("document").ready(function()
{
$('#wordlist').load('drawPhraseList.php');	//draw the word list when the page loads
$("#sub").click( function() {
var Values = $("#word_entry :input").val();
if (Values == "")
{
alert("BlankSpanner!");
}
else
{
  var scriptName = "addphrase.php";
  var params = $("#word_entry :input").serializeArray();
  if ($('#radioPhrase').prop('checked')) 
  {
  params.push({name: 'phrase', value: "Y"});
  var pageRefresh= 'drawPhraseList.php';
  } 
  else
  {
  params.push({name: 'phrase', value: "N"});
  var pageRefresh= 'drawWordList.php';
  }
  
 $.post( scriptName, 
         params, 
         function(info){ $("#result").html(info); 
   });
	setTimeout(function(){						//allow db to catch up
    $('#wordlist').load(pageRefresh)}		//reload word list
	,200);
clearInput();
 }
});

//change content view according to which radio button has been clicked
$('#radioPhrase').click( function(){
$('#wordlist').load('drawPhraseList.php');	
});
$('#radioWords').click( function(){
$('#wordlist').load('drawWordList.php');	//draw the word list when the page loads
});

$("#word_entry").submit( function() {     //prevent action script in page being called
  return false;	
});
	
//clear contents of #target area when mouse moves into top div

$('#wordlist').mousemove(function(){
$("#result").html("");
});
									
});

 
function clearInput() {
	$("#word_entry :input").each( function() {
	   $(this).val('');
	});

}

$( document ).on( "click", "[id^=deleteword]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
	var ID = $(this).attr('id').split('-')[1];				//deduce the page number from the class ID (its the number after the hyphen)
	 $.post("deleteword.php", 						//call script to delete the entry
         {wordid: ID}, 										//set the data to send in the POST
         function(info){ $("#result").html(info);			//set the format of the delete message
  })
  $('#wordlist').load('drawWordList.php');	//reload word list
});

$( document ).on( "click", "[id^=deletephrase]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
	var ID = $(this).attr('id').split('-')[1];				//deduce the page number from the class ID (its the number after the hyphen)
	 $.post("deletephrase.php", 						//call script to delete the entry
         {phraseid: ID}, 										//set the data to send in the POST
         function(info){ $("#result").html(info);			//set the format of the delete message
  })
  $('#wordlist').load('drawPhraseList.php');	//reload word list
});

$( document ).on( "click", "[id^=savephrase]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
  var ID = $(this).attr('id').split('-')[1];				//deduce the page number from the class ID (its the number after the hyphen)
  var phraseID = "phraseString-" + ID;
  var phraseTextAreaInputSelector =  'textarea#'+phraseID;
  var phraseTextAreaInput = $(phraseTextAreaInputSelector).val();
  
  var notesID = "notesString-" + ID;
  var notesTextAreaInputSelector =  'textarea#'+notesID;
  var notesTextAreaInput = $(notesTextAreaInputSelector).val();
  
  var themeID = "themeString-" + ID;
  var themeSelectSelector =  'select#'+themeID;
  var themeSelectInput = $(themeSelectSelector).val();
  
  var pointsID = "pointsString-" + ID;
  var pointsSelectSelector =  'select#'+pointsID;
  var pointsSelectInput = $(pointsSelectSelector).val();
   $.post("addphrase.php", 						//call script to save the entry
         {phraseid: ID,word:phraseTextAreaInput,notes:notesTextAreaInput,theme:themeSelectInput,phrase:'Y',points:pointsSelectInput}, 										//set the data to send in the POST
         function(info){ $("#result").html(info);			//set the format of the save message
         
  })
  	setTimeout(function(){						//allow db to catch up
    $('#wordlist').load('drawPhraseList.php')}		//reload word list
	,300);
});


$( document ).on( "click", "[id^=radioWords]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
	var ID = $(this).attr('id');		
	$.post( "drawWordList.php", 
    {sort: ID}, 
    function(data){
    $('#wordlist').html(data);
  }); 
});