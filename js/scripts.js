
$("document").ready(function()
{

$('#leaderboard').load('leaderboard.php?direct=FALSE');			//load leaderbord when page loads
$("#moreLeaderBoard").on("ready click",function(){				//load leaderboard when 'more' button clicked
   $('#leaderboard').load('leaderboard.php?direct=TRUE&direction=MORE');				
});

$("#lessLeaderBoard").on("ready click",function(){				//load leaderboard when 'less' button clicked
  $('#leaderboard').load('leaderboard.php?direct=TRUE&direction=LESS');				
});

$('#byThisAuthor').load('bythisauthor.php?direct=FALSE');		//load author gallery on page load
$('#moreFromAuthor').click(function(){							//load author gallery when 'more' button clicked
	$('#byThisAuthor').load('bythisauthor.php?direct=TRUE');			
});
$('#mostRecentContent').load('mostRecent.php?direct=FALSE'); 	//load recent videos list on page load
$('#gallerymoreMain').click(function(){							//load recent videos list when 'more' button clicked
	$('#mostRecentContent').load('mostRecent.php?direct=NEXT');
});

$('#gallerylessMain').click(function(){							//load recent videos list when 'more' button clicked
	$('#mostRecentContent').load('mostRecent.php?direct=PREV');
});

$("[id^=galleryPage").click(function(){							//a specific page number has been requested

	var ID = $(this).attr('id').split('-')[1];							//deduce the page number from the class ID (its the number after the hyphen)
	$('#mostRecentContent').load('mostRecent.php?direct=TRUE&page=' + ID);  //load script passing in page number
});

});



	