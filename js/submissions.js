$("document").ready(function()
{
$('#wordlist').load('drawSubmissionList.php');	//draw the word list when the page loads
								
});

 
function clearInput() {
	$("#word_entry :input").each( function() {
	   $(this).val('');
	});

}
//save any rejection text entered once user tabs off box
$( document ).on( "blur", "[id^=decision]", function() {
var ID = $(this).attr('id').split('-')[1];
var decisionNotesID = "decision-" + ID;
var decisionNotesTextAreaInputSelector =  'textarea#'+decisionNotesID;
var decisionNotesTextAreaInput = $(decisionNotesTextAreaInputSelector).val();
 $.post( "savesubmission.php", 
         {action: "decisionnotes", notes: decisionNotesTextAreaInput, submitID: ID});
});

$( document ).on( "click", "[id^=reject]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
//
	var ID = $(this).attr('id').split('-')[1];				//deduce the rejection number from the class ID (its the number after the hyphen)
  // validate the text 
  fnOpenNormalDialog(ID,"rejection");
});

$( document ).on( "click", "[id^=partial]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
//
	var ID = $(this).attr('id').split('-')[1];				//deduce the rejection number from the class ID (its the number after the hyphen)
  // validate the text 
  fnOpenNormalDialog(ID,"partial_rejection");        
});

$( document ).on( "click", "[id^=accept]", function() {		//set up event handler for any clicks on 'delete*' which occur after initial page load
//
if (confirm("Definately accept entry?") == true) {
	var ID = $(this).attr('id').split('-')[1];				//deduce the rejection number from the class ID (its the number after the hyphen)
  $.post("processSubmission.php",{action: "acceptance",recordID: ID})
window.location.href = "addvid.php?submissionID="+ID;
    } 
});

function processEmailDialog(value,ID,ProcessingAction){
  if (value) {
  //proceed with rejection
  $.post("processSubmission.php",{action: ProcessingAction,recordID: ID})
  //redraw page
    $('#wordlist').load('drawSubmissionList.php');	//reload word list
  }
  else
  {
  // halt
  alert("process cancelled");
  }
}
function fnOpenNormalDialog(ID, ProcessingAction) {
var email_action = ProcessingAction + "_email";
    $.post("processSubmission.php",{action:email_action,recordID: ID},function(result){
      $("#dialog-confirm").html(result);
    })    
    // Define the Dialog and its properties.
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: "send rejection email with this text?",
        height: 550,
        width: 500,
        buttons: {
            "Yes": function () {
                $(this).dialog('close');
                processEmailDialog(true,ID,ProcessingAction);
            },
                "No": function () {
                $(this).dialog('close');
                processEmailDialog(false);
            }
        }
    });
}

