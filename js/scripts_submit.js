$(document).ready(function() {
    
    //if submit button is clicked
    $('#submit_btn').click(function () {        
        
        //Get the data from all the fields
        var name = $('input[name=name]');
        var email = $('input[name=email]');
        var website = $('input[name=website]');
        var file = $('input[name=file]');
        //Simple validation to make sure user entered something
        //If error found, add hightlight class to the text field
		if (file.val()=='') {
            alert("Please select a file to upload");
            return false;
        } 
        
        if (name.val()=='') {
            name.addClass('hightlight');
            return false;
        } else name.removeClass('hightlight');
        
        if (email.val()=='') {
            email.addClass('hightlight');
            return false;
        } else email.removeClass('hightlight');
        
        //organize the data properly
        var data = 'name=' + name.val() + '&email=' + email.val()  + '&file='  + encodeURIComponent(file.val())+ '&website=' + website.val();
        
        //disable all the text fields
        $('.text').attr('disabled','true');
        
        //show the loading sign
        $('.loading').show();
        
        //start the ajax
        $.ajax({
            //this is the php file that processes the data and sends	 mail
            url: "comp_process.php",    
            
            //GET method is used
            type: "GET",
            //pass the data            
            data: data,        
            
            //Do not cache the page
            cache: false,
            
            //success
            success: function (html) {    
                 //show the result message
                 $('.done').fadeIn('slow');            
                if (html=='ok') {           
					//Build thankyou message
					var msg = 'Thank you ' + name.val() +  'for your entry ' +  file.val() + '. We will let you know if it passed in within 72 hours.' +
					' All payments will be made via paypal after the close of the competition.'
					$('.done').text(msg)
                    //hide the form
                    $('.form').fadeOut('slow');                    
                }    
				else
				{
				$('.done').text(html);
				 //re-enable all the text fields
				$('.text').attr('enabled','true');      
				//hide the loading sign
				$('.loading').hide();
				}
            }        
        });
        
        //cancel the submit button default behaviours
        return false;
    });    
});