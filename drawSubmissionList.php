<?php
require("db_functions.php");
global $connection; 											//set up db connection using global variable
opendb(); 
$submissionlist = getSubmissions();  								//get list of wanted words

	while($submission = mysqli_fetch_array($submissionlist))
	{
	if ($submission["STATUS"] == null)							//only unprocessed
	{
		$submissions[] = $submission;									//store the sql result in working array
	}
	}
$total = count($submissions);

echo"<br>";
//create grid div
echo "<div id=\"phraselist\">";
//draw grid header row 
echo "<table class=\"wordlisttable\">";
echo "<tr class=\"tableRowStyle1\"><td>Name</td><td>file</td><td>Submitted</td><td>Producer notes</td><td>Decision notes</td><td>Action</td></tr>";
//loop through each item in the table
if (!empty($submissions))
{
foreach($submissions as $gridEntry)							
	{	
	//prepare data
    $prodnotes=stripslashes($gridEntry["PRODNOTES"]);
    $decision=stripslashes($gridEntry["DECISIONNOTES"]);
	$email=$gridEntry["EMAIL"];
	$ID=$gridEntry["SUBMISSIONID"];
	$name=$gridEntry["NAME"];
	$submitted=$gridEntry["SUBMITTED"];
	$filename=$gridEntry["FILENAME"];
	$fullfilename="upload/pending/".$filename;
	$sceneID = $gridEntry["SCENEID"];
	//get phrase to display in hover text
	$phraseText = "not read yet: ".$sceneID;
	if ($sceneID != null and $sceneID != 0)
	{
	$phraseText = getPhrase($sceneID);
	}else{
	$phraseText ="phase not selected by user";
	}
	//create ids for ajax layer
	$prodnotesID="prodnotes-".$ID;
	$decisionID="decision-".$ID;
	$downloadID="download-".$ID;
	$acceptID="accept-".$ID;
	$rejectID="reject-".$ID;
  $partialID="partial-".$ID;
	//draw grid row
	echo "<tr>";
	echo "<td><span title=\"{$email}\">{$name}</td>";
	echo "<td><textarea readonly cols=\"30\" rows=\"3\" span title=\"{$phraseText}\">{$filename}</textarea></td>";
	echo "<td>{$submitted}</td>";
	echo "<td><textarea id=\"{$prodnotesID}\" cols=\"25\" rows=\"4\">{$prodnotes}</textarea></td>";
	echo "<td><textarea id=\"{$decisionID}\" cols=\"25\" rows=\"4\">{$decision}</textarea></td>";
	//<a href=\"{$fullfilename}\" target=\"_blank\"><img border=\"0\" src=\"images/download.png\" ></a>
	echo "<td><a href=\"{$fullfilename}\" target=\"_blank\"><img border=\"0\" src=\"images/download.png\" title=\"download file\" ></a>
	<IMG SRC=\"images/blank.gif\" HSPACE=2>	<a href=\"#\" id=\"{$acceptID}\"><img src=\"images/accept.png\" title=\"accept entry\"></a>
	<IMG SRC=\"images/blank.gif\" HSPACE=2><a href=\"#\" id=\"{$rejectID}\"><img src=\"images/reject.png\" title=\"complete rejection\"></a>
  <IMG SRC=\"images/blank.gif\" HSPACE=2><a href=\"#\" id=\"{$partialID}\"><img src=\"images/nearly.png\" title=\"reject but try again\"></a></td>";
		}
}
      echo "</table>";
echo "</div>";
mysqli_close($connection);										//close the database connection


?>