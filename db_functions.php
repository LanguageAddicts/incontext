<?php
require("common_db/common_db_functions.php");
require("common/common_functions.php");


function getVideoFiledetails($videoid)
{
//build the query to get the video table record for this video ID
trace("getVideoFiledetails, videoID:", $videoid);
global $connection;
$sql = "SELECT * FROM `videos` WHERE `VIDEOID` = $videoid LIMIT 0, 30";
//execute read for this file id
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
$vidrow = mysqli_fetch_assoc($result);
trace("getVideoFiledetails, vidrow:", $vidrow);
return $vidrow;
}
function getSubmissions()
{
// find the most recent live entry submitted by this author
global $connection;
$sql = "SELECT * FROM `submissions` ORDER BY SUBMITTED ASC";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}
function getSubmission($submissionID)
{
// find the most recent live entry submitted by this author
global $connection;
$sql = "SELECT * FROM `submissions` WHERE `SUBMISSIONID` = $submissionID ORDER BY SUBMITTED ASC";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}
function updateSubmission($ID, $status)
{
$timeProcessed = date("Y/m/d h:i:s a");
global $connection;
$sql = "UPDATE `submissions` SET status='$status', timeprocessed='$timeProcessed' WHERE submissionid='$ID'";
$result = mysqli_query($connection,$sql);
if (mysqli_error($connection)){
  return mysqli_error($connection)."<br>";
}
else{
return $result;}
}

function getContributorID($email)
{
	// find the most recent live entry submitted by this author
	global $connection;
	$sql = "SELECT * FROM contributors WHERE EMAIL = '$email'";
	$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
	$contributorArray = mysqli_fetch_array($result);
	$ContributorID = $contributorArray["contribid"];
	echo $ContributorID;
	return $ContributorID;
}

function getAllTags()
{
// get full list of themes from tag file
global $connection;
$sql = "SELECT * FROM `tags` LIMIT 0, 30";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}

function getWordListEntryByName($inputword)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$word = mysqli_real_escape_string($connection, $inputword);
$sql = "SELECT * FROM `wantedwords` WHERE wantedwords.word = '$word' LIMIT 0, 30 ";
$result = mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return mysqli_num_rows($result);
}

function getWantedPhrases()
{
global $connection;
$sql = "SELECT * FROM `wantedphrases` ORDER BY phraseid DESC LIMIT 0, 100 ";
$result = mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}

function getPhrase($phraseid)
{
	
	// get phrase
	global $connection;
	$sql = "select phrase from wantedphrases where phraseid = '$phraseid'";
	$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
	$phraseArray = mysqli_fetch_array($result);
	$phrase =  $phraseArray["phrase"];
	return $phrase;

}

function getPhrasesByWord($inputword)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$word = mysqli_real_escape_string($connection, $inputword);
$sql = "SELECT * FROM `wantedphrases` WHERE `PHRASE` LIKE '% $word' OR `PHRASE` LIKE '$word %' OR `PHRASE` LIKE '% $word %' LIMIT 0, 30 ";
$result = mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return mysqli_num_rows($result);
}

function insertPhrase($inputphrase, $inputnotes)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$phrase = mysqli_real_escape_string($connection, $inputphrase);
$notes = mysqli_real_escape_string($connection, $inputnotes);
$sql="INSERT INTO wantedphrases (`phrase`,`notes`) VALUES('$phrase','$notes')";
$result = mysqli_query($connection, $sql);
if (mysqli_error($connection)){
  return mysqli_error($connection)."<br>";
}
else{
return $result;}
}

function insertSubmission($inputemail,$inputname,$inputfilename,$inputurl,$sceneid,$inputprodnotes,$inputoriginalfile,&$sucess)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$email = mysqli_real_escape_string($connection, $inputemail);
$name = mysqli_real_escape_string($connection, $inputname);
$filename = mysqli_real_escape_string($connection, $inputfilename);
$url = mysqli_real_escape_string($connection, $inputurl);
$prodnotes = mysqli_real_escape_string($connection, $inputprodnotes);
$originalfile = mysqli_real_escape_string($connection, $inputoriginalfile);
$sql="INSERT INTO submissions (`NAME`,`EMAIL`,`FILENAME`,`URL`,`SCENEID`,`PRODNOTES`,`ORIGINAL_FILENAME`) VALUES('$name','$email','$filename','$url','$sceneid','$prodnotes','$originalfile')";

$result = mysqli_query($connection, $sql);
if (mysqli_error($connection)){
	$sucess = false;
  return mysqli_error($connection);
}
else{
$success=true;
return $result;}
}
function updatePhrase($inputphrase, $inputnotes, $ID, $theme,$points)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$phrase = mysqli_real_escape_string($connection, stripslashes($inputphrase));
$notes = mysqli_real_escape_string($connection, stripslashes($inputnotes));
$sql = "UPDATE `wantedphrases` SET phrase='$phrase', notes='$notes',theme='$theme',points='$points' WHERE phraseid='$ID'";
$result = mysqli_query($connection,$sql);
if (mysqli_error($connection)){
  return mysqli_error($connection)."<br>";
}
else{
return $result;}
}

function updateSubmissionNotes($ID, $inputnotes)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$notes = mysqli_real_escape_string($connection, stripslashes($inputnotes));
$sql = "UPDATE `submissions` SET decisionnotes='$notes' WHERE submissionid='$ID'";
$result = mysqli_query($connection,$sql);
if (mysqli_error($connection)){
  return mysqli_error($connection)."<br>";
}
else{
return $result;}
}

function insertWord($inputword)
{
global $connection;
mysqli_set_charset($connection, "utf8");
$word = mysqli_real_escape_string($connection, $inputword);
$sql="INSERT INTO wantedwords (`word`) VALUES('$word')";
$result = mysqli_query($connection, $sql);
if (mysqli_error($connection)){
  return mysqli_error($connection);
}
else{
return "word added: ".$word;}
}

function deleteWord($wordid)
{
global $connection;
$sql="DELETE FROM wantedwords WHERE `wordid` = $wordid ";
if(mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql))
  return "Successfully deleted";
else
return "deletion Failed";
}

function deletePhrase($phraseid)
{
global $connection;
$sql="DELETE FROM wantedphrases WHERE `phraseid` = $phraseid ";
if(mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql))
  return "Successfully deleted";
else
return "deletion Failed";
}

function getMostRecentVideos()
{
$fromdate='2014-05-01 00:00:00'; //// competition, remove 
global $connection;
$sql = "SELECT videos.VIDEOID, videos.SUBMITTED, contributors.name, contributors.surname FROM (`videos` INNER JOIN contributors ON videos.CONTRIBID=contributors.contribid) WHERE DATE(videos.SUBMITTED) > '2014-05-05' ORDER BY videos.SUBMITTED DESC";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
//sort the results
return $result;
}

function getContributors()
{
//get all contributors where the contributor has at least one live video
global $connection;
$sql = "SELECT COUNT(videos.CONTRIBID) AS entries,\n"
    . " contributors.name,\n"
    . " contributors.surname,\n"
    . " contributors.contribid\n"
    . "FROM contributors\n"
    . "INNER JOIN videos ON contributors.contribid = videos.CONTRIBID\n"
    . "WHERE videos.ACTIVE = \"Y\"\n"
    . "GROUP BY videos.CONTRIBID\n"
    . "ORDER BY entries DESC";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}

function getLatestVideos($limit=1)
{
// find the most recent live entry submitted by this author
global $connection;
$sql = "SELECT * FROM `videos` WHERE videos.ACTIVE = \"Y\" ORDER BY videos.CREATED DESC LIMIT 0,$limit";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}

function getWantedWordsByGroup($getWordCount = false)
{
// find all entries in the wanted words file

global $connection;

switch ($getWordCount)
{
// find all entries in the wanted words file
case false:
$sql = "SELECT * FROM `wantedwords`  ORDER BY wantedwords.GROUP, wantedwords.WORD ASC";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
break;

case true:
// find all entries in the wanted words file as well as the number of videos they appear in
$sql = "SELECT DISTINCT wantedwords.WORDID ,wantedwords.GROUP, wantedwords.WORD, wantedwords.COMMENTS, COUNT(videowords.WORDID) AS videoCount FROM wantedwords \n"
    . "LEFT OUTER JOIN words\n"
    . "ON wantedwords.WORD=words.WORD\n"
    . "LEFT OUTER JOIN videowords\n"
    . "ON words.WORDID = videowords.WORDID\n"
    . "GROUP BY wantedwords.WORD\n"
    . "ORDER BY wantedwords.GROUP, wantedwords.WORD ASC\n"
    . "\n";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
break;
}
}

function getWantedWords($getWordCount = false, $sortOrder="WordsNewestFirst")
{
// find all entries in the wanted words file
global $connection;

switch ($getWordCount)
{
// find all entries in the wanted words file
case false:
$sql = "SELECT * FROM `wantedwords`  ORDER BY wantedwords.GROUP, wantedwords.WORD ASC";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
break;

case true:

switch ($sortOrder)
{
case "WordsNewestFirst":
$orderBy="ORDER BY wantedwords.WORDID DESC\n";
BREAK;
case "WordsOldestFirst":
$orderBy="ORDER BY wantedwords.WORDID ASC\n";
BREAK;
case "WordsAtoZ":
$orderBy="ORDER BY wantedwords.WORD ASC\n";
BREAK;
case "WordsZtoA":
$orderBy="ORDER BY wantedwords.WORD DESC\n";
break;
case "WordsMostLiveFirst":
$orderBy="ORDER BY videoCount DESC\n";
BREAK;
case "WordsLeastLiveFirst":
$orderBy="ORDER BY videoCount ASC\n";
BREAK;
default:
$orderBy="ORDER BY wantedwords.WORDID DESC\n";
}
// find all entries in the wanted words file as well as the number of videos they appear in
$sql = "SELECT DISTINCT wantedwords.WORDID ,wantedwords.GROUP, wantedwords.WORD, wantedwords.COMMENTS, COUNT(videowords.WORDID) AS videoCount FROM wantedwords \n"
    . "LEFT OUTER JOIN words\n"
    . "ON wantedwords.WORD=words.WORD\n"
    . "LEFT OUTER JOIN videowords\n"
    . "ON words.WORDID = videowords.WORDID\n"
	. "GROUP BY wantedwords.WORD\n"
    //. "ORDER BY wantedwords.WORDID DESC\n";
	.$orderBy;
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
break;
}
}

function getAcceptedVideoCount()
{
// find the most recent live entry submitted by this author
global $connection;
$sql = "SELECT COUNT(videos.CONTRIBID) AS entries FROM `videos` WHERE videos.ACTIVE = \"Y\"";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
$vidrow = mysqli_fetch_assoc($result);
return $vidrow["entries"];
}

function getPendingVideoCount()
{
// find the most recent live entry submitted by this author
global $connection;
$sql = "SELECT COUNT(videos.CONTRIBID) AS entries FROM `videos` WHERE videos.ACTIVE = \" \" AND videos.SUBMITTED >='2013-10-01'";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
$vidrow = mysqli_fetch_assoc($result);
return $vidrow["entries"];
}

function getAuthorLatestVideos($authorID, $limit=1)
{
// find the most recent live entry submitted by this author
global $connection;
$sql = "SELECT * FROM `videos` WHERE videos.CONTRIBID = $authorID ORDER BY videos.CREATED DESC LIMIT 0,$limit";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}

function getAuthorDetails($authorID)
{
global $connection;
$sql = "SELECT * FROM `contributors` WHERE contribid='$authorID'\n"
    . " LIMIT 0, 30 ";
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
return $result;
}

function getFullWordList()
{
// Get all the data from the "words" table
global $connection;
$sql = "SELECT * FROM words ORDER BY word"; 
//execute read for this file id
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
return $result;
}

function  getVideoFiledetailsByWord($wordid)
{//build the query to get all video files containing this word
trace("getVideoFiledetailsByWord, wordID: $wordid");
global $connection;
$sql = "SELECT DISTINCT videos.WORDCOUNT, videos.VIDEONAME, videos.VIDEOID \n"
    . "FROM videos\n"
    . "INNER JOIN videowords\n"
    . "ON videos.videoid=videowords.videos_videoid\n"
    . "WHERE videowords.wordid = $wordid LIMIT 0, 30 ";
//execute read for this word id
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
return $result;
}

function getDateVideoLastShown($videoid, $wordid = null)
{
global $connection;
if ($wordid != null)
{$sql = "SELECT * FROM `videohistory` WHERE `VIDEOID`='$videoid' AND `WORDID`='$wordid' ORDER BY `DATEVIEWED` DESC LIMIT 1";}
else
{$sql = "SELECT * FROM `videohistory` WHERE `VIDEOID`='$videoid' ORDER BY `DATEVIEWED` DESC LIMIT 1";}
$date = null;
$result=mysqli_query($connection,$sql) or die(mysqli_error($connection)) ;
if (mysqli_num_rows($result)>0 )
{
$histrow = mysqli_fetch_array($result);
$date = $histrow["DATEVIEWED"];
}

return $date;
}

function getVideosLastShown($limit = 10)
{
global $connection;
$sql = "SELECT videohistory.VIDEOID, videohistory.WORDID,videohistory.DATEVIEWED, videos.VIDEONAME\n"
    . "FROM videohistory LEFT JOIN videos\n"
    . "ON videohistory.videoid=videos.videoid\n"
    . "ORDER BY `SERIAL` DESC LIMIT $limit";
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
if (mysqli_num_rows($result)>0 )
{ 
return $result;
}
return null;
}

function isWordKnown($wordid, $user)
{
trace("isWordKnown");
// read user's library to establish if this word is known
global $connection;
$sql = "SELECT * FROM `wordlibrary` WHERE `USER`=\"$user\" AND `WORDID` = $wordid";

$result=mysqli_query($connection,$sql) or die(mysqli_error($connection));

if (mysqli_num_rows($result)>0 )
{ 
return true;
}
else
{
return false;
}
}
function printvidfiledetails($result)
{//how many entries are there?
global $connection;
$num=mysqli_num_rows($result);
//if ($num > 0){
echo "There are $num videos for this word";
}


function getWord($wordid){ 
trace("get word:".$wordid);
global $connection;
 //build the query to find out what word this is
$sql = "SELECT * FROM `words` WHERE `WORDID` = $wordid LIMIT 0, 30 ";
//execute read for this word id
$result=mysqli_query($connection, $sql);
// or die($sql)
$num = mysqli_num_rows($result);
if ($num > 0)
  { 
    $row = mysqli_fetch_assoc($result);
    return $row["WORD"];
  }
else
  { die("Error:$sql");
  }

}


function saveVideoHistory($chosenVideo,$wordid,$user)
{
global $connection;
$vidid = $chosenVideo["VIDEOID"];

$sql = "INSERT INTO `englisq7_videolang`.`videohistory` (`SERIAL`, `USER`, `VIDEOID`, `WORDID`, `DATEVIEWED`) VALUES (NULL, '$user', '$vidid', '$wordid', NOW());";
 $result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
}

function saveWord($wordid, $vidid = null)
{
global $connection;

$sql = "INSERT INTO `englisq7_videolang`.`wordlibrary` (`SERIAL`, `USER`, `WORDID`,`VIDCRACKED`, `DATECRACKED`) VALUES (NULL, '$user','$wordid','$vidid', NOW());";
 $result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
}

function getVideowords($vidid)
{// read the videowords table to return the list of words and the order in which they appear
trace("getVideoWords");
global $connection;
$sql = "SELECT * FROM `videowords` WHERE `VIDEOS_VIDEOID` = $vidid ORDER BY `SEQUENCE` ASC LIMIT 0, 30 ";
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
return $result;
}

function getVideoAttribution($VideoID)
{// read the attribution table to return the list of attribution and the order in which they appear
trace("getVideoAttribution");
global $connection;
$sql = "SELECT * FROM `attribution` WHERE `VIDEOS_VIDEOID` = $VideoID ORDER BY `SEQUENCE` ASC LIMIT 0, 30 ";
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
return $result;
}

function getVideosWithKnownWords()
{
trace("getVideosWithKnownWords");
global $connection;

//Explanation: The query consists of three tables which are videos, videowords and wordlibrary
//We return a list of video files for which the user already knows at least one word
//The actual information needed for this query to work is in videos and wordlibrary but these two 
//tables do not link so require videowords as the bridge

//left join syntax means that all records in the table on the left hand side of the statement are 
//returned but on those that have a match from the right

//'Count' syntax allows us to sum the number of records for given items
//'AS' syntax allows us to attach our own label to the result of the count
//'group by' will group the result set according to the table/field specified.

/* Replace references to wordvideomap with videowords     
$sql = "SELECT wordvideomap.WORDID, videos.VIDEOID, videos.VIDEONAME, videos.WORDCOUNT, COUNT( wordlibrary.wordid ) AS KNOWNWORDS\n"
    . "FROM (\n"
    . "videos\n"
    . "LEFT JOIN wordvideomap ON videos.videoid = wordvideomap.videoid\n"
    . ")\n"
    . "LEFT JOIN wordlibrary ON wordvideomap.wordid = wordlibrary.wordid\n"
    . "WHERE wordlibrary.wordid IS NOT NULL \n"   //**BUG HERE** This join should take account of the user id
    . "GROUP BY videos.videoid\n" ;
*/
$sql = "SELECT videowords.WORDID, videos.VIDEOID, videos.VIDEONAME, videos.WORDCOUNT, COUNT( wordlibrary.wordid ) AS KNOWNWORDS\n"
    . "FROM (\n"
    . "videos\n"
    . "LEFT JOIN videowords ON videos.videoid = videowords.videos_videoid\n"
    . ")\n"
    . "LEFT JOIN wordlibrary ON videowords.wordid = wordlibrary.wordid\n"
    . "WHERE wordlibrary.wordid IS NOT NULL \n"   //**BUG HERE** This join should take account of the user id
    . "GROUP BY videos.videoid\n" ;
       
$result=mysqli_query($connection, $sql)or die(mysqli_error($connection).$sql);
//sort the results
return $result;
}

function getWordfrequency($targetWord)
{
global $connection;
$sql = "SELECT DISTINCT VIDEOS_VIDEOID FROM `videowords` WHERE `WORDID` = $targetWord";
$result=mysqli_query($connection, $sql) or die(mysqli_error($connection));
return mysqli_num_rows($result);
}

?>