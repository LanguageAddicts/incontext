<?php
/**
* @desc Form file
* @package form
*/
class form_file {
  
  private $name;
  private $size;
  private $cssClass;
  private $style;
  private $formHTML;
  
  /**
  * @desc Constructor
  */
  public function __construct($name) {
    $this->name = $name;
  }
  
  /**
  * @desc Add form attributes
  * @param integer $size Length of the field
  * @param integer $maxlengh Maximum length of entered data
  * @param string $cssClass Css class to use for field
  * @param string $style Style options
  * @param string $value Default field value
  */
  public function add_attributes($size, $cssClass='', $style='') {
    $this->size = $size;
    if(strlen($cssClass) > 0) {
      $this->cssClass = $cssClass;
    }
    if(strlen($style) > 0) {
      $this->style = $style;
    }
    // Create form html
    $this->create_html();    
  }
  
  /**
  * @desc Create html for form based on attributes
  */
  private function create_html() {
    $this->formHTML = "<input type=\"file\" name=\"" . $this->name;
    $this->formHTML .= "\" id=\"" . $this->name;
    $this->formHTML .= "\" size=\"" . $this->size;
    $this->formHTML .= "\" value=\"";
    if(isset($this->cssClass)) {
      $this->formHTML .= "\" class=\"" . $this->cssClass;
    }
    if(isset($this->style)) {
      $this->formHTML .= "\" style=\"" . $this->style;
    }
    $this->formHTML .= "\" />\r\n";
  }
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value() {
    return $_POST[$this->name];
  }
  
  /**
  * @desc Get name
  */
  public function get_name() {
    return $this->name;
  }
}
?>