<?php
/**
* @desc Form array select menu
* @package form
*/
class form_arrayselect {
  
  private $name;
  private $options;
  private $cssClass;
  private $style;
  private $preset;
  private $formHTML;
  
  /**
  * @desc Constructor
  */
  public function __construct($name) {
    $this->name = $name;
  }
  
  /**
  * @desc Add form attributes
  * @param array $options Select menu options
  * @param string $cssClass Css class to use for field 
  * @param string $style Style options
  * @param string $preset Pre selected menu option
  */
  public function add_attributes($options, $cssClass='', $style='', $preset='') {
    $this->options = $options;
    if(strlen($cssClass) > 0) {
      $this->cssClass = $cssClass;
    }
    if(strlen($style) > 0) {
      $this->style = $style;
    }
    $this->preset = $preset;
    // Create form html
    $this->create_html();    
  }
  
  /**
  * @desc Create html for form based on attributes
  */
  private function create_html() {
    $this->formHTML = "<select name=\"" . $this->name;
    $this->formHTML .= "\" id=\"" . $this->name;
    if(isset($this->cssClass)) {
      $this->formHTML .= "\" class=\"" . $this->cssClass;
    }
    if(isset($this->style)) {
      $this->formHTML .= "\" style=\"" . $this->style;
    }
    $this->formHTML .= "\">\r\n";
    foreach($this->options as $key => $value) {
      $this->formHTML .= "<option value=\"" . $this->sticky($key) . "\">";
      $this->formHTML .= $value;
      $this->formHTML .= "</option>\r\n";
    }
    $this->formHTML .= "</select>\r\n";
  }
  
  /**
  * @desc Sticky/Default value
  */
  private function sticky($key) {
    if(isset($_POST[$this->name])) {
      // $_POST value exists
      if($_POST[$this->name] == $key) {
        return $key . "\" selected=\"selected";
      } else {
        return $key;
      }
    } else {
      // New form
      if($key == $this->preset) {
        return $key . "\" selected=\"selected";
      } else {
        return $key;
      }      
    }    
  }
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value() {
    return $_POST[$this->name];
  }
  
}
?>