<?php
/**
* @desc Form class, creates form, action buttons and fieldset if required
* @package form
*/
class form_form {
  
  private $outputStart;
  private $fieldset;
  private $action;
  private $self;
  private $type;
  
  /**
  * @desc Constructor, set options for form
  * @param string $action Form action, additonal get or set parameters
  * @param string $self Whether the form is resubmitted back to itself or a seperate page, 0=Self, 1=Other. If set to 1 the value from $action is used.
  * @param integer $type Form type, standard = 0 or multipart = 1
  */
  public function __construct($action='', $self=0, $type=0) {
    $this->fieldset = 0;
    $this->action = $action;
    $this->self = $self;
    $this->type = $type;
    $this->create_start_form();
  }
  
  private function create_start_form() {
    if($this->self == 0) {
      // Form being submitted back to original script
      if($this->type == 0) {
        // Standard form
        $this->outputStart = "\r\n<form action=\"" . $_SERVER['PHP_SELF'] . $this->action;
        $this->outputStart .= "\" method=\"post\">\r\n";
      } else {
        // Multipart form
        $this->outputStart = "\r\n<form action=\"" . $_SERVER['PHP_SELF'] . $this->action;
        $this->outputStart .= "\" enctype=\"multipart/form-data\" method=\"post";
        $this->outputStart .= "\">\r\n";      
      }      
    } else {
      // Form being submitted to a seperate script
      if($this->type == 0) {
        // Standard form
        $this->outputStart = "\r\n<form action=\"" . $this->action;
        $this->outputStart .= "\" method=\"post\">\r\n";
      } else {
        // Multipart form
        $this->outputStart = "\r\n<form action=\"" . $this->action;
        $this->outputStart .= "\" enctype=\"multipart/form-data\" method=\"post";
        $this->outputStart .= "\">\r\n";      
      }    
    } 
  }
  
  /**
  * @desc Fieldset, adds a legend to the form
  */
  public function create_fieldset($legend) {
    $this->fieldset = 1;
    $this->outputStart .= "<fieldset>\r\n";
    $this->outputStart .= "<legend>" . $legend . "</legend>\r\n";
  }
  
  /**
  * @desc Gets the html for the start of the form
  */
  public function get_html_start_form() {
    return $this->outputStart;    
  }
  
  /**
  * @desc Gets the html for the end of the form.
  */
  public function get_html_end_form() {
    if($this->fieldset == 1) {
      $output = "\r\n</fieldset>\r\n</form>";          
    } else {
      $output = "\r\n</form>";
    }
    return $output;
  }
  
  /**
  * @desc Form action button
  * @param string $name Name of the action button
  * @param string $label Visible label
  * @param string $class CSS class to use for button, defaults to null.
  * @param bool $active Whether or not the button is active
  */
  public function get_html_button($name, $label, $class='', $active=TRUE) {
    if(strlen($class) == 0) {
      // Button without css
      $output = "<input type=\"submit\" name=\"" . $name . "\" value=\"";
      $output .= $label . "\"";
      if($active) {
        $output .= " />\r\n";
      } else {
        $output .= " disabled=\"disabled\" />\r\n";
      }
    } else {
      // Button with css
      $output = "<input type=\"submit\" name=\"" . $name . "\" value=\"";
      $output .= $label . "\" class=\"" . $class . "\"";
      if($active) {
        $output .= " />\r\n";
      } else {
        $output .= " disabled=\"disabled\" />\r\n";
      }
      
               
    }
    return $output;            
  }
  
  /**
  * @desc Required field 
  */
  public function req_field() {
    return '<span class="req_field">*</span>';
  }
  
  /**
  * @desc Check to see if form has been submitted
  */
  public function is_submitted($name) {
    if(isset($_POST[$name])) {
      $result = TRUE;
    } else {
      $result = FALSE;
    }
    return $result;
  }  
}
?>