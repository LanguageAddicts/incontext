<?php
/**
* @desc Form textfield
* @package form
*/
class form_textfield {
  
  private $name;
  private $size;
  private $maxlength;
  private $cssClass;
  private $style;
  private $value;
  private $formHTML;
  /**
  * @var validation_validator
  */
  public $validation;
  
  /**
  * @desc Constructor
  */
  public function __construct($name) {
    $this->name = $name;
  }
  
  /**
  * @desc Add form attributes
  * @param integer $size Length of the field
  * @param integer $maxlengh Maximum length of entered data
  * @param string $cssClass Css class to use for field
  * @param string $style Style options
  * @param string $value Default field value
  * @param bool $id If TRUE id attribute is added to input tag
  */
  public function add_attributes($size, $maxlength, $cssClass='', $style='', $value='', $id=TRUE) {
    $this->size = $size;
    $this->maxlength = $maxlength;
    if(strlen($cssClass) > 0) {
      $this->cssClass = $cssClass;
    }
    if(strlen($style) > 0) {
      $this->style = $style;
    }
    $this->value = $value;
    // Create form html
    $this->create_html($id);
  }
  
  /**
  * @desc Create html for form based on attributes
  */
  private function create_html() {
    $this->formHTML = "<input type=\"text\" name=\"" . $this->name;
    if($id == TRUE) {
      $this->formHTML .= "\" id=\"" . $this->name;
    }
    $this->formHTML .= "\" size=\"" . $this->size . "\" maxlength=\"" . $this->maxlength;
    if(isset($_POST[$this->name])) {
      // $_POST value exists
      $this->formHTML .= "\" value=\"" . $_POST[$this->name];
    } else {
      // New form
      $this->formHTML .= "\" value=\"" . $this->value;
    }
    if(isset($this->cssClass)) {
      $this->formHTML .= "\" class=\"" . $this->cssClass;
    }
    if(isset($this->style)) {
      $this->formHTML .= "\" style=\"" . $this->style;
    }
    $this->formHTML .= "\" />\r\n";
  }
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value() {
    if(isset($_POST[$this->name])) {
      return $_POST[$this->name];
    } else {
      return $this->value;
    }
  }
  
}
?>