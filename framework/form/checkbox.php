<?php
/**
* @desc Form checkbox
* @package form
*/
class form_checkbox {
  
  private $name;
  private $cssClass;
  private $style;
  private $preset;
  private $formHTML;
  
  /**
  * @desc Constructor
  */
  public function __construct($name) {
    $this->name = $name;
  }
  
  /**
  * @desc Add form attributes
  * @param string $cssClass Css class to use for field
  * @param string $style Style options
  * @param string $preset Default field value
  */
  public function add_attributes($cssClass='', $style='', $preset='') {
    if(strlen($cssClass) > 0) {
      $this->cssClass = $cssClass;
    }
    if(strlen($style) > 0) {
      $this->style = $style;
    }
    if(strlen($preset) > 0) {
      $this->preset = $preset;
    } else {
      $this->preset = NULL;  
    }
    // Create form html
    $this->create_html();
  }
  
  /**
  * @desc Create html for form based on attributes
  */
  private function create_html() {
    $this->formHTML = "<input type=\"checkbox\" name=\"" . $this->name;
    $this->formHTML .= "\" id=\"" . $this->name;
    $this->formHTML .= "\" value=\"1";
    if(isset($_POST[$this->name])) {
      $this->formHTML .= "\" checked=\"checked";      
    } else {
      if($this->preset == 1) {
        $this->formHTML .= "\" checked=\"checked";
      }  
    }
    if(isset($this->cssClass)) {
      $this->formHTML .= "\" class=\"" . $this->cssClass;
    }
    if(isset($this->style)) {
      $this->formHTML .= "\" style=\"" . $this->style;
    }
    $this->formHTML .= "\" />\r\n";
  }  
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value() {
    if(isset($_POST[$this->name])) {
      return 1;
    } else {
      return 0;
    }
  }
  
}
?>