<?php
/**
* @desc Form textarea
* @package form
*/
class form_textarea {
  
  private $name;
  private $cols;
  private $rows;
  private $cssClass;
  private $style;
  private $value;
  private $formHTML;
  private $readonly;
  
  /**
  * @desc Constructor
  */
  public function __construct($name) {
    $this->name = $name;
  }
  
  /**
  * @desc Add form attributes
  * @param integer $cols The number of columns
  * @param integer $rows The number of rows
  * @param string $cssClass Css class to use for field
  * @param string $style Style options
  * @param string $value Default field value
  * @param bool $readonly Whether the field is read only.
  */
  public function add_attributes($cols, $rows, $cssClass='', $style='', $value='', $readonly=FALSE) {
    $this->cols = $cols;
    $this->rows = $rows;
    if(strlen($cssClass) > 0) {
      $this->cssClass = $cssClass;
    }
    if(strlen($style) > 0) {
      $this->style = $style;
    }
    $this->value = $value;
    $this->readonly = $readonly;
    // Create form html
    $this->create_html();    
  }
  
  /**
  * @desc Create html for form based on attributes
  */
  private function create_html() {
    $this->formHTML = "<textarea name=\"" . $this->name;
    $this->formHTML .= "\" id=\"" . $this->name;
    $this->formHTML .= "\" cols=\"" . $this->cols;
    $this->formHTML .= "\" rows=\"" . $this->rows;
    // CSS style
    if(isset($this->cssClass)) {
      $this->formHTML .= "\" class=\"" . $this->cssClass;
    }
    if(isset($this->style)) {
      $this->formHTML .= "\" style=\"" . $this->style;
    }
    if($this->readonly == TRUE) {
      $this->formHTML .= "\" readonly=\"readonly";
    }
    $this->formHTML .= "\">\r\n";
    if(isset($_POST[$this->name])) {
      // $_POST value exists
      $this->formHTML .= $_POST[$this->name];
    } else {
      // New form
      $this->formHTML .= $this->value;
    }
    $this->formHTML .= "</textarea>\r\n";
  }
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value() {
    if(isset($_POST[$this->name])) {
      return $_POST[$this->name];
    } else {
      $this->value; 
    }
  }
  
}
?>