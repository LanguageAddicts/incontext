<?php
/**
* @desc Form hidden field
* @package form
*/
class form_hidden {
  
  private $name;
  private $value;
  private $formHTML;
  
  /**
  * @desc Constructor
  */
  public function __construct($name) {
    $this->name = $name;
  }
  
  /**
  * @desc Add form attributes
  * @param string $value Default field value
  * @param bool $sticky Defaults to TRUE, if false the field will not act as a sticky field
  * @param bool $id Defaults to TRUE, if false id will not be added to input
  */
  public function add_attributes($value='', $sticky=TRUE, $id=TRUE) {
    $this->value = $value;
    // Create form html
    $this->create_html($sticky, $id);
  }
  
  /**
  * @desc Create html for form based on attributes
  * @param bool $sticky Defaults to TRUE, if false the field will not act as a sticky field
  * @param bool $id Defaults to TRUE, if false id will not be added to input
  */
  private function create_html($sticky, $id=TRUE) {
    $this->formHTML = "<input type=\"hidden\" name=\"" . $this->name;
    if($id == TRUE) {
      $this->formHTML .= "\" id=\"" . $this->name;
    }
    if(isset($_POST[$this->name]) && $sticky == TRUE) {
      // $_POST value exists
      $this->formHTML .= "\" value=\"" . $_POST[$this->name];
    } else {
      // New form
      $this->formHTML .= "\" value=\"" . $this->value;
    }
    $this->formHTML .= "\" />\r\n";
  }
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value() {
    if(isset($_POST[$this->name])) {
      return $_POST[$this->name];
    } else {
      return $this->value;  
    }
  }
}
?>