<?php
/**
* @desc Form checkbox group
* @package form
*/
class form_checkboxgroup {
  
  private $fields;
  private $formHTML;
 
  /**
  * Constructor
  * @param array $fields An array containing the checkboxs to create
  */
  public function __construct($fields) {
    if(is_array($fields)) {
      $this->fields = $fields;
      $this->create_html();
    } else {
      die('$fields parameter must be an array');  
    }
  }
    
  /**
  * Create html
  */
  private function create_html() {
    
    $this->formHTML = '';
    
    foreach ($this->fields as $value) {
      $checkbox = new form_checkbox($value);
      $checkbox->add_attributes('checkbox');
      $label = str_replace('_', ' ', $value);
      $this->formHTML .= $checkbox->get_html() . " {$label} " . "<br />\r\n";
    }
  }  
  
  /**
  * @desc Output html
  */
  public function get_html() {
    return $this->formHTML;
  }
  
  /**
  * @desc Get value
  */
  public function get_value($index) {
    if(isset($_POST[$index])) {
      return 1;
    } else {
      return 0;
    }
  }
  
  /**
  * Get values as text
  */
  public function get_values_as_text() {
    
    $html = '';
    
    foreach($this->fields as $value) {
      $label = str_replace('_', ' ', $value);
      if(isset($_POST[$value])) {
        $html .= $label . ": Yes\r\n";
      } else {
        $html .= $label . ": No\r\n";
      }
        
    }    
    return $html;
  }
  
}
?>