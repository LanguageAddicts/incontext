<?php
/**
* @package site
*/
class site_paging {
  
  private $perPage;
  private $totalPages;
  private $currentPage;
  private $url;
  private $links; 
  
  /**
  * @desc Constructor
  * @param integer $currentPage The current page
  * @param integer $perPage Number of records to display per page
  * @param integer $totalPages Total number of pages
  * @param string $url Base url to use for the paging results
  */
  public function __construct($currentPage, $perPage, $totalPages, $url) {
    $this->currentPage = $currentPage;
    $this->perPage = $perPage;
    $this->totalPages = $totalPages;
    $this->url = $url; 
  }
  
  /**
  * @desc Create next link
  */
  private function next_link() {
    if($this->currentPage < $this->totalPages-1) {
      return TRUE;    
    } else {
      return FALSE;
    }    
  } 
  
  /**
  * @desc Create previous link
  */
  private function previous_link() {
    if($this->currentPage > 0) {
      return TRUE;
    } else {
      return FALSE;  
    }
  }
  
  /**
  * @desc Get paging list
  */
  public function get_paging() {
    
    if($this->totalPages != 1) {
      
      $html = '<span class="b">Pages:</span> ';
      
      // Previous Page
      if($this->previous_link() == TRUE) {
        $link = $this->currentPage - 1;
        $html .= "<a href=\"{$this->url}{$link}\">Previous Page</a> | ";  
      }
      
      // Show previous two pages      
      if($this->currentPage >= 2) {
                
        for($i=$this->currentPage-2;$i<$this->currentPage;$i++){
          $displayLink = $i + 1;
          $html .= "<a href=\"{$this->url}{$i}\">{$displayLink}</a>" . ' | ';        
        }      
      } else if($this->currentPage > 0) {
        $html .= "<a href=\"{$this->url}0\">1</a>" . ' | ';
      } else {
        $html .= ' ';
      }
      
      // Current Page
      if($this->currentPage == 0) {
        $displayLink = $this->currentPage + 1;
        $html .= $displayLink . ' ';
      } else if($this->currentPage == $this->totalPages) {
        $displayLink = $this->currentPage + 1;
        $html .= $displayLink;
      } else {
        $displayLink = $this->currentPage + 1;
        $html .= $displayLink . ' | ';
      }
      
      // Show next two pages
      if($this->currentPage + 2 < $this->totalPages) {
        //die('here');
        for($i=$this->currentPage+1;$i<=$this->currentPage + 2;$i++){
          $displayLink = $i + 1;
          $html .= "<a href=\"{$this->url}{$i}\">{$displayLink}</a>" . ' | ';
        }      
      } else if($this->currentPage + 2 == $this->totalPages) {
        //die('here2');
        for($i=$this->currentPage+1;$i<$this->totalPages;$i++){
          $displayLink = $i + 1;
          $html .= "<a href=\"{$this->url}{$i}\">{$displayLink}</a>" . ' | ';
        }              
      } else if($this->currentPage < $this->totalPages) {
        //die('here3');
        for($i=$this->currentPage + 1;$i<$this->totalPages;$i++){
          $displayLink = $i + 1;
          $html .= "<a href=\"{$this->url}{$i}\">{$displayLink}</a>" . ' | ';
        }  
      } else {
        $html .= ' ';  
      }
      
      // Next Page
      if($this->next_link() == TRUE) {
        $link = $this->currentPage + 1;
        $html .= " <a href=\"{$this->url}{$link}\">Next Page</a>";
      }
      
      return $html;
      
    } else {
      return '';  
    }
    
  }
}
?>