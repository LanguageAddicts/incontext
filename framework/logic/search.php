<?php
/**
* @package logic
*/
class logic_search extends logic_core {
      
  /**
  * @desc Constructor
  */
  public function __construct($view) { 
    parent::__construct($view);
    $this->create_html();
  }
  
  /**
  * @desc Create html
  */
  private function create_html() {
    $this->html = '<div id="results_"></div>
    <script type="text/javascript">
    var googleSearchIframeName = "results_";
    var googleSearchFormName = "searchbox_";
    var googleSearchFrameWidth = 600;
    var googleSearchFrameborder = 0;
    var googleSearchDomain = "google.com";
    var googleSearchPath = "/cse";
    </script>
    <script type="text/javascript" src="http://www.google.com/afsonline/show_afs_search.js"></script>';   
  }
}
?>