<?php
/**
* @package logic
*/
class logic_core {
  
  /**
  * @var loader
  */
  protected $loader;
  protected $html;
  protected $view;
  
  /**
  * @desc Constructor
  */
  public function __construct($view) {
    
    $registry = core_registry::getInstance();
    $this->loader = $registry->get('loader');
    $this->view = $view;
  }
  
  /**
  * @desc Entity replacement
  */
  protected function convert_output($value) {
   
    $searchArray = array('&', '£', '’', '–', '  ', '€');
    $replaceArray = array('&amp;', '&pound;', '\'', '-', ' ', '&euro;');
    return str_replace($searchArray, $replaceArray, $value);
  } 
   
  /**
  * @desc Get html
  */
  public function get_html() {
    return $this->html;
  }
  //  
}
?>