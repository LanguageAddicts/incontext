<?php
/**
* @package logic
*/
class logic_contact extends logic_core {
  
  private $form;
    
  /**
  * @desc Constructor
  */
  public function __construct($view) { 
    parent::__construct($view);
    $this->form = new forms_contact($view);
    $this->create_html();
  }
    
  /**
  * @desc Create html
  */
  private function create_html() {
    $this->html .= $this->form->get_form();
  }
  
  /**
  * @desc Get html failed
  */
  public function get_html_failed($form) {
    $html .= $form->get_error_messages();
    $html .= $form->get_form();
    return $html;
  }
  //  
      
}
?>