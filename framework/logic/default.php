<?php
/**
* @package logic
*/
class logic_default extends logic_core {
      
  /**
  * @desc Constructor
  */
  public function __construct($view) { 
    parent::__construct($view);
    $this->create_html();
  }
  
  /**
  * @desc Create html
  */
  private function create_html() {
    $this->html = "";   
  }
}
?>