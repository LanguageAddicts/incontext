<?php
/**
* @package forms
*/
class forms_contact extends forms_core {
  
  private $form;

  private $fFirstName;
  private $fEmail;
  private $fEnquiry;

  private $valFirstName;
  private $valEmail;
  private $valEnquiry;
  
  private $firstNameMin;
  private $firstNameMax;
  private $firstNameError;
  private $firstNameValue;
  
  private $emailError;
  private $emailValue;
  
  private $enquiryMin;
  private $enquiryMax;
  private $enquiryError;
  private $enquiryValue;
 
  private $toName;
  private $toEmail;
  
  private $toName2;
  private $toEmail2;
  
  private $org;
  private $domain;
  private $subject;
  
  /**
  * @desc Constructor
  */
  public function __construct($view) {
    parent::__construct();
    $this->view = 'contact';
    $this->form = new form_form('action.php?view=' . $this->view . '&amp;action=submit', 1);
    $this->load_config_options();
    $this->add_attributes();
    $this->add_validation_rules();
  }
  
  /**
  * @desc Get config options
  */
  private function load_config_options() {
    try {
      $options = $this->loader->read_config_file('settings');
    } catch (Exception $e) {
      die($e->getMessage());
    }

    $this->firstNameMin = intval($options['Contact']['FirstNameMin']);
    $this->firstNameMax = intval($options['Contact']['FirstNameMax']);
    $this->firstNameError = $options['Contact']['FirstNameError'];
    $this->firstNameValue = $options['Contact']['FirstNameValue'];
    
    $this->emailError = $options['Contact']['EmailError'];
    $this->emailValue = $options['Contact']['EmailValue'];
    
    $this->enquiryMin = intval($options['Contact']['EnquiryMin']);
    $this->enquiryMax = intval($options['Contact']['EnquiryMax']);
    $this->enquiryError = $options['Contact']['EnquiryError'];
    $this->enquiryValue = $options['Contact']['EnquiryValue'];
   
    $this->toName = $options['Contact']['ToName'];
    $this->toEmail = $options['Contact']['ToEmail'];
    
    $this->toName2 = $options['Contact']['ToName2'];
    $this->toEmail2 = $options['Contact']['ToEmail2'];
    
    $this->org = $options['Contact']['Org'];
    $this->domain = $options['Contact']['Domain'];
    
    $this->subject = $options['Contact']['Subject'];
  }
  
  /**
  * @desc Add form attributes
  */
  private function add_attributes() {
    $this->fFirstName = new form_textfield('name');
    $this->fFirstName->add_attributes(20, $this->firstNameMax, '', 'width:250px; height:16px;', $this->firstNameValue);     
    $this->fEmail = new form_textfield('email');
    $this->fEmail->add_attributes(20, 255, '', 'width:250px; height:16px;', $this->emailValue);
    $this->fEnquiry = new form_textarea('enquiry');
    $this->fEnquiry->add_attributes(6, 28, '', 'width:250px; height:96px;', $this->enquiryValue);   
  }
  
  /**
  * @desc Add validation rules
  */
  private function add_validation_rules() {
    
    $this->valFirstName = new validation_validator($this->fFirstName->get_value());
    $this->valFirstName->set_attributes(array('rule'=>'name', 'min'=>$this->firstNameMin, 'max'=>$this->firstNameMax), $this->firstNameError);
    
    $this->valEmail = new validation_validator($this->fEmail->get_value());
    $this->valEmail->set_attributes(array('rule'=>'email'), $this->emailError);
    
    $this->valEnquiry = new validation_validator($this->fEnquiry->get_value());
    $this->valEnquiry->set_attributes(array('rule'=>'anything', 'min'=>$this->enquiryMin, 'max'=>$this->enquiryMax), $this->enquiryError);
 
  }
  
  /**
  * @desc Form submitted, process form
  */
  public function process_form() {
    
    // Validate name
    if($this->valFirstName->validate() == FALSE) {
      $this->errors[] = $this->valFirstName->get_error_message();  
    }
    
    // Validate email
    if($this->valEmail->validate() == FALSE) {
      $this->errors[] = $this->valEmail->get_error_message();  
    }
    
    // Validate enquiry
    if($this->valEnquiry->validate() == FALSE) {
      $this->errors[] = $this->valEnquiry->get_error_message();  
    }
         
    
    if(count($this->errors) == 0) {
      $this->send_array();
      return TRUE;      
    } else {
      return FALSE;
    }
  }
  
  /**
  * @desc Send array
  */
  private function send_array() {
    $sendArray = array();
    $sendArray['Name'] = $this->fFirstName->get_value();
    $sendArray['Email'] = $this->fEmail->get_value();
    $sendArray['Enquiry'] = $this->fEnquiry->get_value();
    $sendArray['Posted'] = 'NOW()';
    $sendArray['IP'] = $_SERVER['REMOTE_ADDR'];
    
    $this->create_email($sendArray);
  }
  
  /**
  * @desc Create email
  */
  private function create_email($sendArray) {
     
    $email = new mail_mail();
    $email->add_to($this->toName, $this->toEmail);
    //$email->add_additional_to($this->toName2, $this->toEmail2);
    $email->add_from($sendArray['Name'], $sendArray['Email']);
    $email->add_org_and_domain($this->org, $this->domain);
    $email->add_subject($this->subject);
    $email->create_headers(FALSE); 
        
    $body = "The following enquiry was submitted to the Language Addicts site.\r\n\r\n";
    $body .= 'Name: ' . $sendArray['Name'] . "\r\n";
    $body .= 'Email: ' . $sendArray['Email'] . "\r\n";
    $body .= 'Enquiry: ' . $sendArray['Enquiry'] . "\r\n";
    $body .= 'The above enquiry  was sent on ' . date('d-m-Y') .  ' by the following IP address: ' . $sendArray['IP'];
         
    $email->add_body($body);
    
    $email->send();
    
  }  
       
  /**
  * @desc Build form
  */
  private function build_form() {
    $html .= "<div style=\"padding-bottom:10px;\">\r\n";
    
    $html .= "<div style=\"width:465px;\" class=\"\">Name: {$this->form->req_field()}</div>";
    $html .= "<div style=\"width:465px;\">{$this->fFirstName->get_html()}</div>";
    
    $html .= "<div style=\"width:465px;\" class=\"\">Email: {$this->form->req_field()}</div>";
    $html .= "<div style=\"width:465px;\">{$this->fEmail->get_html()}</div>";
    
    $html .= "<div style=\"width:465px;\" class=\"\">Enquiry: {$this->form->req_field()}</div>";
    $html .= "<div style=\"width:465px;\">{$this->fEnquiry->get_html()}</div>";
    
    $html .= "<div style=\"width:465px; height:5px; line-height:5px;\">&nbsp;</div>";
    $html .= "<div style=\"width:465px;\"><input type=\"image\" name=\"contact\" src=\"legacy_images/buttons/contact_send.gif\"
    style=\"border:0;\" alt=\"Submit\" title=\"Submit Enquiry\" /></div>";
    
    $html .= "<div style=\"width:465px; height:10px;\">&nbsp;</div>";
    
    $html .= "<div style=\"width:465px;\">Fields marked as {$this->form->req_field()} are required.</div>";
    
    $html .= "</div>\r\n";
    return $html;
  }
  
  /**
  * @desc Get form
  */
  public function get_form() {
    $this->html = $this->form->get_html_start_form();
    $this->html .= $this->build_form();
    $this->html .= $this->form->get_html_end_form();
    return $this->html;
  }
    
}
?>