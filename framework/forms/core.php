<?php
/**
* @package forms
*/
class forms_core {
  
  /**
  * @var db_mysql 
  */
  protected $dbconn;
  protected $html;
  protected $errors;
  /**
  * @var loader
  */
  protected $loader;
  /**
  * @var site_session
  */
  protected $session;
  
  /**
  * @desc Constructor
  */
  public function __construct() {
    $registry = core_registry::getInstance();
    $this->dbconn = $registry->get('dbconn');
    $this->loader = $registry->get('loader');
    $this->session = $registry->get('session');
  }
     
  /**
  * @desc Process errors
  */
  public function get_error_messages() {
    $x = 1;
    $text .= "<span style=\"color:red;\">There was a problem with your submission, please review the error(s) below:</span><br />";
    if(is_array($this->errors)) {
      foreach($this->errors as $value) {
        $text .= $x . ': ' . $value . '<br />';
        ++$x;
      }
    } else {
      $text .= '1. Please contact the administrator, there is a problem with the validation for this form.<br />';
    }
    $text .= '<br />';
    return html_elements::create_div('form_errors', $text, '', 'error_header');
  }
    
}
?>