<?php
/**
* @desc Validation rules
* @package validation
*/
class validation_rules {
  
  /**
  * @desc Process validation
  * @param string $value Value
  * @param array $validationArray Validation rules (Rule, Min and Max values)
  */
  protected function validate_field($value, $validationArray) {
    switch($validationArray['rule']) {
            
      case 'name':
        if(validation_alpha::validate_name($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;  
        }
      break;
      
      case 'company':
        if(validation_alphanumeric::validate_company($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;  
        }
      break;
      
      case 'telephone':
        if(validation_numeric::validate_telephone($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;  
        }
      break;
      
      case 'email':
        if(validation_email::validate_email($value) == TRUE) {
          return TRUE;
        } else {
          return FALSE;
        }        
      break;
      
      case 'url':
        if(validation_url::validate_url($value) == TRUE) {
          return TRUE;
        } else {
          return FALSE;
        }        
      break;
      
      case 'address':
        if(validation_alphanumeric::validate_address($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;
        }        
      break;
      
      case 'postcode':
        if(validation_alphanumeric::validate_postcode($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;
        }        
      break;
      
      case 'enquiry':
        if(validation_alphanumeric::validate_enquiry($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;
        }        
      break;
      
      case 'anything':
        if(validation_alphanumeric::validate_anything($value, $validationArray['min'], $validationArray['max']) == TRUE) {
          return TRUE;
        } else {
          return FALSE;
        }        
      break;
        
    }
  }
}
?>
