<?php
/**
* @desc Alpha validation class 
* @package validation
*/
class validation_alpha {
  
  /**
  * @desc Validate name
  * @param string $value String being validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public static function validate_name($value, $min, $max) {
    $pattern = '/^[- \'a-zA-Z]{' . $min . ',' . $max . '}$/i';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }

}  
?>