<?php
/**
* @desc Date validation class
* @package validation
*/
class validation_date {

  /**
  * @desc Validate date
  */
  public static function validate_real_date($day, $month, $year) {
    if(is_numeric($day) && is_numeric($month) && is_numeric($year)) {
      if(strlen($day) == 2 && strlen($month) == 2 && strlen($year) == 4) {
        if(checkdate($month, $day, $year)) {
          return TRUE;
        } else {
          return FALSE;  
        }
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;
    }
  }

  /**
  * @desc Return a valid mysql date, or null
  */
  public static function return_mysql_date($day='', $month='', $year='') {
    if(strlen($day) == 2 && strlen($month) == 2 and strlen($year) == 4) {
      return $year . '-' . $month . '-' . $day;
    } else { 
      return null;
    }
  }
    
  /**
  * @desc Split date into three values
  * @param string $date MySQL Date, passed by reference
  * @return array Array with day, month and year indexes
  */
  public static function return_split_date(&$date) {
    if(strlen($date) == 10) {
      $date = explode('-', $date);
      $date['day'] = $date[2];
      $date['month'] = $date[1];
      $date['year'] = $date[0];
    } else {
      $date['day'] = NULL;      
      $date['month'] = NULL;      
      $date['year'] = NULL;      
    }
    return $date;
  }
  
}
  
?>