<?php
/**
* @desc Validation base class 
* @package validation
*/
class validation_validator extends validation_rules {
  
  private $value;
  private $validationArray;
  private $validationPassed;
  private $error;
  
  /**
  * @desc Construtor
  * @param string $value Value from form
  */
  public function __construct($value) {
    $this->value = $value;
    $this->validationPassed = FALSE;
  }
   
  /**
  * @desc Set attributes
  * @param array $validationArray Validation rules
  * @param string $error Error message for failed validation
  */
  public function set_attributes($validationArray, $error) {
    $this->validationArray = $validationArray;
    $this->error = $error;
  }
  
  /**
  * @desc Get status
  */
  public function validate() {
    $this->validationPassed = $this->validate_field($this->value, $this->validationArray); 
    return $this->validationPassed;  
  }
  
  /**
  * @desc Get error message
  */
  public function get_error_message() {
    return $this->error;
  }
  
}
?>