<?php
/**
* @desc Email validation class
* @package validation
*/
class validation_email {

  /**
  * @desc Validate email
  * @param string $value Email address to be validated
  */
  public static function validate_email($value) {
    $atom = '[-a-z0-9!#$%&\'*+\/\=?^_`{|}~]';       // allowed characters for part before "at" character
    $domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)'; // allowed characters for part after "at" character
    $pattern = '/^' . $atom . '+' .                // One or more atom characters.
    '(\.' . $atom . '+)*'.                        // Followed by zero or more dot separated sets of one or more atom characters.
    '@'.                                          // Followed by an "at" character.
    '(' . $domain . '{1,63}\.)+'.                 // Followed by one or max 63 domain characters (dot separated).
    $domain . '{2,63}'.                           // Must be followed by one set consisting a period of two
    '$/i';                                          // or max 63 domain characters.
    if(preg_match($pattern, $value)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
}  
?>