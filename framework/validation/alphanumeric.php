<?php
/**
* @desc Alphanumeric validation class 
* @package validation
*/
class validation_alphanumeric {
  
  /**
  * @desc Validate company
  * @param string $value String to be validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public static function validate_company($value, $min, $max) {
    $pattern = '/^[- \–\(\)\\\'\\/\.\"\,\’\?\|;£=:@<>!&\+0-9a-zA-Z[:space:]]{' . $min . ',' . $max . '}$/i';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }
  
  /**
  * @desc Validate enquiry
  * @param string $value String to be validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public static function validate_enquiry($value, $min, $max) {
    $pattern = '/^[- \–\(\)\\\'\\/\.\"\,\’\#\?\|;£=:@<>!\$€%&\+0-9a-zA-Z[:space:]]{' . $min . ',' . $max . '}$/i';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }
  
  /**
  * @desc Validate address
  * @param string $value String to be validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public static function validate_address($value, $min, $max) {
    $pattern = '/^[- \–\(\)\\\'\\/\.\"\,\’\|:@<>!\&0-9a-zA-Z[:space:]]{' . $min . ',' . $max . '}$/i';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }
  
  /**
  * @desc Validate postcode
  * @param string $value String to be validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public static function validate_postcode($value, $min, $max) {
    $pattern = '/^[- 0-9a-zA-Z[:space:]]{' . $min . ',' . $max . '}$/i';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }
  
  /**
  * @desc Validate anything
  * @param string $value String to be validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public static function validate_anything($value, $min, $max) {
    $pattern = '/^[- \â€“\(\)\\\'\\/\.\"\,\â€™\#\?\|;Â£=:@<>&\+0-9a-zA-Z[:space:]]{' . $min . ',' . $max . '}$/i';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }

}  
?>