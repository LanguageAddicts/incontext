<?php
/**
* @desc Numeric validation class 
* @package validation
*/
class validation_numeric {

  /**
  * @desc Validate telephone
  * @param string $value String to be validated
  * @param integer $min Minimum character length
  * @param integer $max Maximum character length
  */
  public function validate_telephone($value, $min, $max) {
    $pattern = '/^[- \+\(\)0-9]{' . $min . ',' . $max . '}$/';
    if(preg_match($pattern, $value)) {
      return TRUE;    
    } else {
      return FALSE;
    }  
  }
   
}  
?>