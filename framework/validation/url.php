<?php
/**
* @desc URL validation class
* @package validation
*/
class validation_url {

  /**
  * @desc Validate url
  * @param string $value URL to be validated
  */
  public static function validate_url($value) {
    $pattern = '/^((http:|https:|ftp:)\/\/)?((www.)?([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i';
    if(preg_match($pattern, $value)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
}  
?>