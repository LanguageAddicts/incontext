<?php
/**
* @desc Request class, basic checks
* @package core
*/
class core_requestpost {
  
  /**
  * @desc Check $_POST numeric
  * @return bool
  */
  public static function check_numeric($var) {
    if(isset($_POST[$var])) {
      if(is_numeric($_POST[$var])) {
        return TRUE;  
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;
    }
  }
  
  /**
  * @desc Check $_POST alphanumeric, get vars max at 10 characters
  */
  public static function check_alphanumeric($var) {
    if(isset($_POST[$var])) {
      $pattern = '/^[0-9a-zA-Z]{1,10}$/i';
      if(preg_match($pattern, trim($_POST[$var]))) {
        return TRUE;
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;  
    }    
  }
    
  /**
  * @desc Check $_POST alpha, get vars max at 10 characters
  */
  public static function check_alpha($var) {
    if(isset($_POST[$var])) {
      $pattern = '/^[a-zA-Z]{1,10}$/i';
      if(preg_match($pattern, trim($_POST[$var]))) {
        return TRUE;
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;  
    }    
  }
  
  /**
  * @desc Return $_POST var 
  */
  public static function return_get_var($var) {
    if(isset($_POST[$var])) {
      return trim($_POST[$var]);  
    } else {
      return NULL;  
    }
  }
}
?>