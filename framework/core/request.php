<?php
/**
* The request class provides basic methods for validating $_GET vars and a method to return the values
* @package core
*/
class core_request {
  
  /**
  * Checks that a $_GET value is numeric
  * @param string $var The $_GET var to check
  * @return bool Returns true if the check was sucessful
  */
  public static function check_numeric($var) {
    if(isset($_GET[$var])) {
      if(is_numeric($_GET[$var])) {
        return TRUE;  
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;
    }
  }
  
  /**    
  * Checks that a $_GET value is alphanumeric
  * @param string $var The $_GET var to check
  * @return bool Returns true if the check was sucessful
  */
  public static function check_alphanumeric($var) {
    if(isset($_GET[$var])) {
      $pattern = '/^[0-9a-zA-Z]{1,10}$/i';
      if(preg_match($pattern, trim($_GET[$var]))) {
        return TRUE;
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;  
    }    
  }
    
  /**
  * Checks that a $_GET var is alphabetical
  * @param string $var The $_GET var to check
  * @return bool Returns true if the check was sucessful
  */
  public static function check_alpha($var) {
    if(isset($_GET[$var])) {
      $pattern = '/^[a-zA-Z]{1,10}$/i';
      if(preg_match($pattern, trim($_GET[$var]))) {
        return TRUE;
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;  
    }    
  }
  
  /**
  * Returns the value of the requested $_GET var
  * @param string $var The $_GET var to return
  * @return mixed Returns the requested value
  */
  public static function return_get_var($var) {
    if(isset($_GET[$var])) {
      return trim($_GET[$var]);  
    } else {
      return NULL;  
    }
  }
  
  /**
  * Checks that a $_GET var is a valid order id format
  * @param string $var The $_GET var to check
  * @return bool Returns true of the check was sucessful
  */
  public static function check_orderid($var) {
    if(isset($_GET[$var])) {
      $pattern = '/^[-0-9a-zA-Z]{1,20}$/i';
      if(preg_match($pattern, trim($_GET[$var]))) {
        return TRUE;
      } else {
        return FALSE;  
      }
    } else {
      return FALSE;  
    }    
  }
}
?>