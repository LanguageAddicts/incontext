<?php
/**
* The registry objects manages the storage of global objects that are to be used throughout the application.
* @package core
*/
class core_registry {
  
  /**
  * @var References to the objects are stored within $objects
  */
  private $objects;
  static private $thisInstance = NULL;
  
  /**
  * Registry class is a singleton, the getInstance() method insures that only one object is instantiated
  * @return reference Returns either a new instance of the class or a reference
  */
  static public function getInstance() {
    if(self::$thisInstance == null) {
      self::$thisInstance = new core_registry();
    }
    return self::$thisInstance;
  }  
  
  /**
  * Stores object for later use, if the object already exists within the registry the call is ignored
  * @param string $label Label for Object, Index
  * @param object $object The object to be stored.
  * @return void
  */
  public function store($label, $object) {
    if(!$this->exists($label)) {
      $this->objects[$label] = $object;
    }
  }
  
  /**
  * Removes an object from the registry. If the object does not exists in the registry the call is ignored.
  * @param string $label Object to be removed from registry
  * @return void
  */
  public function remove($label) {
    if($this->exists($label)) {
      unset($this->objects[$label]);
    }
  }
  
  /**
  * Checks to see if the object (label) already exists wihin the registry
  * @param string $label The object to be checked
  * @returns bool Returns true or false depending on whether the key was found
  */
  private function exists($label) {
    if(isset($this->objects[$label])) {
      return TRUE;
    } else {
      return FALSE;
    } 
  }
  
  /**
  * Returns a reference to the object, if the object is not found within the registry the method returns false
  * @param string $label Object being requested
  * @return Reference Returns a refernece to the object
  */
  public function get($label) {
    if(isset($this->objects[$label])) {
      return $this->objects[$label];
    } else {
      return FALSE;
    }
  } 
}
?>