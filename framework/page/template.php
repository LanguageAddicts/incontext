<?php

/**

* @desc Core template class, builds up page

* @package page

*/

class page_template extends page_core {

  

  private $title;

  private $description;

  private $keywords;
  
  private $fbimage;

  private $language;

  private $robots;

  private $author;

  private $copyright;

  private $contentFromFile;

  

  /**

  * @desc Constructor

  */

  public function __construct($view) {

    $this->view = $view;

    parent::__construct($view);

    $this->get_meta_details();

    $this->get_page_content();

    $this->create_html_header();

  }

  

  /**

  * @desc Get meta details

  */

  private function get_meta_details() {

    $meta = $this->loader->read_config_file('meta');

    

    $searchArray = array(' and ', '<colon>', '<ob>', '<cb>');

    $replaceArray = array(' &amp; ', ':', '(', ')');

    

    $this->title = str_replace($searchArray, $replaceArray, $meta[$this->view]['title']);

    $this->description = str_replace($searchArray, $replaceArray, $meta[$this->view]['description']);

    $this->keywords = str_replace($searchArray, $replaceArray, $meta[$this->view]['keywords']);
    
    $this->fbimage = str_replace($searchArray, $replaceArray, $meta[$this->view]['fbimage']);

  }

  

  /**

  * @desc Get Menu

  */

  private function get_menu() {

    $data = $this->loader->read_config_file('menu');

    $html = '';

    foreach($data as $k=>$v) {

     $title = $k;

     $mainLink = strtolower($k);

     $image = $v['Image']; 

     $link1 = strtolower($v['Link1']); 

     $link1Name = $v['Link1Text']; 

     $link1Image = $v['Link1Image']; 

     $link2 = strtolower($v['Link2']); 

     $link2Name = $v['Link2Text']; 

     $link2Image = $v['Link2Image']; 

     $link3 = strtolower($v['Link3']); 

     $link3Name = $v['Link3Text']; 

     $link3Image = $v['Link3Image'];

     

     $html .= "<div class=\"menu_section\">\r\n";

      $html .= "<div class=\"menu_section_header\">\r\n";

      $html .= "<a href=\"{$mainLink}.htm\" title=\"{$title}\">\r\n";

      $html .= "<img src=\"legacy_images/buttons/{$image}\" alt=\"{$title} image\" title=\"{$title}\" width=\"176\" height=\"38\" style=\"display: block;\" />\r\n";

      $html .= "</a>\r\n";

      $html .= "</div>\r\n";

      $html .= "<div class=\"menu_section_content\">\r\n";

        $html .= "<div class=\"menu_section_icon\">\r\n";

          $html .= "<a href=\"{$mainLink}_{$link1}.htm\" title=\"{$title} {$link1Name}\">\r\n";

          $html .= "<img src=\"legacy_images/icons/{$link1Image}\" alt=\"{$title} {$link1Name} icon\" title=\"{$title} {$link1Name}\" width=\"21\" height=\"21\" style=\"display: block;\" />\r\n";

          $html .= "</a>\r\n";

        $html .= "</div>\r\n";

        $html .= "<div class=\"menu_section_text\">\r\n";

          $html .= "<a href=\"{$mainLink}_{$link1}.htm\" title=\"{$title} {$link1Name}\">\r\n";

          $html .= $link1Name;

          $html .= "</a>\r\n";

        $html .= "</div>\r\n";

        $html .= "<div class=\"menu_section_icon\">\r\n";

          $html .= "<a href=\"{$mainLink}_{$link2}.htm\" title=\"{$title} {$link2Name}\">\r\n";

          $html .= "<img src=\"legacy_images/icons/{$link2Image}\" alt=\"{$title} {$link2Name} icon\" title=\"{$title} {$link2Name}\" width=\"21\" height=\"21\" style=\"display: block;\" />\r\n";

          $html .= "</a>\r\n";

        $html .= "</div>\r\n";

        $html .= "<div class=\"menu_section_text\">\r\n";

          $html .= "<a href=\"{$mainLink}_{$link2}.htm\" title=\"{$title} {$link2Name}\">\r\n";

          $html .= $link2Name;

          $html .= "</a>\r\n";

        $html .= "</div>\r\n";

        $html .= "<div class=\"menu_section_icon\">\r\n";

          $html .= "<a href=\"{$mainLink}_{$link3}.htm\" title=\"{$title} {$link3Name}\">\r\n";

          $html .= "<img src=\"legacy_images/icons/{$link3Image}\" alt=\"{$title} {$link3Name} icon\" title=\"{$title} {$link3Name}\" width=\"21\" height=\"21\" style=\"display: block;\" />\r\n";

          $html .= "</a>\r\n";

        $html .= "</div>\r\n";

        $html .= "<div class=\"menu_section_text\">\r\n";

          $html .= "<a href=\"{$mainLink}_{$link3}.htm\" title=\"{$title} {$link3Name}\">\r\n";

          $html .= $link3Name;

          $html .= "</a>\r\n";

        $html .= "</div>\r\n"; 

      $html .= "</div>\r\n"; 

     $html .= "</div>\r\n"; 

    }

    

    return $html;

    

  }

  

  /**

  * @desc Get Voucher CTA details

  */

  private function get_voucher_details($filename, $cta) {

    $data = $this->loader->read_cta_config_file($filename);

    

    $searchArray = array(' and ', '<colon>', '<ob>', '<cb>');

    $replaceArray = array(' &amp; ', ':', '(', ')');

    

    $line1 = trim($data[$cta]['Line1']);

    $line2 = trim($data[$cta]['Line2']);

    $line3 = trim($data[$cta]['Line3']);

    $link = trim($data[$cta]['Link']);

    

    $html = "<div class=\"voucher\">\r\n";

      $html .= "<div class=\"voucher_line1\">\r\n";

      $html .= "<a href=\"{$link}.htm\" onclick=\"javascript:urchinTracker ('Discounts_CTA_Clicked')\">\r\n";

      $html .= $line1;

      $html .= "</a>\r\n";

      $html .= "</div>\r\n";

      $html .= "<div class=\"voucher_line2\">\r\n";

      $html .= "<a href=\"{$link}.htm\" onclick=\"javascript:urchinTracker ('Discounts_CTA_Clicked')\">\r\n";

      $html .= $line2;

      $html .= "</a>\r\n";

      $html .= "</div>\r\n";

      $html .= "<div class=\"voucher_line3\">\r\n";

      $html .= "<a href=\"{$link}.htm\" onclick=\"javascript:urchinTracker ('Discounts_CTA_Clicked')\">\r\n";

      $html .= $line3;

      $html .= "</a>\r\n";

      $html .= "</div>\r\n";

    $html .= "</div>\r\n";

    

    return $html;

    

  }

  

  /**

  * @desc Get Featured CTA details

  */

  private function get_featured_details($filename, $cta) {

    $data = $this->loader->read_cta_config_file($filename);

    

    $searchArray = array(' and ', '<colon>', '<ob>', '<cb>');

    $replaceArray = array(' &amp; ', ':', '(', ')');

    

    $type = trim($data[$cta]['Type']);

    $image = trim($data[$cta]['Image']);

    $title = trim($data[$cta]['Title']);

    $desc = trim($data[$cta]['Desc']);

    $button = trim($data[$cta]['Button']);

    $link = trim($data[$cta]['Link']);

    

    

    $html = "<div class=\"featured_cta\">\r\n";

      $html .= "<div class=\"featured_cta_header\">\r\n";

      $html .= $type;

      $html .= "</div>\r\n";

      $html .= "<div class=\"featured_cta_image\">\r\n";

      $html .= "<a href=\"{$link}.htm\" title=\"{$title} - {$button}\" onclick=\"javascript:urchinTracker ('CTA_Album_Clicked')\">\r\n";

      $html .= "<img src=\"legacy_images/ctas/{$image}\" alt=\"{$type} image\" title=\"{$title}\" width=\"210\" height=\"93\" style=\"display: block;\" onclick=\"javascript:urchinTracker ('CTA_Album_Clicked')\" />\r\n";

      $html .= "<a href=\"{$link}.htm\" title=\"{$title} - {$button}\" onclick=\"javascript:urchinTracker ('CTA_Album_Clicked')\">\r\n";

      $html .= "</div>\r\n";

      $html .= "<div class=\"featured_cta_content\">\r\n";

        $html .= "<div class=\"featured_cta_text\">\r\n";

        $html .= "<span class=\"b\">{$title}</span><br />\r\n";

        $html .= $desc;

        $html .= "</div>\r\n";

        $html .= "<div class=\"featured_cta_button\">\r\n";

        $html .= "<div style=\"float: right;\">\r\n";

        $html .= "<a href=\"{$link}.htm\" title=\"{$title} - {$button}\" onclick=\"javascript:urchinTracker ('CTA_Podcast_Clicked')\">\r\n";

        $html .= $button;

        $html .= "</a>\r\n";

        $html .= "</div>\r\n";

        $html .= "<div style=\"float: right; padding-right: 7px;\">\r\n";

        $html .= "<a href=\"{$link}.htm\" onclick=\"javascript:urchinTracker ('CTA_Podcast_Clicked')\">\r\n";

        $html .= "<img src=\"legacy_images/buttons/cta_button.gif\" alt=\"{$type} button\" title=\"{$title} - {$button}\" width=\"19\" height=\"19\" style=\"display: block;\" />\r\n";

        $html .= "</a>\r\n";

        $html .= "</div>\r\n"; 

        $html .= "</div>\r\n";

      $html .= "</div>\r\n";

    $html .= "</div>\r\n";

    

    return $html;

    

  }

   /**

  * @desc Get Featured CTA details

  */

  private function get_affiliate_details($filename) {

    list($link,$partner,$img) = $this->get_specific_affiliate_data($filename);

    $html = "<div class=\"affiliate_banner\">\r\n";       

        $html .= "<a href={$link}";

        $html.="><img src={$img}";

        $html.="onclick=\"javascript:urchinTracker ('{$partner}_Affiliate_Clicked')\">\r\n";

        $html .= "</a>\r\n";

        $html .= "</div>\r\n";



    

    return $html;

    

  }

  

  private function get_specific_affiliate_data($filename)

  {

    $data = $this->loader->read_cta_config_file($filename);

    $searchArray = array(' and ', '<colon>', '<ob>', '<cb>');

    $replaceArray = array(' &amp; ', ':', '(', ')');

    $schemes = count($data);

    if ($schemes > 1)

    {// more than one affiliate scheme in the file, pick a random one

    $schemeno = rand(1, $schemes) -1;

    }

    else

    {//only one scheme so go with that 

    $schemeno=0;

    }

    //get array of keys from $data array

    $keys = array_keys($data);

    //find the key name of the scheme number we chose

    $index=$keys[$schemeno];

    //echo $data[$index];

    $link = trim($data[$index]['href']);

    $partner = trim($data[$index]['partner']);

    $img = trim($data[$index]['img']);

    

  return  array($link,$partner,$img);

  }

  /**

  * @desc Get page content from file

  */

  private function get_page_content() {

    $this->contentFromFile = $this->loader->read_content_file($this->view);    

  }

     

  /**

  * @desc Create html header

  */

  private function create_html_header() {

    $html = $this->create_html_header_start($this->type);

    /*compatibility view for IE8 */

    $html .= "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EmulateIE7\" />";

    $html .= "<title>" . $this->title . "</title>\r\n";

    $html .= "<meta name=\"DESCRIPTION\" content=\"" . $this->description . "\" />\r\n";

    $html .= "<meta name=\"KEYWORDS\" content=\"" . $this->keywords . "\" />\r\n";

    $html .= "<meta name=\"DC.Language\" content=\"English\" />\r\n";

    $html .= "<meta name=\"ROBOTS\" content=\"ALL\" />\r\n";

    $html .= "<meta name=\"AUTHOR\" content=\"Language Addicts\" />\r\n";
    $html .="<meta property=\"og:site_name\" content=\"Language Addicts Ltd\" />\r\n";
    $html .="<meta property=\"og:image\" content=\""  . $this->fbimage . "\" />\r\n";

    $html .= "<meta name=\"COPYRIGHT\" content=\"Language Addicts 2007 - " . date('Y') . "\"/>\r\n";
    //$html .= "<meta name=\"description\" content=\"State the obvious and win \$200 for a ten second video\" />\r\n";
    //$html .="<script type=\"text/javascript\" src=\"http://w.sharethis.com/button/buttons.js\"></script>";
    //$html .="<script type=\"text/javascript\" src=\"http://s.sharethis.com/loader.js\"></script>";
    
    $html .="<script type=\"text/javascript\">var switchTo5x=true;</script>";
    $html .="<script type=\"text/javascript\" src=\"http://w.sharethis.com/button/buttons.js\"></script>";
    $html .="<script type=\"text/javascript\">stLight.options({publisher: \"ur-c21c6480-c654-cddc-6ffc-4be3f4a1f48b\"});</script>";

    $html .= "<script type=\"text/javascript\" src=\"scripts/scripts.js\"></script>\r\n";

    $html .= $this->create_html_header_end();

    $html .= "<div id=\"container\">\r\n";

    $html .= "<div id=\"shell\">\r\n";

    $html .= "<div id=\"shell_inner\">\r\n";

    return $html;

  }

  

  /**

  * @desc Header

  */

  private function header_block() {

    $html = "<div id=\"top_bar\">\r\n";

      $html .= "<div id=\"auxiliary\">\r\n";

        $html .= "<div id=\"contact_icon\">\r\n";

        $html .= "<a href=\"contact.htm\">Contact</a>\r\n";

        $html .= "</div>\r\n";

        /*$html .= "<div id=\"sitemap_icon\">\r\n";

        $html .= "<a href=\"sitemap.htm\">Sitemap</a>\r\n";

        $html .= "</div>\r\n";*/

        $html .= "<div id=\"search_box\">\r\n";

        $html .= "<form action=\"" . $this->loader->get_domain() . "search.htm\" id=\"searchbox_015617931419115741286:dgwd17gqaos\" >\r\n";

        $html .= "<div>\r\n";

        $html .= "<input type=\"hidden\" name=\"cx\" value=\"015617931419115741286:dgwd17gqaos\" />\r\n";

        $html .= "<input type=\"hidden\" name=\"cof\" value=\"FORID:11\" />\r\n";

        $html .= "<input type=\"text\" name=\"q\" size=\"28\" class=\"search_text\" />\r\n";

        $html .= "<input type=\"hidden\" name=\"sa\" />\r\n";

        $html .= "<input type=\"image\" name=\"search_button\" src=\"legacy_images/buttons/search.gif\" style=\"border:0; padding: 0;\" alt=\"Search\" title=\"Search Site\" />\r\n";

        $html .= "</div>\r\n";

        $html .= "</form>\r\n";

        $html .= '<script type="text/javascript" src="http://google.com/coop/cse/brand?form=searchbox_015617931419115741286%3Adgwd17gqaos"></script>' . "\r\n";

        $html .= "</div>\r\n";

      $html .= "</div>\r\n";

    $html .= "</div>\r\n";

    

    $html .= "<div id=\"header_container\">\r\n";
    

      $html .= "<div id=\"header_logo\">\r\n";

       //header link to home 

      $html .= "<a href=\"/\" title=\"Home\">"; 

      $html .= "<div id=\"logo_bubble\" title=\"Language Addicts Logo\"></div>\r\n";
      

      $html .= "<div id=\"logo_type\" title=\"Language Addicts\"></div>\r\n";

       //header link to home  

      $html .= "<a href=\"/\" title=\"Home\">";    

      //$html .= "<div id=\"logo_type\" title=\"Language Addicts\"></div>\r\n";   

      //$html .= "<div id=\"logo_bubble\" title=\"Language Addicts Logo\"></div>\r\n";   
      
      $html .= "</div>\r\n";
       
      

      $html .= "<div id=\"top_menu\">\r\n";   

        $html .= "<ul>\r\n";
        
          //$html .= "<li><a href=\"comp.htm\" title=\"Competition\"><span>COMPETITION</span></a></li>\r\n"; 

          $html .= "<li><a href=\"/\" title=\"Home\"><span>HOME</span></a></li>\r\n";



          $html .= "<li><a href=\"how.htm\" title=\"How It Works\"><span>DEMO</span></a></li>\r\n";

          /*$html .= "<li><a href=\"resources.htm\" title=\"Resources\"><span>RESOURCES</span></a></li>\r\n";*/

          $html .= "<li><a href=\"contact.htm\" title=\"Contact Us\"><span>CONTACT</span></a></li>\r\n";

          $html .= "<li><a href=\"http://www.payloadz.com/go/view_cart.asp?id_user=58727\" target=\"paypal\" title=\"View Cart\"><span>CART</span></a></li>\r\n";

        $html .= "</ul>\r\n";  

      $html .= "</div>\r\n";  

    $html .= "</div>\r\n";
     switch ($_SERVER['PHP_SELF']){
    	case "/comp.htm":
      case "/comp_new.htm":
      case "/visual_cue.htm":
      case "/comp_rules.htm":
      case "/comp_submit.htm":
      case "/comp_completion.htm":
      case "/winners.htm":
      case "/winners_2.htm":
      case "/winners_3.htm":
      case "/contributor.htm":
		  break;
      default:
    $html .= "<div id=\"banner\" title=\"Familiarize, Recognize, Memorize\">\r\n";

      /* header link to home*/  

      $html .= "<a href=\"/\" title=\"Home\">";   

      $html .= "<div id=\"banner_image\">\r\n";  

      $html .= "</div>\r\n";  

    $html .= "</div>\r\n";  
    }
    return $html;

  }

  

  // set the featured data file name to use

  private function get_featured_filename($languages)

  {

  $featured = 'featured-data';

  $NumberOfLanguages = count($languages);

  //loop through the array of languages to see if this page is language specific

       for ($i=0; $i<=$NumberOfLanguages; $i++)

      {

      //does the language name appear in the page name?

      $pos =strpos($_SERVER['PHP_SELF'],$languages[$i]);

        //yes it does!

         if ($pos == true)

        {

        //this is a language-specific page

        //set the correct ini files to use if they exist

        if ($this->loader->check_cta_file_exists("$languages[$i]-$featured.ini")==true) 

        {

        $featured = $languages[$i]."-".'featured-data';

        }

        }

      }

  return $featured;

  }

  

  //set the affiliate CTA file name to use

  private function get_affiliate_filename($languages)

  {

  $affiliate = 'affiliate-data';

  $NumberOfLanguages = count($languages);

    //loop through the array of languages to see if this page is language specific

       for ($i=0; $i<=$NumberOfLanguages; $i++)

      {

      //does the language name appear in the page name?

      $pos =strpos($_SERVER['PHP_SELF'],$languages[$i]);

        //yes it does!

         if ($pos == true)

        {

        //this is a language-specific page

        //set the correct ini files to use if they exist

        if ($this->loader->check_cta_file_exists("$languages[$i]-$affiliate.ini")==true) 

        {

        $affiliate = $languages[$i]."-".$affiliate;

        }

        }

      }

  return $affiliate;

  }

  

  

  /**

  * @desc Content 

  */

  private function content_block() 

  {    

  //when new langauges are added, add them to this array

  $languages=array("german","greek");

  $featured=$this->get_featured_filename($languages);

  $affiliate=$this->get_affiliate_filename($languages);


      /* set sidebar type according to page type*/
      
      switch ($_SERVER['PHP_SELF']){
    	case "/search.htm":
    	case "/comp.htm":
	    case "/comp_rules.htm":
      case "/visual_cue.htm":
      case "/comp_submit.htm":
      case "/comp_completion.htm":
      case "/comp_new.htm":
      case "/winners.htm":
      case "/winners_2.htm":
      case "/winners_3.htm":
      case "/contributor.htm":
      $sidebar = "notinuse";
      break;
      case "/greek_pronunciation.htm":
      $sidebar= "adverts";
      break;
      default:
      $sidebar="standard";
		  break;
      }      
      
      //left hand sidebar
      switch ($sidebar){
       case "notinuse":
      $html = "<div id=\"content_container_wide\">\r\n";
      $html .= "<div id=\"banner_shadow\"></div>\r\n";
      $html .= "<div id=\"content_main_wide\">\r\n";
      break;
      case "standard":
      case "adverts":  
      $html = "<div id=\"content_container\">\r\n";
      $html .= "<div id=\"banner_shadow\"></div>\r\n";
      $html .= "<div id=\"side_menu\">\r\n";
      $html .= $this->get_menu();           
      $html .= "</div>\r\n";
      $html .= "<div id=\"content_main\">\r\n";
      break;
      
     
      }
     
     //contents of content.inc file
      $html .= $this->contentFromFile;
      $html .= "\r\n";
      $html .= $this->get_content();        
      $html .= "</div>\r\n";


      //right hand sidebar
      switch ($sidebar){
      case "standard":
      $html .= "<div id=\"right_column\">\r\n";
      $html .= $this->get_voucher_details('discount-data', 'Discount');           
      $html .= $this->get_featured_details($featured, 'LatestAlbum');           
      $html .= $this->get_featured_details($featured, 'LatestPodcast');
      //add affiliate data if we have some for this page 
      if ($this->loader->check_cta_file_exists("$affiliate.ini")==true) 
      {$html .= $this->get_affiliate_details($affiliate);}  
      $html .= "</div>\r\n";
      $html .= "</div>\r\n";
      break;

      case "adverts":
       $html .= "<div id=\"right_column\">\r\n";
      $html .= "<iframe src=\"http://rcm-uk.amazon.co.uk/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=wwwlanguagead-21&o=2&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=0340860219\" style=\"width:120px;height:240px;\" scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\"></iframe>";           
      $html .="<br/>";
      $html .="<iframe src=\"http://rcm-uk.amazon.co.uk/e/cm?lt1=_blank&bc1=FFFFFF&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=wwwlanguagead-21&o=2&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=0864426836\" style=\"width:120px;height:240px;\" scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\"></iframe>";
      $html .= "</div>\r\n";
      break;
      case "notinuse":
      break;
      } 

    //close final div   

    $html .= "</div>\r\n";

    return $html;

  }

  

  /**

  * @desc Footer

  */

  private function footer_block() {

    $html = "<div id=\"footer_container\">\r\n";

      $html .= "<div id=\"footer_content\">\r\n";

        $html .= "<ul>\r\n";

          $html .= "<li><a href=\"/\" title=\"Home\">Home</a></li>\r\n";

          /*$html .= "<li><a href=\"help.htm\" title=\"Help\">Help</a></li>\r\n";

          $html .= "<li><a href=\"faq.htm\" title=\"FAQ\">FAQ</a></li>\r\n";*/

          $html .= "<li><a href=\"terms.htm\" title=\"Terms of Use\">Terms of Use</a></li>\r\n";

          $html .= "<li><a href=\"privacy.htm\" title=\"Privacy Policy\">Privacy Policy</a></li>\r\n";

        $html .= "</ul>\r\n";

      $html .= "</div>\r\n";

      $html .= "<div id=\"footer_copyright\">\r\n";

        $html .= "Copyright &copy; Language Addicts Ltd 2007 - " . date('Y') . "\r\n";

      $html .= "</div>\r\n";

    $html .= "</div>\r\n";

    return $html;

  }

        



            

  /**

  * @desc Create finished page

  */

  public function create_page() {

    $this->page = $this->create_html_header();

    $this->page .= $this->header_block();

    $this->page .= $this->content_block();

    $this->page .= $this->footer_block();

    $this->page .= $this->create_html_footer();

    return $this->page;

  }



}



?>