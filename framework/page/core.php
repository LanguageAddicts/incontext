<?php
/**
* @desc Core template class, builds up page
* @package page
*/
class page_core {
  
  /**
  * @var loader  
  */
  protected $loader;
  /**
  * @var db_mysql
  */
  protected $dbconn;
  /**
  * @var site_shop
  */
  //protected $shop;
  protected $page;
  protected $content;
  protected $view;

  /**
  * @desc Constructor, set html meta tags
  * @param integer $view Current view
  */
  public function __construct($view) {
    $registry = core_registry::getInstance();
    $this->loader = $registry->get('loader');
    $this->dbconn = $registry->get('dbconn');
  }
   
  /**
  * @desc get style sheet link
  */
  protected function get_style_sheet() {
    return "<link href=\"legacy_styles/legacy_styles.css\" rel=\"stylesheet\" type=\"text/css\" />";
  }
  
  /**
  * @desc Create html header, start
  */
  protected function create_html_header_start() {
    //$html = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"";
    //$html .= " \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n";
    //$html .= "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
    //$html .= "<head>\r\n";
    $html = "<!DOCTYPE html>\r\n";
    $html .= "<head>\r\n";
    return $html;      
  }
  
  /**
  * @desc Create html header, end
  */
  protected function create_html_header_end() {
    $html = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n";
    $html .= $this->get_style_sheet();
    $html .= "\r\n</head>\r\n<body>\r\n";
    return $html;      
  }
    
  /**
  * @desc Create html footer
  */
  protected function create_html_footer() {
    $html = "</div>\r\n</div>\r\n</div>\r\n";
    $html .= $this->get_analytics_block();
    $html .= "</body>\r\n</html>";
    return $html;
  }
   
/**
  * @desc Create analytics section
  */
  protected function get_analytics_block() {
   $html .= "<!-- Google analytics code tick-->\r\n";
    $html .= "<script src=\"http://www.google-analytics.com/urchin.js\" type=\"text/javascript\"></script>\r\n";
    $html .= "<script type=\"text/javascript\">\r\n";
    $html .= "_uacct = \"UA-1960131-2\";\r\n";
    $html .= "urchinTracker();\r\n";
    $html .= "</script>\r\n";
 return $html;
}
  /**
  * @desc Create content
  */
  public function add_content($content) {  
    $this->content = $content;
  } 
  
  /**
  * @desc Get content      
  */
  protected function get_content() {
    return $this->content;  
  }
   
  
  /**
  * @desc Get page
  */
  public function get_page() {
    return $this->page;
  }

}

?>