<?php
/**
* @desc Side Menu  
* @package page
*/
class page_menu {
  
  private $activeView;
  
  private $menu;
  
  /**
  * @desc Constructor
  */
  public function __construct($view) {
    $registry = core_registry::getInstance();
    $this->activeView = $view;
    $this->build_menu();
  }
  
  /**
  * @desc Build menu
  */
  private function build_menu() {
    
    $this->menu .= "\r\n";

    $this->menu .= "\r\n";
    
  }
  
  /**
  * @desc Get menu
  */
  public function get_menu() {
    return $this->menu;  
  }
  
}
?>
