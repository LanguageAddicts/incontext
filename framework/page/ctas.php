<?php
/**
* @desc CTA Column  
* @package page
*/
class page_ctas {
  
  private $activeView;
  
  private $ctas;
  
  /**
  * @desc Constructor
  */
  public function __construct($view) {
    $registry = core_registry::getInstance();
    $this->activeView = $view;
    $this->build_column();
  }
  
  /**
  * @desc Get Voucher CTA details
  */
  private function get_voucher_details($filename, $cta) {
    $data = $this->loader->read_cta_config_file($filename);
    
    $searchArray = array(' and ', '<colon>', '<ob>', '<cb>');
    $replaceArray = array(' &amp; ', ':', '(', ')');
    
    $line1 = $data[$cta]['Line1'];
    $line2 = $data[$cta]['Line2'];
    $line3 = $data[$cta]['Line3'];
    $link = $data[$cta]['Link'];
    
    $html = "<div class=\"voucher\">\r\n";
      $html .= "<div class=\"voucher_line1\">\r\n";
      $html .= "<a href=\"index.php?view={$link}\">\r\n";
      $html .= $line1;
      $html .= "</a>\r\n";
      $html .= "</div>\r\n";
    $html .= "</div>\r\n";
    
    return $html;
    
  }
  
  /**
  * @desc Build column
  */
  private function build_column() {
    
    $this->ctas = $this->get_voucher_details('discount-data', 'GreekDiscount');

    $this->ctas .= "\r\n";
    
  }
  
  /**
  * @desc Get ctas
  */
  public function get_ctas() {
    return $this->ctas;  
  }
  
}
?>
