<?php

/**

* @desc loads classes and sets directory paths

* @package loader

*/

class loader {

  

  private $webRootPath;

  private $frameworkPath;

  private $domain;

  private $configPath;

  private $ctaPath;

  private $contentPath;

  private $adminPath;

  

  /**

  * @desc Initialise class variables

  */

  public function __construct() {

    


	$path = pathinfo($_SERVER['SCRIPT_FILENAME']);
	$this->webRootPath = $path['dirname'];
	$this->domain =	"http://" . $_SERVER['HTTP_HOST'];    

 



    $this->configPath = '/config/';
    
    $this->ctaPath = '/ctas/';

    $this->contentPath = '/content/';

    $this->frameworkPath = '/framework/';

    $this->adminPath = 'admin/';     

  }

  

  /**

  * @desc Convert classname

  */

  private function convert_classname($class) {

    return $this->webRootPath . $this->frameworkPath . strtolower(str_replace('_', '/', $class)) . '.php';            

  }

  

  /**

  * @desc Load class files

  * @param array $array Array of classnames to include, throws an exception on failure

  */

  public function load_classes($array) {

    foreach ($array as $class) {

      if(file_exists($this->convert_classname($class))) {

        require($this->convert_classname($class));

      } else {

        throw new Exception('Unable to include class: ' . $this->convert_classname($class)); 

      }

    }    

  }

  

  /**

  * @desc Load class file

  * @param string $class Ingle class file to include, throws an exception on failure

  */

  public function load_class($class) {

    if(file_exists($this->convert_classname($class))) {

      require($this->convert_classname($class));

    } else {

      throw new Exception('Unable to include class:- ' . $this->convert_classname($class));

    }

  }

  

  /**

  * @desc Reads a .ini file and returns a settings array, throws an Exception on failure

  * @param string $config The config file to load

  */

  public function read_config_file($config) {

    if(file_exists($this->webRootPath . $this->configPath . $config . '.ini')) {

      return parse_ini_file($this->webRootPath . $this->configPath . $config . '.ini', TRUE);    

    } else {

      throw new Exception('Unable to read configuration file: ' . $this->webRootPath . $this->configPath . $config . '.ini'); 

    }    

  }

  /**

  * @desc checks existence of file

  * @param string $config The config file to load

  */

  public function check_cta_file_exists($config) {

  //echo $this->webRootPath . $this->ctaPath . $config;

    if(file_exists($this->webRootPath . $this->ctaPath . $config)) {

      return true;    

    } else {

      return false; 

    }    

  }

  /**

  * @desc Reads a .ini file and returns a settings array, throws an Exception on failure

  * @param string $cta The config file to load

  */

  public function read_cta_config_file($ctas) {

    if(file_exists($this->webRootPath . $this->ctaPath . $ctas . '.ini')) {

      return parse_ini_file($this->webRootPath . $this->ctaPath . $ctas . '.ini', TRUE);    

    } else {

      throw new Exception('Unable to read CTA configuration file: ' . $this->webRootPath . $this->ctaPath . $ctas . '.ini'); 

    }    

  }

  

  /**

  * @desc Reads an .inc file and returns the content to a variable, throws an Exception on failure

  * @param string $page The config file to load

  */

  public function read_content_file($page) {

    if(file_exists($this->webRootPath . $this->contentPath . $page . '.inc')) {

      return file_get_contents($this->webRootPath . $this->contentPath . $page . '.inc');    

    } else {

      die('Unable to read content file: ' . $this->webRootPath . $this->contentPath . $page . '.inc'); 

    }    

  }

  

  /**

  * @desc Redirect to page

  */

  public function redirect($page) {

    header('Location: ' . $this->domain . $page);

    exit(); 

  }



  /**

  * @desc Redirect to admin page

  */

  public function redirect_admin($page) {

    header('Location: ' . $this->domain . $this->adminPath . $page);

    exit(); 

  }

  

  /**

  * @desc Redirect external

  */

  public function redirect_ext($page) {

    header('Location: ' . $page);  

    exit();

  }

   

  /**

  * @desc Get webroot path

  */

  public function get_webroot_path() {

    return $this->webRootPath;  

  }

  

  /**

  * @desc Get framework path

  */

  public function get_framework_path() {

    return $this->frameworkPath;  

  }

  

  /**

  * @desc Get domain

  */

  public function get_domain() {

    return $this->domain;  

  } 

   

  /**

  * @desc Get config path

  */

  public function get_config_path() {

    return $this->configPath;

  }

}



// Autoloader



$class = '';



function __autoload($class) {

  $loader = new loader();

  try {

    $loader->load_class($class);

  } catch(Exception $e) {   

    die($e->getMessage());

  }

}



// Pretty print_r

function print_var($var) {

  echo "\r\n<pre>\r\n";

  print_r($var);

  echo "</pre>\r\n";

}



?>