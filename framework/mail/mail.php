<?php
/**
* @desc Mail class
* @package mail
*/
class mail_mail {
  
  private $headers;
  private $subject;
  private $body;
  private $toName;
  private $toEmail;  
  private $additionalTo;
  private $fromName;
  private $fromEmail;
  private $org;
  private $domain;
  
  /**
  * @desc Constructor
  */
  public function construct() {
    $this->additionalTo = '';  
  }
  
  /**
  * @desc Validate email
  */
  private function validate_email($email) {
    if(validation_email::validate_email($email) == TRUE) {
      return $email;
    } else {
      // Kill process, needs to be updated to a cleaner error
      die('Email invalid, please contact administrator');
    }
  }
  
  /**
  * @desc Add to
  */
  public function add_to($toName, $toEmail) {
    if(strlen($toName) > 0) {
      $this->toName = $toName;
    } else {
      $this->toName = $this->validate_email($toEmail);
    }
    $this->toEmail = $this->validate_email($toEmail);
  }
  
  /**
  * @desc Add additional to
  */
  public function add_additional_to($toName, $toEmail) {
    if(strlen($toName) > 0) {
      $this->additionalTo .= ",{$toName} <{$toEmail}> ";
    } else {
      $this->additionalTo .= ", <{$toEmail}>";
    }
  }
  
  /**
  * @desc Add from
  */
  public function add_from($fromName, $fromEmail) {
    if(strlen($fromName) > 0) {
      $this->fromName = $fromName;
    } else {
      $this->fromName = $this->validate_email($fromEmail);
    }
    $this->fromEmail = $this->validate_email($fromEmail);
  }
  
  /**
  * @desc Add org and domain
  */
  public function add_org_and_domain($org, $domain) {
    $this->org = $org;
    $this->domain = $domain;  
  }
  
  /**
  * @desc Build headers and set email type
  * @param bool $html True for HTML email, False for plain text
  */
  public function create_headers($html=TRUE) {
    if($html == TRUE) {
      // HTML headers
      $this->headers = "Content-type: text/html; charset=utf-8\r\n";
      $this->headers .= "Organization: " . $this->org . "\r\n";
      $this->headers .= "Content-Transfer-encoding: 8bit\r\n";
      $this->headers .= "X-Priority: 3\r\n";
      $this->headers .= "X-MSMail-Priority: Normal\r\n";
      $this->headers .= "X-Mailer: PHP\r\n";
      $this->headers .= "From: " . $this->fromName . " <" . $this->fromEmail . ">\r\n";
      $this->headers .= "To: " . $this->toName . " <" . $this->toEmail . ">{$this->additionalTo}\r\n";
      $this->headers .= "Reply-To: " . $this->fromName . " <" . $this->fromEmail . ">\r\n";
      $this->headers .= "Return-Path: " . $this->fromName . " <" . $this->fromEmail . ">\r\n";
      $this->headers .= "Message-ID: <" . md5(uniqid(time())) . "@" . $this->domain . ">\r\n";
      $this->headers .= "Date: " . date("r") . "\r\n";    
    } else {
      // Plain text headers
      $this->headers = "Content-type: text/plain\r\n";
      $this->headers .= "From: " . $this->fromName . " <" . $this->fromEmail . ">\r\n";
      $this->headers .= "To: " . $this->toName . " <" . $this->toEmail . ">{$this->additionalTo}\r\n";
      $this->headers .= "Reply-To: " . $this->fromName . " <" . $this->fromEmail . ">\r\n";
      $this->headers .= "Return-Path: " . $this->fromName . " <" . $this->fromEmail . ">\r\n";
      $this->headers .= "Message-ID: <" . md5(uniqid(time())) . "@" . $this->domain . ">\r\n";
      $this->headers .= "Date: " . date("r") . "\r\n";
    }
  }
  
  /**
  * @desc Add subject
  * @param string $subject Email subject
  */
  public function add_subject($subject) {
    $this->subject = $subject;  
  }
  
  /**
  * @desc Add body
  */
  public function add_body($body) {
    $this->body = $body;
  }
  
  /**
  * @desc Send email
  */
  public function send() {
    @mail('', $this->subject, $this->body, $this->headers);
  }
}
?>