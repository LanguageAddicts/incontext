<?php
/**
* @package html
*/
class html_elements {
  
  /**
  * @desc Create a <div></div>
  */
  public function create_div($id, $data, $style='', $class='') {
    $html = "<div id=\"" . $id . "\"";
    if(strlen($style) > 0) {
      $html .= " style=\"" . $style . "\"";
    } 
    if(strlen($class) > 0) {
      $html .= " class=\"" . $class . "\"";
    }
    $html .= ">\r\n" . $data . "\r\n";
    $html .= "</div>\r\n";
    return $html;
  }
  
  /**
  * @desc Create a <span></span>
  */
  public function create_span($id, $data, $style='', $class='') {
    $html = "<span id=\"" . $id . "\"";
    if(strlen($style) > 0) {
      $html .= " style=\"" . $style . "\"";
    } 
    if(strlen($class) > 0) {
      $html .= " class=\"" . $class . "\"";
    }
    $html .= ">\r\n" . $data . "\r\n";
    $html .= "</span>\r\n";
    return $html;
  }
  
  /**
  * @desc Create am img tag  
  */
  public function create_img(image_image $imageObject, $title, $class='') {
    $html = "<img src=\"" . $imageObject->get_image() . "\" width=\"" . $imageObject->get_width() . "\" height=\"";
    $html .= $imageObject->get_height() . "\" alt=\"image\" title=\"" . $title . "\"";
    if(strlen($class) >0) {
      $html .= " class=\"" . $class . "\"";    
    } 
    $html .= " />";
    return $html;  
  }
  
  /**
  * @desc Create an A tag
  */
  public function create_link($url, $link) {
    $html = "<a href=\"" . $url . "\">" . $link . "</a>\r\n";
    return $html;
  }
}
?>