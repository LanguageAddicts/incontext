<?php
/**
* Table class, dynamically create tables
* @package html
*/
class html_table {
  
  private $width;
  private $border;
  private $cellpadding;
  private $cellspacing;
  private $cssClass;
  private $style;
  private $cellData;
  private $tableRow;
  private $table;
  
  /**
  * @desc Constructor, sets tables properties
  * @param integer $width The width of the table
  * @param integer $border The table border
  * @param integer $cellpadding Cellpadding for cells
  * @param integer $cellspacing Cellspacing for cells
  * @param string $cssClass CSS class for table
  * @param string $style Additional style info
  */
  public function __construct($width, $border, $cellpadding, $cellspacing, $cssClass='', $style='') {
    $this->width = $width;
    $this->border = $border;
    $this->cellpadding = $cellpadding;
    $this->cellspacing = $cellspacing;
    $this->cssClass = $cssClass;
    $this->style = $style;
    $this->cellData = NULL;
    $this->tableRow = NULL;
    $this->table = NULL;
  }
    
  /**
  * @desc Add cell data
  * @param $data The data for the cell
  * @param string $cssClass CSS class to use for cell
  * @param string $style Additional style info
  * @param integer $span Whether the cell spans multiple cells, value equals number of cells to span, defaults to 0
  */
  public function add_cell_data($data, $cssClass='', $style='', $span=0) {
    if($span == 0) {
      // Single cell
      if(strlen($style) > 0) {
        $this->cellData[] .= "\r\n<td style=\"" . $style . "\"";  
      } else {
        $this->cellData[] .= "\r\n<td";
      }
      if(strlen($cssClass) > 0) {
        $this->cellData[] .= " class=\"" . $cssClass . "\">";
      } else {
        $this->cellData[] .= ">";
      }
      $this->cellData[] .= $data . "</td>";
    } else {
      // Span multiple cells
      if(strlen($style) > 0) {
        $this->cellData[] .= "\r\n<td colspan=\"" . $span . "\" style=\"" . $style . "\"";  
      } else {
        $this->cellData[] .= "\r\n<td colspan=\"" . $span . "\"";
      }
      if(strlen($cssClass) > 0) {
        $this->cellData[] .= " class=\"" . $cssClass . "\">";
      } else {
        $this->cellData[] .= ">";
      }
      $this->cellData[] .= $data . "</td>";
    }    
  }
  
  /**
  * @desc Build a table row
  * @param string $class CSS style to use
  */
  public function build_table_row($class='') {
    if(strlen($class) > 0) {
      $this->tableRow[] = "\r\n<tr class=\"" . $class ."\">";
    } else {
      $this->tableRow[] = "\r\n<tr>";
    }
    foreach($this->cellData as $data) {
      $this->tableRow[] .=  $data;
    }
    $this->tableRow[] .= "\r\n</tr>";
    // Reset celldata array
    $this->cellData = NULL;
  }
  
  /**
  * @desc Build the full table
  */
  public function build_table() {
    $this->tableData = "<table width=\"" . $this->width . "\" border=\"" . $this->border;
    $this->tableData .= "\" cellpadding=\"" . $this->cellpadding . "\" cellspacing=\"" . $this->cellspacing;
    if(strlen($this->style) > 0) {
      $this->tableData .= "\" style=\"" . $this->style;
    }
    if(strlen($this->cssClass) > 0) {
      $this->tableData .= "\" class=\"" . $this->cssClass;
    }
    $this->tableData .= "\">";
    foreach($this->tableRow as $data) {
      $this->tableData .= $data;
    }
    $this->tableData .= "\r\n</table>";
  }
  
  /**
  * @desc Output the generated table
  * @return string
  */
  public function get_table_html() {
    return $this->tableData;    
  }  
   
}
?>