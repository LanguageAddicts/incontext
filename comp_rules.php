<?php

require("page_elements.php");

//competition submission page
openPage();
drawHead("Language Addicts English - Competition Rules");
drawBody();
closePage();

function drawBody()
{
//open body tag
echo "<body class=\"body\">";
drawHeader();
drawMainContent();
drawFooter();
drawAnalytics();
//close body tag
echo "</body>";
}


function drawMainContent()
{
//open maincontent div
echo "<div class=\"mainContent\">";
drawContent();
//close maincontent div
echo "</div><!--end of mainContent-->";
}

function drawContent()
{
//new line
echo "\n";
//open content div
echo "<div class=\"content\">";
//list of section functions here
terms();
echo "</div><!--end of content-->";
}

function terms()
{
echo <<<EOF
<div id="comp_body">
<h1>Official Rules</h1>
NO PURCHASE OR PAYMENT NECESSARY TO ENTER OR WIN. A PURCHASE OR PAYMENT WILL NOT INCREASE YOUR CHANCES OF WINNING.
<h2>Sponsor</h2>
The sponsor and administrator of this Contest is Language Addicts Ltd., 30 Pound Lane, Thatcham, Berks, RG193TQ, Great Britain. ("Sponsor").
<h2>Contest Timing</h2>
The Contest begins at 00:00:00 BST on April 30th, 2014 and ends  at 11:59:59 BST on June 30th, 2014 ("Contest Period").
<h2>How to Enter</h2>
During the Contest Period, you ("Contestant") can enter the Contest at http://www.languageaddicts.com/comp.htm ("Website") by submitting original video content of up to 15 seconds in length ("Contest Entry"). There is no limit to the number of Contest Entries Contestants can enter.

<h2>Contest Entry Requirements</h2>
Contest Entries must be in the following file formats: AVI, MOV, WMV, MP4, MPEG, FLV, 3GP or 3G2. The Contest Entry must be no more than 15 seconds  in length. The Sponsor reserves the right to allow for minor fluctuations between the Contestant&#39;s timing device and the actual runtime of the Contest Entry, based on clear intent to comply with the Official Rules, as determined in the Sponsor&#39;s sole discretion. Each Contest Entry must meet the following Contest Entry Requirements: (a) the Contest Entry must be the Contestant&#39;s original work and not contain any third party copyrighted material; (b) the Contestant should be able to provide upon request all appropriate clearances, permissions and releases for the Contest Entry (in the event the Contestant cannot provide all required releases, the Sponsor reserves the right, in the Sponsor&#39;s sole discretion, to disqualify the applicable Contest Entry, or seek to secure the releases and clearances for the Sponsor&#39;s benefit, or allow the applicable Contest Entry to remain in the Contest). Any Contest Entry that is considered by the Sponsor in its sole and absolute discretion to be obscene, pornographic, libelous, hate speech or otherwise objectionable, in whole or in part, will be disqualified and will not be eligible for the Contest. The sponsor will not share your personal information with third parties. The sponsor will only use your email to contact you directly in relation to the Contest. Any communication or information transmitted to the Sponsor or the Website by e-mail or otherwise is and will be treated as non-confidential and non-proprietary. Proof of submission is not considered proof of delivery to or receipt of such of the Contest Entry. Furthermore, the Sponsor shall have no liability for any Contest Entry that is lost, intercepted or not received by the Sponsor. By submitting a Contest Entry video, you are agreeing to grant a world-wide, royalty free license to the Sponsor to use the Contest Entry video (in its entirety or any portion thereof) and/or the Contestant&#39;s information in any media now known or hereafter devised, without restriction, including, commercially using and exploiting the Contest Entry to the fullest extent possible. The Sponsor will not be required to pay any additional consideration or seek any additional approval in connection with such use or exploitation. Additionally, by participating, each Contest grants to the Sponsor the unrestricted right to use all statements made in connection with the Contest, and pictures or likeness of Contestants appearing in the Contest Entry or any promotional photos or videos taken by the Sponsor to promote the winners of the contest. The Sponsor will not be required to pay any additional consideration or seek any additional approval in connection with such use or exploitation.

<h2>Accepted Videos</h2>
Each Contest Entry that is successfully uploaded to the Contest server during the submission process will be acknowledged by email ("Acknowledgement Email"). 
Each Contestant will receive a second email ("Acceptance Email") for their Contest Entry indicating whether or not the Contest Entry has been accepted into 
the Contest. Contest Entries that have been accepted into the Contest ("Accepted Video") are those that the Judges at their sole discretion have deemed to 
have met the Contest Entry Quality Requirements. See Website for Contest Entry Quality Requirements. 
A Contest Entry that is not accepted into the Contest ("Rejected Video") may receive constructive feedback within the Acceptance Email so that 
the Contest Entry can be revised and submitted for reconsideration. The resubmission will be subject to the same rules. 

<h2>Contest Winner Selection</h2>
Two prizes will be awarded. A prize for Most Prolific Producer and a prize for Most Gifted Producer. The prize for the Most Prolific Producer will be awarded to  the Contestant who submits the highest number of Accepted Videos during the Contest Period. In the event that the Most Prolific Producer prize is tied with two or more Contestants submitting the same number of Accepted Videos the prize money will be shared equally between the Contestants. The prize for the Most Gifted Producer will be awarded to the Contestant who submits the best Accepted Video of the Contest. The Contest for Most Gifted Producer shall be judged by the directors of Language Addicts Ltd ("Judges"). The Judges, in their sole discretion, will determine the winner of the Most Gifted Producer prize. All Contest Entries will be judged at the conclusion of the Contest Period by the Judges.
All results of the winner selections and judging are final and binding and subject to these Official Rules. In the event that a winning Contest Entry is discovered to be invalid for any reason whatsoever or the Contestant who submitted the winning Contest Entry fails to comply with these Official Rules prior to delivery of the prize, the prize may be forfeited and awarded to an alternate winner. No more than the advertised number of prizes will be awarded.
<h2>Contest Prizes</h2>
The Most Prolific Producer will be awarded $150. The Most Gifted Producer will be awarded $50. If the total number of Accepted Videos surpases 200 during the Contest Period then the prize for Most Prolific Producer will be increased to $300 and the prize for Most Gifted Producer will be increased to $100. The winners will receive their payment via Paypal. Prizes are non-transferable and no substitution will be made except as provided herein at the Sponsor&#39;s sole discretion. The Sponsor reserves the right to substitute a prize for one of equal or greater value if the designated prize should become unavailable for any reason. Winners are responsible for all taxes and fees associated with prize receipt and/or use. All prizes will be fulfilled on or before July 30th 2014.

<h2>Payment for Accepted Videos</h2>
Each of the first 500 Accepted Videos will result in a payment of $2 for the Contestant. A one-time payment of $10 will be made to every Contestant who submits 10 or more Accepted Videos during the Contest Period. All payments will be fulfilled on or before July 30th 2014.

<h2>Publicity</h2>
Participation in this Contest or acceptance of any prize by a Contestant shall constitute and signify the Contestant&#39;s agreement and consent that the Sponsor and its designees may use the winner&#39;s name and Contest Entry (collectively "Submission") in any manner the Sponsor deems fit including, but not limited to, in connection with the exploitation of the Submission and for promotional, advertising or other purposes, worldwide, in any and all media now known or hereafter devised, without limitation and without further payment, notification, permission or other consideration. The Contestant accepts and acknowledges that the Sponsor shall not be obligated to use the Submission, and that the Sponsor in its sole discretion shall have the right to refrain from using the Submission. By participating, the Contestant agrees to release and hold harmless the Sponsor and their respective subsidiaries, affiliates, suppliers, distributors, advertising/promotion agencies, and prize suppliers, and each of their respective parent companies and each such company&#39;s officers, directors, employees and agents from and against any claim or cause of action, including, but not limited to, receipt or use of the Contest Entry, Contest Entry information, and for any claim or cause of action based on publicity rights, defamation or invasion of privacy arising from or related to the Contestant&#39;s participation in this Contest.
<h2>Release</h2>
Each Contestant releases and holds harmless the Sponsor and its respective subsidiaries, affiliates, supplies, distributors, advertising/promotion agencies, and prize suppliers, and each of their respective parent companies and each such company&#39;s officers, directors, employees and agents from and against any claim or cause of action, including, but not limited to, personal injury, death, damage to or loss of property, arising out of participation in the contest or receipt or use or misuse of any prize.
<h2>Limitations of Liability</h2>
The Sponsor is not responsible for: (1) any incorrect or inaccurate information, whether caused by Contestants, printing errors or by any of the equipment or programming associated with or utilized in the Contest; (2) technical failures of any kind, including, but not limited to malfunctions, interruptions, or disconnections in phone lines or network hardware or software; (3) unauthorized human intervention in any part of the entry process or the Contest; (4) technical or human error which may occur in the offer or administration of the Contest, including but not limited to errors in the advertising, Official Rules, selection and announcement of the winners and distribution of the prizes or the processing of Contest Entries; (5) any inability of any winner to accept or use any prize for any reason; or (6) any injury or damage to persons or property which may be caused, directly or indirectly, in whole or in part, from the Contestant&#39;s participation in the Contest or receipt or use or misuse of any prize. No more than the stated number of prizes will be awarded. In the event that production, technical, seeding, programming or any other reasons cause more than the stated number of prizes as set forth in these Official Rules to be available and/or claimed, the Sponsor reserves the right to award only the stated number of prizes by a random drawing among all legitimate, un-awarded, eligible prize claims.
The Sponsor reserves the right to cancel, suspend and/or modify the Contest, or any part of it, if any fraud, technical failures or any other factor beyond the Sponsor&#39;s reasonable control impairs the integrity or proper functioning of the Contest, as determined by the Sponsor in its sole discretion. The Sponsor reserves the right, in its sole discretion, to disqualify any individual it finds to be tampering with the entry process or the operation of the Contest or to be acting in violation of these Official Rules or in an unsportsmanlike or disruptive manner. Any attempt by any person to deliberately undermine the legitimate operation of the Contest may be a violation of criminal and civil law, and, should such an attempt be made, the Sponsor reserves the right to seek damages from any such person to the fullest extent permitted by law. The Sponsor&#39;s failure to enforce any term of these Official Rules shall not constitute a waiver of that provision
<h2>Winner List</h2>
The Winners of the Contest will be contacted directly by the Sponsor to organise delivery of prize money. The list of winners will be posted on the Website no later than July 20th 2014.
<br>
</div> 
EOF;
}
?>