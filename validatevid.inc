<?php
/**
 * @author Timothy Clements
 * @copyright 2010
 */
 
 
session_start();

// Draw the textarea for scenario and the header columns for the table
echo
"<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">
<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
<link href=\"http://www.languageaddicts.com/css/styles.css\" rel=\"stylesheet\" media=\"all\">
<script>
$(function(){
$('#currentTime').html($('#video_container').find('video').get(0).load());                      
$('#currentTime').html($('#video_container').find('video').get(0).play());
})
setInterval(function(){
$('#currentTime').html($('#video_container').find('video').get(0).currentTime);
$('#totalTime').html($('#video_container').find('video').get(0).duration);    
},500)
</script>
</head><body>
<form action=\"$_SERVER[PHP_SELF]\" method=\"post\">
<h1>Validate.inc</h1>
<h2>Scenario</h2>

<p><textarea name=\"scenario\" cols=\"128\" rows=\"4\">$scenario</textarea></p> 
<p>$videoToEdit_jpg</p>
<DIV>
<button onclick=\"getPlaySpeed()\" type=\"button\">What is the playback speed?</button>
<button onclick=\"setPlaySpeedHalf()\" type=\"button\">Half speed</button>
<button onclick=\"setPlaySpeedQuarter()\" type=\"button\">Full speed</button>
<br> 
<div id=\"video_container\">
<article class=\"videoplayer\">
    <video poster=\"../thumbs/{$videoToEdit}.jpg\" preload=\"none\" controls=\"\" id=\"video\" tabindex=\"0\" width=\"420\">
      <source type=\"video/mp4\" src=\"../movies/{$videoToEdit}.mp4\" id=\"mp4\"></source>
      <source type=\"video/webm\" src=\"../movies/{$videoToEdit}.webm\" id=\"webm\"></source>
      <source type=\"video/ogg\" src=\"../movies/{$videoToEdit}.mp4.ogv\" id=\"ogv\"></source>
      <p>Your user agent does not support the HTML5 Video element.</p>
    </video>
</article>	
<script>
myVid=document.getElementById(\"video\");
function getPlaySpeed()
  { 
  alert(myVid.playbackRate);
  } 
function setPlaySpeedHalf()
  { 
  myVid.playbackRate=0.5;
  }
function setPlaySpeedFull()
  { 
  myVid.playbackRate=1;
  }   
</script>
</div>

<div>Current Time : <span  id=\"currentTime\">0</span></div>
<div>Total time : <span id=\"totalTime\">0</span></div> 

</DIV>

<table width=\"1043\" border=\"0\">
  <tr>
    <td align=\"center\">Sequence</td>
    <td align=\"center\">Word</td>
    <td align=\"center\">Time in video</td>
    <td align=\"center\">New word?</td>
    <td align=\"center\">Notes</td>
  </tr>";
  
// Now loop round the new words adding rows to the table. Add an edit box for time in video and check box for new word in each row.

for ($i = 0; $i <= $WordCount -1; $i++) {

    // As a loop always starts at zero, need to add 1 to $i for sequence
	$Sequence = $i + 1;

    echo 
		"<tr>
	      	<td align=\"center\">" .$newWords[$i]['sequence']. "</td>
    		<td align=\"center\">" .$newWords[$i]['word']. "</td>
    		<td align=\"center\">
      			<label>
        		<input type=\"text\" name=\"wordtime_$Sequence\" id=\"wordtime_$Sequence\" value=\"".$newWords[$i]['time']."\" />
        		</label>
      		</td>
  			<td align=\"center\">
      			<label>
        		<input name=\"newword_$Sequence\" type=\"checkbox\" id=\"newword_$Sequence\" value=\"checked\"  ".$newWords[$i]['newword']." />
        		</label>
      		</td>
      	 	<td align=\"center\">
			   <label>
        		<input name=\"notes_$Sequence\" type=\"\" id=\"notes_$Sequence\" value=\"".$newWords[$i]['notes']."\" size=\"60\" />
      			</label>
      		</td>
	  </tr>\n";
}

// Finish the table and add the add button

echo "
</table>

<table width=\"1043\" border=\"1\">
  <tr>
    <td align=\"left\">Attribution:</td>
    <td align=\"left\"><input type=\"text\" size=\"140\" name=\"attribution\" value=\"$attribution\"</td>
  </tr>
  <tr>
	<td align=\"left\">Attribution URL:</td>    
    <td align=\"left\"><input type=\"text\" size=\"140\" name=\"attributionurl\" value=\"$attributionurl\"</td>
  </tr>
  <tr>
    <td colspan=\"2\" align=\"left\"><input type=\"submit\" size=\"30\" name=\"button\" value=\"Add\"</td>
  </tr>
</table>
</form>
</body>
</html>"
?>