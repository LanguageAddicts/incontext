<?php
function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

function print_post_contents()
// function to print the contents of a $post from the form
{
 	foreach ($_POST as $key => $entry) 
	{ 
     print $key . ": " . $entry . "<br>"; 
	} 
}

function moviePlaceHolder($vidname, $loc=".")
{
$filename =  $loc."/thumbs/".$vidname. ".jpg";
//echo "looking for: $filename";
if (file_exists($filename)==false)
{
//set a default placeholder
$filename = $loc."/images/brokenlink.jpg";
}
return $filename ;
}


function searchForFile($fileToSearchFor)
{
  $numberOfFiles = count(glob($fileToSearchFor));
  if($numberOfFiles == 0){ return(FALSE); } else { return(TRUE);}
}

function getMovieDirectory()
{
return "../english/movies/";
}


function getPosterDirectory($abs = false, $videoid = -1)
{
if ($abs == true)
{$path = "/english/thumbs/";}
else
{$path= "./english/thumbs/";}
if ($videoid > 0)
{
$video = $path.$videoid.".jpg";
}
else
{
$video = $path."pending.jpg";
}
if (file_exists($video)==false)
{
//set a default placeholder
$video = $path."pending.jpg";;
}
return $video;
}

function getAuthorName($authorID)
{
$alias = null;
$firstName = null;
$surname = null;
$displayName = "Name unknown";
$result = getAuthorDetails($authorID);
if (mysqli_num_rows($result)>0 )
{
$authtrow = mysqli_fetch_array($result);
$firstName = $authtrow["name"];
$surname = $authtrow["surname"];
$alias = $authtrow["displayname"];
}
if ($alias<> null)
{
$displayName = $alias;
}
else if ($firstName <> null and $surname <> null)
{
$displayName = $firstName . " ".$surname[0];
}
return $displayName;
}


function getAuthorURL($contribID)
{
$URL = null;
$result = getAuthorDetails($contribID);
if (mysqli_num_rows($result)>0 )
{
$authtrow = mysqli_fetch_array($result);
if( $authtrow["url"] != " "){$URL = $authtrow["url"];}
}
return $URL;
}

function nice_url($url) {

    if(!(strpos($url, "http://") === 0)
	&& !(strpos($url, "https://") === 0)) {

        $url = "http://$url";
	
	}
	
    return $url;

}

function getCurrentPath()
{
$url = getcwd();
$array = explode('/',$url); 
$count = count($array);
return $array[$count-1];


}

// New function
function TimingsBeenAdded($VideoId, $connection)
{
	$query = "select * from videowords where videos_videoid = '$VideoId'";
	$result = mysqli_query($connection, $query) or die($query);
	while ($row = mysqli_fetch_row($result)) {
		if (($row[3] > 0))
		{
			return true;

		}

			
	}
	return false;
}

function get_competition_start()
{
return "01/04/2015";
}

function get_competition_end()
{
return "31/05/2014";
}
function get_competition_announcement()
{
return "04/05/2015";
}

function getPointsList()
{
return array("1", "2", "3");
}

function getRoot()
{
$domain = $_SERVER['HTTP_HOST'];
$docRoot = $_SERVER['DOCUMENT_ROOT'];
$dirRoot = dirname(__FILE__);
$protocol = isset($_SERVER["HTTPS"]) ? 'https://' : 'http://';
$urlDir = str_replace($docRoot, '', $dirRoot);
$site_path = $protocol.$domain;
return $site_path;
}
?>
