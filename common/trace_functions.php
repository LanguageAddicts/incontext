<?php

$traceHandle = null;

function tracefileName()
{return "tracefile.txt";}

function traceType()
{//possible trace types:
//inline
//file
return "file"; }

function traceActive()
{//true or false
return true;}

function traceStart()
{//kick off trace file
if ((traceActive() == TRUE) and (traceType()=="file"))
{
global $traceHandle;
$traceHandle = fopen(tracefileName(), 'w') or die("can't start trace");
fwrite($traceHandle,"************************************".PHP_EOL);
fwrite($traceHandle,"**********   Trace file    *********".PHP_EOL);
fwrite($traceHandle, date("D dS M,Y h:i:s a").PHP_EOL );
fwrite($traceHandle, php_uname().PHP_EOL );
fwrite($traceHandle,"************************************".PHP_EOL);
}
return $traceHandle ;
}

function trace($message, $dataObject = null)
{
if (traceActive() == TRUE)
{
global $traceHandle;
if ($dataObject <> null)
{
if (traceType()=="file")
{
fwrite($traceHandle,print_r($dataObject, true).PHP_EOL );
}
else
{
echo "</br>Trace: $message";
print_r($dataObject);
}
}
else
{
if (traceType()=="file")
{
fwrite($traceHandle,$message.PHP_EOL );
}
else
{
echo "</br>Trace: $message";
}
}
}
}

?>