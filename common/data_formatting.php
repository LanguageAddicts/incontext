<?php
function buildVideoMasterData($VideoID, $User=null, $Target = null){				$KnownWords = 0;
$Videos_Record = array();$Videos_Record = getVideoFiledetails($VideoID);
//callmadeupfunction();
trace("Get Video Master Data video record:",$Videos_Record);  	//run trace$Returned_Record["VIDEOID"] = $VideoID;$Returned_Record["POSTERID"] = $VideoID;
$Returned_Record["MOVIELINK"] = getMovieDirectory().$VideoID;
$Returned_Record["POSTERLINK"] = getPosterDirectory(false,$VideoID);
$Returned_Record["WORDCOUNT"] = $Videos_Record["WORDCOUNT"];$Returned_Record["VIDEONAME"] = $Videos_Record["VIDEONAME"];$Returned_Record["CONTRIBID"] = $Videos_Record["CONTRIBID"];
$AuthorRecord = getAuthorDetails($Videos_Record["CONTRIBID"]);$Returned_Record["CONTRIBNAME"] = getAuthorName($Videos_Record["CONTRIBID"]);
$Returned_Record["CONTRIBURL"] = getAuthorURL($Videos_Record["CONTRIBID"]);$AttributionArray = buildVideoAttributionList($VideoID);$Returned_Record["ATTRIBUTIONARRAY"] = $AttributionArray ;//keep next two lines temporarily$Returned_Record["ATTRIBUTION"] = $Videos_Record["ATTRIBUTION"];$Returned_Record["ATTRIBUTIONURL"] = $Videos_Record["ATTRIBUTIONURL"];//$Returned_Record["TARGET"] = $Target;$Returned_Record["LASTSEEN"] = getDateVideoLastShown($VideoID, $Target);$VideoWordsArray = buildVideoWordsMasterData($VideoID, $KnownWords,$User);$Returned_Record["KNOWNWORDS"]=$KnownWords;//$Returned_Record["KNOWNPERCENTAGE"] = getPercentage($Returned_Record["WORDCOUNT"], $Returned_Record["KNOWNWORDS"]);$Returned_Record["VIDEOWORDSARRAY"] = $VideoWordsArray;trace("Video Master Data :", $Returned_Record);return $Returned_Record;
}


function buildVideoAttributionList($VideoID)
{$videoattribution = getVideoAttribution($VideoID);$loop = null;
$VideoAttributionArray = array();while($attr = mysqli_fetch_assoc($videoattribution)){$attribution["ATTRIBUTION"] = $attr["ATTRIBUTION"];$attribution["ATTRIBUTIONURL"] = $attr["ATTRIBUTIONURL"];//Save this record to the words array$VideoAttributionArray[$loop++] =$attribution;}return $VideoAttributionArray;}
function buildVideoWordsMasterData($VideoID, &$KnownWords,$User){
$VideoWordsArray = array();trace("buildVideoWordsMasterData ".$VideoID." KnownWords ".$KnownWords);$videowords = getVideowords($VideoID);$wordLoop = null;while($word = mysqli_fetch_assoc($videowords)){$VideoWord["WORDID"] = $word["WORDID"];$VideoWord["SEQUENCE"] = $word["SEQUENCE"];$VideoWord["POSITION"] = $word["TIME"];//floor($word["TIME"]);$VideoWord["WORDNAME"]  = getWord($VideoWord["WORDID"]);$VideoWord["SOUNDFILENAME"] = $VideoWord["WORDNAME"];$VideoWord["WORDKNOWN"] = isWordKnown($word["WORDID"], $User);$VideoWord["VIDEOCOUNT"] = getVideoCount($VideoWord["WORDID"]);//ensure that repeated words are excluded from the known words tally if (($VideoWord["WORDKNOWN"]==true) && (hasWordAlreadyBeenCounted($wordLoop,$VideoWordsArray,$VideoWord) == false))//increment the known words counter{++$KnownWords;}//Save this record to the words array$VideoWordsArray[$wordLoop++] =$VideoWord;}trace("buildVideoWordsMasterData ".$VideoID." KnownWords ".$KnownWords);return $VideoWordsArray;}
function hasWordAlreadyBeenCounted($wordLoop,$VideoWordsArray,$VideoWord){$alreadyCounted = false;trace("buildVideoWordsMasterData: Wordloop=".$wordLoop."WordName:".$VideoWord["WORDNAME"]);if ($wordLoop != null){  foreach ($VideoWordsArray as $vWA)  {    if ($vWA["WORDNAME"] ==$VideoWord["WORDNAME"])    {$alreadyCounted = true;}  }}return $alreadyCounted;}
function getVideoCount($target){trace("get video count:",$target);$videoCount = 1;//get list of videos containing this word$result = getVideoFiledetailsByWord($target);$videoCount = mysqli_num_rows($result);return $videoCount;}

?>
